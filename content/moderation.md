---
title: Charte de modération pour les Réfractaires
type: docs
---

# Charte de modération pour les Réfractaires

## L'état d'esprit

> Le rôle de modérateur n'est pas un enjeu de pouvoir. Son objet est de se mettre au service de la communauté. C'est une personne capable d'évaluer une situation avec bienveillance, d'intervenir efficacement pour préserver l'espace collectif, et qui joue le rôle d'un relais au nom du collectif des réfractaires.

## Principes d'attitudes

Sur quels principes peut-on s'appuyer pour prendre les meilleurs décisions en tant que modos... ?

**Modérer en laissant son point de vue personnel de côté**  
Lors d'une action de modération, le modérateur met son point de vue personnel sur le sujet à modérer, de côté. Il ne doit se baser que sur la charte de modération, sur la protection de l'environnement, et non pas sur ce qu'il pense ou ce qu'il croit du sujet.

**Ne pas encourager les attitudes irrespectueuses**  
Ne pas nourrir les trolls (les provocateurs). Ils souhaitent l'attention du public, que ce soit en provoquant ou en se moquant, ils cherchent à mener l'autre au débordement émotionnel.

**Rester maître de soi**  
Ne pas répondre aux attaques personnelles, se retirer en cas d'énervement, laisser la place à un autre modérateur en cas de besoin (tension, affect, etc).

**Faire preuve d'humilité et d'empathie**  
Savoir douter et savoir écouter. Ne pas laisser son égo peser dans la balance.

**Temporiser et collectiviser**  
Tant qu'il n'y a pas urgence, demandez l'aide des réfractaires et des autres modérateurs, et référez vous à quelques principes détaillés plus bas. ​​​​​​​Demander l'avis d'autres réfractaires systématiquement quand c'est possible.

**Proposer des actions concrètes et s'assurer que ces actions soient effectuées**  
Exemple, envoi d'un mp pour rappeler les règles (pousser à la remise en question).

**Travailler en équipe entre les modérateurs et les réfractaires**  
Les réfractaires sont aussi en mesure d'effectuer des médiations lorsque les modérateurs ne sont pas connectés ou présents. Ils soutiennent et participent aux actions de modération.

**Bannir**  
En cas de diffamation, de propagande, de spams, d'insultes ou de manque de respect flagrant, sans aucune remise en question de la personne concernée par ces actes, le Ban est de rigueur.

**Calme**  
Le canal Calme (anciennement marais-cage) est un espace réservé aux personnes dont on suppose qu'ils peuvent se remettre en question suite à un débordement (engagement émotionnel passager, etc). Réintégration des personnes sous 3 à 4 jours, sinon, ban. Limite de 2 modérateurs pour échanger avec la personne pour éviter l'effet de meute.

## Les dispositifs Discord

La plateforme Discord propose un certain nombre d'outils et de procédures pour administrer le fonctionnement de chaque espace. Passons-les en revue.

**Bannir pour délit**  
Dans le cas de brèche manifeste des règles du serveur, un ban immédiat doit être appliqué : spams, fishing, recrutement abusif, flooding, ce genre de choses.

**Bannir pour perturbation**  
Cette initiative est envisageable lorsqu'un membre diffame, poste à tout va sans argumenter, spamme, insulte ou manque de respect à d'autres membres. Il faut s'appuyer impérativement sur une décision collective : mettre une proposition de ban dans le salon #Modération.

*Toute trace du délit (sauf en cas de flood ou de délits à répétition) doit être conservée dans le canal modération cependant (capture d'écran).*

**Expulser / Mettre en sourdine**  
Aucun intérêt.

**Rendre muet sur un vocal**  
Source de bruit, saoul et dérangeant, insultant / malpoli, monopolise la parole et persiste. Et le notifier dans le canal modération (personne et raison).

**Déconnexion d'un vocal**  
À ne jamais utiliser comme outil de modération, éventuellement pour rendre service à la personne, qui n'aurait pas conscience d'être écoutée. (Exception: les gens qu'on ne connaît pas avec casque et micro coupé et qui ne communique pas à l'écrit).

**Déplacement d'une personne**  
On n'aime pas trop déplacer les gens par confort, on préfère perdre du temps à leur expliquer comment faire : éduc' pop ! Quand quelqu'un ne comprend pas comment rejoindre tel ou tel vocal, il vaut mieux lui expliquer que le déplacer direct.

**Déplacement d'un contenu**  
Demander à la personne de déplacer (effacer puis reposter) son propre contenu : "Bonjour @machin, pourrais-tu déplacer ton message dans le canal #truc s'il-te-plaît ? Dans le cas contraire, le message sera déplacé (supprimé puis reposté dans #truc sous la forme "message de @machin déplacé depuis #bidule: [le message]" ") car non déplaçable.

**Suppression d'un contenu**  
(par ex doublon de lien) Signaler aux utilisateurs concernés la suppression (information).

*Archiver le message en faisant une capture d'écran et en le postant dans le canal modération pour garder des traces.*

