---
title: Comment paramétrer OBS pour streamer sur PeerTube ?
type: docs
weight: 30
---

# Comment paramétrer OBS pour streamer sur PeerTube ?

## D'abord quelques définitions

### "OBS"
[OBS Studio](https://obsproject.com/fr) alias OBS est un logiciel qui permet de diffuser en direct un flux vidéo ou de l'enregistrer dans un fichier vidéo.

### "Quota des vidéos"
Sur une instance PeerTube, il s'agit de la taille du stockage vidéo allouer à chaque utilisateur.

### "Live permanent" quézaco ?
Sur PeerTube, un live "normal" une fois arrêter (plantage d'OBS, ou arrêt de la part du streameur), ce dernier est transformé en vidéo.
Un live permanent lui ne disparait pas.

## PeerTube

### Mon instance gère-t-elle les lives ?
Pour savoir si votre instance PeerTube vous permet de faire des lives, aller dans la section "À propos" en bas à gauche de l'instance.

![PeerTube: À propos](/img/obs-peertube/PeerTube_A_propos.png)

Vous tomberez ensuite sur une page proche de celle-ci :

![PeerTube: Instance](/img/obs-peertube/PeerTube_Instance.png)

Regarder la section "Version de PeerTube" à droite de la page, si la version n'est pas supérieure ou égale à 3.0.0 alors cette instance ne supporte pas encore les lives :

![PeerTube: Version](/img/obs-peertube/PeerTube_Version.png)

Chercher ensuite la section "Diffusion en direct" qui devrait ressembler à ça :

![PeerTube: Section](/img/obs-peertube/PeerTube_Section.png)

Ici, le streaming en direct est activé, avec un maximum de 1 live par utilisateur avec 2 live simultané max sur l'instance.

---
### Lancer un live pour auto-configurer OBS

Au premier lancement d'OBS l'assistant de configuration automatique devrait se lancer, sinon aller dans `Outils` puis `Assistant de Configuration automatique`.

Si vous avez déjà configuré OBS pour un autre objectif que liver sur PeerTube, vous pouvez créer un nouveau profil dans `Profil` en haut de la fenêtre d'OBS.

Sur l'assistant laissez sur `Optimiser pour la diffusion en direct, l'enregistrement est secondaire`.

![OBS Auto 1](/img/obs-peertube/OBS_Auto_1.png)

<!--La `Résolution de base` devrait-être celle de votre écran, il faut que son ratio soit du 16/9, voici une [liste des résolutions qui ont ce ratio](https://fr.wikipedia.org/wiki/Format_16/9#R%C3%A9solutions_d%E2%80%99%C3%A9cran_communes_suivant_un_rapport_16/9). À VOIR -->

Ici laissez tous par défaut.

![OBS Auto 2](/img/obs-peertube/OBS_Auto_2.png)

Pour remplir la fenêtre suivante il faudra que vous créiez un live permanent pour faire le test de bande passante.

Pour ça aller sur votre instance puis cliquez sur `Publier` en haut à droite.

![PeerTube: Publier](/img/obs-peertube/PeerTube_Publier.png)

Vous allez vous retrouver avec une page comme celle-ci. Cliquez sur `Aller au direct`.

![PeerTube: Téléverser](/img/obs-peertube/PeerTube_Televerser.png)

Sélectionnez une chaine puis un [niveau de visibilité](https://docs.joinpeertube.org/fr/use-create-upload-video?id=options-de-visibilit%c3%a9-des-vid%c3%a9os) (choisissez `Privé`). Puis cliquez sur `Aller au direct`.

![PeerTube: Live 1](/img/obs-peertube/PeerTube_Live_1.png)

Vous arriverez sur une page où vous pourrez donner les [détails du live](https://docs.joinpeertube.org/fr/use-create-upload-video?id=d%c3%a9tails-du-fichier). Aller dans l'onglet `Paramètres du direct`.

![PeerTube: Live 2](/img/obs-peertube/PeerTube_Live_2.png)

Dans cet onglet, vous trouverez les infos nécessaires pour l'assistant d'OBS, n'hésitez pas à utiliser les boutons à droite pour copier les champs puis les collez dans l'assistant.

>N'oubliez pas de cocher la case `C'est un direct permanent` car vous allez utliser l'auto-configurateur d'OBS.

![PeerTube: Live 3](/img/obs-peertube/PeerTube_Live_3.png)

Dans OBS, sélectionner `Personnalisé...` dans `Service` puis collez les infos. Sur l'instance PeerTube cliquez sur `Mise à jour`.
![OBS Auto 3](/img/obs-peertube/OBS_Auto_3.png)

Votre direct est prêt, vous pouvez faire le test de bande passante sur OBS.

![OBS Auto 4](/img/obs-peertube/OBS_Auto_4.png)

![OBS Auto 5](/img/obs-peertube/OBS_Auto_5.png)

Et voici plusieurs résultats pour une même instance. Dans ces résultats vous allez trouver :
- Le débit vidéo, si celui-ci tourne autour de 2000 vous pourrez peut-être streamer en HD (720p).
- Pour le reste on en parlera en peaufinant les paramètres.


>Dans les images si dessous, le `Erreur d'enregistrement` est une erreur de traduction d'OBS cela devrait être `Encodeur d'enregistrement`

![OBS Auto 6](/img/obs-peertube/OBS_Auto_6.png)

![OBS Auto 7](/img/obs-peertube/OBS_Auto_7.png)

![OBS Auto 8](/img/obs-peertube/OBS_Auto_8.png)

![OBS Auto 9](/img/obs-peertube/OBS_Auto_9.png)

Comme vous pouvez le voir le débit vidéo peut varier d'un essai à un autre.

Il n'y a plus qu'à cliquer sur `Appliquer les paramètres`. Nous allons peaufiner de suite après.

#### Supprimer le live permanent
Vous devriez supprimer le live pour libérer une place dans le quota de live de l'instance.

Allez dans `Vidéos`.

![PeerTube: Vidéos](/img/obs-peertube/PeerTube_Videos.png)

Cliquez sur `...` à droite du live.
![PeerTube: Mes Vidéos](/img/obs-peertube/PeerTube_Mes_Videos.png)

Puis `Supprimer`.
![PeerTube: Supprimer 1](/img/obs-peertube/PeerTube_Supprimer_1.png)

Puis `Confirmer`
![PeerTube Supprimer 2](/img/obs-peertube/PeerTube_Supprimer_2.png)

Vous avez supprimé votre live permanent.
![PeerTube Supprimer 3](/img/obs-peertube/PeerTube_Supprimer_3.png)

---
### Peaufiner les paramètres d'OBS

Ouvrez les `Paramètres`, vous êtes normalement dans la catégorie `Général`, dans la section `Sortie`. Cochez au-moins comme ci-dessous :

![OBS Général Sortie](/img/obs-peertube/OBS_General_Sortie.png)

#### Stream

Ensuite dans la catégorie `Stream (flux)`, vous pourrez ici changer la clé de stream pour vos lives PeerTube.

![OBS Stream](/img/obs-peertube/OBS_Stream.png)

#### Vidéo

Dans la catégorie `Vidéo`

![OBS Vidéo](/img/obs-peertube/OBS_Video.png)

Les deux résolutions doivent être au [format 16/9](https://fr.wikipedia.org/wiki/Format_16/9#D%C3%A9finitions_d%E2%80%99%C3%A9cran_communes_suivant_un_rapport_16/9) pour PeerTube.

La `Résolution de base` correspond à ce qui s'affiche dans OBS.

La `Résolution de sortie` correspond à ce que vous enregistrer ou streamer.
Si vous souhaitez par exemple streamer en HD et enregistrer en Full HD mettez la Sortie en Full HD puis vous appliquerez une mise à l'échelle sur le streaming dans `Sortie` mais celle-ci sera seulement avec le filtre Bicubique.

Pour le `Filtrage de la mise à l'échelle`, plus vous utilisez un filtrage avancé plus le processeur sera impliqué.

#### Sortie
Dans la catégorie `Sortie`, changez le `Mode de Sortie` de `Simple` à `Avancé`.

![OBS Sortie Simple](/img/obs-peertube/OBS_Sortie_Simple.png)

Votre interface devrait changer.

##### Streaming

![OBS Sortie Avancé 1](/img/obs-peertube/OBS_Sortie_Avance_1.png)

Activez et paramétrez la `Mise à l'échelle pour la Sortie`, si vous en avez besoin mais ce dernier est surement bloqué du Bicubique.

> N'oubliez pas de tester, tester et retester plusieurs configurations pour voir laquelle peut vous convenir.

###### Débit

Ici vous devez choisir un débit en fonction de votre connexion et de la définition d'écran, pensez que l'instance devra transcoder dans les définitions inférieures et aussi aux spectateurs qui ne vont pas forcément regarder en 16K.

|[Définitions d'écran](https://fr.wikipedia.org/wiki/Format_16/9#D%C3%A9finitions_d%E2%80%99%C3%A9cran_communes_suivant_un_rapport_16/9)|Débits pour du 30 fps|Débits pour du 60 fps|
|:-:|:-:|:-:|
|480p (SD)|500 à 2000 kb/s||
|720p (HD)|1 500 à 4 000 kb/s|2 250 à 6 000 kb/s|
|900p (HD+)|||
|1080p (Full HD)|3 000 à 6 000 kb/s|4 500 à 9 000 kb/s|
|1440p (QHD)|6 000 à 13 000 kb/s|9 000 à 19 000 kb/s|
|2160p (UHDTV1)|13 000 à 34 000 kb/s|20 000 à 34 000 kb/s|

Basé sur les recommandations de YouTube

###### x264
Avec x264 vous avez des pré-réglages, essayer de choisir entre *veryfast*, *faster*, *fast* ou *medium*.

Si c'est trop lent le processeur va être surchargé et si c'est trop rapide le live va pas être joli joli à voir.

###### NVENC
Si vous voulez utiliser NVENC, il faudra utiliser un débit suffisamment haut pour que ce dernier fasse le boulot correctement (HD 30fps tranche très haute approximativement).

##### Enregistrement (pour ceux qui veulent une sauvegarde locale du live)
Cette section devrait avoir un guide dédié mais pour faire très court :
- Mettez un plus grand débit vidéo si vous faire du montage, vous devrez changer d'encodeur (pensez à utiliser l'outil `Convertir un enregistrement` dans `Fichier` avant de passer au montage).
- Pour le format, toujours du mkv, comme ça même après un plantage le fichier sera lisible.

##### Audio

Pour PeerTube, vous pouvez tous mettre à 320 si vous le pouvez.

![OBS Sortie Audio](/img/obs-peertube/OBS_Sortie_Audio.png)

---
### Conclusion

Il n'y a plus qu'à appliquer le tout, créer des scènes et lancer des lives de test, puis commencer à streamer pour de vrai (seulement après avoir fait des tests compris).

Lisez aussi cette [page]({{< ref "docs/ressources/obs-peertube#soutiens" >}}) si ce n'est pas toujours fait.