---
title: Comment installer OBS sur macOS ?
menutitle: <b>macOS</b>
type: docs
weight: 20
---

# Comment installer OBS sur macOS ?

## D'abord quelques définitions

### "OBS"
[OBS Studio](https://obsproject.com/fr) alias OBS est un logiciel qui permet de diffuser en direct un flux vidéo ou de l'enregistrer dans un fichier vidéo.

---

<!-- NOTE: Je crois qu'il y a une histoire avec le son à apronfondir -->
## Exécutable DMG
Téléchargez l'installateur macOS [ici](https://obsproject.com/fr/download) et installez-le.

## Homebrew
Si vous avez [Homebrew](https://brew.sh/index_fr) d'installé, dans un terminal, tapez cette commande :
```
brew install --cask obs
```
