---
title: Comment installer OBS sur GNU/Linux ?
menutitle: <b>GNU/Linux</b>
type: docs
weight: 10
---

# Comment installer OBS sur GNU/Linux ?

## D'abord quelques définitions

### "OBS"
[OBS Studio](https://obsproject.com/fr) alias OBS est un logiciel qui permet de diffuser en direct un flux vidéo ou de l'enregistrer dans un fichier vidéo.

### "Flatpak" & "Snap" (utilisateur GNU/Linux seulement)
<!--Les Flatpaks et les Snaps sont des types de paquetage qui regroupe une application et ses dépendances qui fonctionne indépendament de la distribution Linux mais peuvent aussi occuper plus de place sur le disque.-->
Les Flatpaks et les Snaps sont deux autres moyens pour fournir des logiciels récents sur n'importe quelle distribution GNU/Linux, ils peuvent cependant nécessiter plus de place sur le disque.

Cela permet à certaine distribution, d'obtenir des versions plus récentes de certains programmes.

Dans notre situation :

>- La version Flatpak ne garantit pas une compatibilité avec tous les plugins OBS.
>- La version Snap regroupe une flopée de plugins OBS qui pourrait vous paraître superflu.

---
## Note à propos ...

### ...d'OBS Studio sur GNU/Linux
> Actuellement la source "Navigateur" (afficher une page Web directement sur OBS) n'est pas présente sur toutes les distributions GNU/Linux. Plus de détail [ici]({{< ref "docs/ressources/obs-peertube/annexes/obs-navigateur" >}}).

> Le plugin VST 2 d'OBS n'est pas présent sur toutes les distributions GNU/Linux. Plus de détail [ici]({{< ref "docs/ressources/obs-peertube/annexes/obs-vst" >}}).

---
### ...de GNOME et Wayland
OBS supporte d'être utilisé sous Wayland qu'à partir d'OBS 27.

Mais le cadriciel Qt a un comportement particulier sur GNOME donc il faut lui forcer la main.

Donc si votre bureau ressemble de près ou de loin à ça :

![Bureau GNOME](/img/obs-peertube/GNOME.png)

Vous utilisez GNOME avec surement Wayland.

Aller dans les paramètres, puis dans "À propos", puis regardez la section "Système de fenêtrage".

Si c'est "Wayland" qui y est marqué, il y a deux solutions :

- Votre version d'OBS est supérieure ou égale à 27, lancez OBS depuis un terminal avec cette commande :
    ```
    obs -platform wayland
    ```
    Si vous êtes sous ArchLinux/Manjaro et que vous utilisez le paquet AUR `obs-studio-tytan652`, faites clique droit sur OBS et vous aurez une option pour lancez OBS sous Wayland avec GNOME.

- Sinon fermez votre session et sur l'écran où vous tapez votre mot de passe, regardez en bas à droite, il devrait y avoir un engrenage.

    Cliquez dessus puis sélectionnez "GNOME sur Xorg".

    Reconnectez-vous et revérifiez dans paramètres, maintenant "X11" devrait avoir remplacé "Wayland".

---
### ...de pourquoi certaines distributions GNU/Linux ne fournissent pas la dernière version d'OBS

Les distributions comme Debian, Linux Mint ou encore Ubuntu sont des distributions versionnées. Elles ne sont pas à publication continue comme Manjaro par exemple. Elles ne fournissent pas la dernière version des logiciels mais une version "gelée" avec les derniers correctifs de sécurité jusqu'à la fin de vie de la version de la distribution.

---
## Méthode graphique

### Linux Mint
> Si vous voulez vraiment la dernière version d'OBS suivez ces [instructions]({{< relref "#ubuntu-avec-un-dépôt-ppa-linux-mint-pop_os" >}})

Ouvrez "Logithèque" et recherchez `obs studio`, dans les résultats vous risquez de trouver deux résultats semblables.

![Linux Mint](/img/obs-peertube/Linux_Mint_OBSs.png)

Ici, le premier est un flatpak, le deuxième est un paquet standard, ce dernier n'est peut-être pas à la dernière version d'OBS.

Choisissez celui que vous préférez.

---
### Manjaro
Ouvrez "Ajouter/supprimer des logiciels" et recherchez `obs`, dans les résultats sélectionnez "OBS Studio" et installez ce dernier.

Vous pouvez aussi installer la version Flatpak ou Snap en activant l'utilisation de ces derniers dans les préférences. Si ce n'est pas le cas, recherchez `pamac plugin` et installez celui dont vous avez besoin.

---
### Pop!\_OS
> Si vous voulez vraiment la dernière version d'OBS suivez ces [instructions]({{< relref "#ubuntu-avec-un-dépôt-ppa-linux-mint-pop_os" >}})

Ouvrez "Pop!\_Shop" et recherchez `obs`, dans les résultats sélectionnez "OBS Studio" mais attention deux versions sont proposées.

![Pop!\_OS](/img/obs-peertube/Pop_OS_OBSs.png)

Vous avez le paquet deb et le Flatpak, ici la version Flatpak est un poil plus récente, mais le paquet deb permettra une installation de plugin plus facile. Ici le choix est à vous.

---
### Ubuntu
> Si vous voulez vraiment la dernière version d'OBS suivez ces [instructions]({{< relref "#ubuntu-avec-un-dépôt-ppa-linux-mint-pop_os" >}})

Ouvrez "Ubuntu Store" et recherchez `obs studio`, dans les résultats sélectionnez "OBS Studio" mais attention il est proposé plusieurs versions.

![Ubuntu](/img/obs-peertube/Ubuntu_OBSs.png)

Si le format `deb` est proposé (elle est peut-être à peine moins récente), choisissez entre celle-ci et la Snap stable. 

---
## Méthode terminal

### Arch Linux (EndeavourOS, Manjaro)
Dans un terminal, tapez cette commande :
```
sudo pacman -S obs-studio
```

---
### Debian (Linux Mint, Pop!\_OS, Ubuntu)
Dans un terminal, tapez cette commande :
```
sudo apt install obs-studio
```
>Il est possible que la version installée ne soit pas récente.

---
### Fedora
Pour installer OBS, il faut ajouter le dépôt tiers RPM fusion Free avec cette commande :
```
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
```
Ensuite vous pourrez, installer OBS avec cette commande :
```
sudo dnf install obs-studio
```

### Ubuntu avec un dépôt PPA (Linux Mint, Pop!\_OS)
> Ce PPA fournit les versions récentes d'OBS Studio.

Dans un terminal, tapez ces commandes :
```
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt-get update
sudo apt install obs-studio
```

---
### Flatpak
>L'installation de ```flatpak``` est requise. Linux Mint, Pop!\_OS & Manjaro l'ont installé de base.

Si vous voulez installez la version flatpak, tapez l'une de ces commandes :

- Pour tous les utilisateurs :
```
flatpak install com.obsproject.Studio
```

- Pour l'utilisateur connecté seulement :
```
flatpak install --user com.obsproject.Studio
```

---
### Snap
>L'installation de ```snapd``` est requise. Ubuntu & Manjaro l'ont installé de base.

Si vous voulez installez la version snap, tapez cette commande :
```
sudo snap install obs-studio
```
