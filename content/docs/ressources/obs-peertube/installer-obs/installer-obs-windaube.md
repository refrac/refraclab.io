---
title: Comment installer OBS sur Windows ?
menutitle: <b>Windows</b>
type: docs
weight: 30
---

# Comment installer OBS sur Windows ?

## D'abord quelques définitions

### "OBS"
[OBS Studio](https://obsproject.com/fr) alias OBS est un logiciel qui permet de diffuser en direct un flux vidéo ou de l'enregistrer dans un fichier vidéo.

---

## Exécutable EXE
Téléchargez l'installateur Windows [ici](https://obsproject.com/fr/download)
## Chocolatey
Si vous avez [Chocolatey](https://chocolatey.org/) d'installé, dans un PowerShell avec les droits admin, tapez cette commande :
```
choco install obs-studio
```