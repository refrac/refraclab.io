---
title: Plugin VST 2 sur OBS Studio (GNU/Linux)
menutitle: <b>Plugin VST 2 sur OBS Studio</b> (GNU/Linux)
type: docs
weight: 20
---

# Le plugin VST d'OBS sur GNU/Linux
Ce dernier n'est pas forcément fourni par défaut sur certaines distributions. Et lorsqu'il y est, il ne détecte pas les greffons VST 2 présents sur le système si la variable VST_PATH possède plusieurs répertoires.

Tapez cette commande :
```
echo $VST_PATH
```
Et si elle retourne quelque chose avec un ou plusieurs ":", vous serez victime de ce problème.

## Obtention

### ArchLinux (EndeavourOS, Manjaro)
Installez `obs-studio-browser` ou `obs-studio-tytan652` à la place d'`obs-studio` depuis le dépôt AUR.

Si vous installez `obs-studio-browser`, il vous réclamera  peut-être `cef-minimal-3770`, installez alors `cef-minimal-3770-bin`.

Plus d'info sur `obs-studio-tytan652` [ici]({{< ref "docs/ressources/obs-peertube/annexes/obs-studio-tytan652" >}}).

> Attention si un jour `obs-studio` propose ce plugin par défaut avec le correctif, vous devrez réinstaller ce dernier. Vous ne perdrez pas vos paramètres.

### Ubuntu avec un dépôt PPA (Linux Mint, Pop!\_OS)
Les paquets du PPA fournissent ce plugin mais avec le problème mentionné plus haut.

Installation [ici]({{< ref "docs/ressources/obs-peertube/installer-obs/installer-obs-linux#ubuntu-avec-un-dépôt-ppa-linux-mint-pop_os" >}}).

### Flatpak
La version Flatpak inclut ce plugin surement avec le problème mentionné plus haut.

Installation [ici]({{< ref "docs/ressources/obs-peertube/installer-obs/installer-obs-linux#flatpak" >}}).

### Snap
La version Snap inclut ce plugin surement avec le problème mentionné plus haut.

Installation [ici]({{< ref "docs/ressources/obs-peertube/installer-obs/installer-obs-linux#snap" >}}).

## Contourner le problème de détection

> Cette méthode n'est pas garantie sur Snap et Flatpak à cause de leur nature "bac à sable" qui ne laisse peut-être pas accés aux greffons VST 2.

Pour que le plugin VST 2 détecte les greffons VST 2, il faut lancer OBS d'une certaine manière:

Dans un terminal tapez ces commandes:
```
unset VST_PATH
obs
```

Pourquoi ça marche maintenant ?

Car on "désactive" la variable VST_PATH (que sur le terminal) et du coup OBS utilise une liste de répertoires codée en dur en son code pour trouver les greffons.
