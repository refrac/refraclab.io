---
title: Le variant obs-studio-tytan652 (ArchLinux/Manjaro)
menutitle: <b>Le variant obs-studio-tytan652</b> (ArchLinux/Manjaro)
type: docs
weight: 30
---

# Le variant obs-studio-tytan652 (ArchLinux/Manjaro)

## "obs-studio-tytan652" quézaco ?

Ce dernier est un variant du paquet `obs-studio` que j'ai crée, ce dernier possède en plus:

- La possibilité de lier le trafic du live sur une interface réseau, il s'agit d'une fonctionalité qui corrige un petit souci si vous avez deux connexions internet distinctes et que vous voulez que OBS utilise à tout prix l'autre sur GNU/Linux.
- La source "Navigateur" comme `obs-studio-browser` mais avec le même CEF (base du navigateur) que les paquets officiels d'OBS Project.
- Le plugin VST 2.
- Un patch pour pouvoir utiliser des scripts Python sur Linux.
- Le protocol FTL.
- Un moyen graphique d'ouvrir OBS sous Wayland sous GNOME.
- Un patch pour mémoriser quel caméra est attribué à une source.
- Les sources qui utilise VLC.
