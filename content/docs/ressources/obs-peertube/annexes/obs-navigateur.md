---
title: Source navigateur sur OBS Studio (GNU/Linux)
menutitle: <b>Source navigateur sur OBS Studio</b> (GNU/Linux)
type: docs
weight: 10
---

# La source OBS "Navigateur" sur GNU/Linux
Cette dernière n'est pas forcément fournie par défaut sur certaines distributions.

## Obtention

### ArchLinux (EndeavourOS, Manjaro)
Installez `obs-studio-browser` ou `obs-studio-tytan652` à la place d'`obs-studio` depuis le dépôt AUR.

Si vous installez `obs-studio-browser`, il vous réclamera  peut-être `cef-minimal-3770`, installez alors `cef-minimal-3770-bin`.

Plus d'info sur `obs-studio-tytan652` [ici]({{< ref "docs/ressources/obs-peertube/annexes/obs-studio-tytan652" >}}).

> Attention si un jour `obs-studio` propose cette source par défaut, vous devrez réinstaller ce dernier. Vous ne perdrez pas vos paramètres.

### Ubuntu avec un dépôt PPA (Linux Mint, Pop!\_OS)
Les paquets du PPA fournissent cette source.

Installation [ici]({{< ref "docs/ressources/obs-peertube/installer-obs/installer-obs-linux#ubuntu-avec-un-dépôt-ppa-linux-mint-pop_os" >}}).

### Snap
La version Snap inclut cette source.

Installation [ici]({{< ref "docs/ressources/obs-peertube/installer-obs/installer-obs-linux#snap" >}}).
