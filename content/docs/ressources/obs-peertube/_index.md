---
title: Live avec OBS Studio sur PeerTube
menutitle: <b>Live avec OBS Studio sur PeerTube</b>
bookCollapseSection: true
weight: 10
---
# Live avec OBS Studio sur PeerTube
Bienvenue sur le tutoriel pour utiliser la fonctionalité live de PeerTube

## Soutiens

Ce tutoriel est centré autour de deux projet donc :

>Si vous aimez PeerTube, pensez à soutenir le projet en :
>- donnant à [Framasoft](https://soutenir.framasoft.org) qui à permis la naissance et surtout la périnité du projet
>- aidant à la traduction de PeerTube sur le [Weblate de Framasoft](https://weblate.framasoft.org/projects/peertube/)
>- aidant au developpement de PeerTube sur le [GitHub du projet](https://github.com/Chocobozzz/PeerTube)
>
>Et aussi en donnant à ceux qui maintiennent le ou les instances que vous utilisez.

>Si vous aimez OBS Studio, pensez à soutenir le projet en : 
>- donnant au projet sur [Open Collective](https://opencollective.com/obsproject)
>- donnant à Jim, le chef du projet sur son [Patreon](https://patreon.com/OBSProject)
>- aidant à la traduction d'OBS sur [Crowdin](https://crowdin.com/project/obs-studio)
>- aidant au développement d'OBS sur le [GitHub du projet](https://github.com/obsproject/obs-studio)

>Si vous voulez donnez vos retour sur le tuto, vous pouvez:
>- répondre à ce [pouet](https://framapiaf.org/@tytan652/105826470523094287) sur Mastodon
>- venir sur le [discord du Canard](https://discord.gg/HaEpyY3) et me mentionner (`@tytan652`) dans un salon
---

Page suivante : [Prérequis]({{< ref "docs/ressources/obs-peertube/prerequis" >}})