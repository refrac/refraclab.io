---
title: Prérequis pour live sur PeerTube
type: docs
weight: 10
---

# Prérequis pour live sur PeerTube

## Préambule
Dans l'ensemble de ce tutoriel, nous mentionnerons seulement PeerTube et seulement ce dernier. Ce tutoriel n'est pas là pour vous aider à rejoindre les rangs des [GAFAM](https://fr.wikipedia.org/wiki/GAFAM).

Ce tutoriel n'est pas non plus là pour vous donner une solution clef en main pour faire un live. Mais il est là pour vous aider à arriver à cet objectif.

## Définition

### Un fichier vidéo

Un fichier vidéo est schématiquement composé de :
- un conteneur vidéo qui contient:
 - les données de la vidéo compressée avec un codec
 - les données de l'audio compressé avec un codec

#### Codec
Codec signifie **Co**mpresseur/**déc**ompresseur. (C'est comme Modem, **Mo**dulateur/**dém**odulateur)

Il s'agit littéralement de la méthode qui va compresser la vidéo pour la stocker et la décompresser pour la lire.

La vidéo/audio ne contient pas le codec (l'outil), elle a seulement les données compressées avec.

>**Il ne faut pas confondre conteneur et codec.**

>Il n'y a pas de meilleur codec, il y en a un pour une situation donnée.

Exemple de codec vidéo :
- H264 ou AVC
- H265 ou HEVC
- ProRes
- DNxHD
- AV1

---

#### Conteneur
Un conteneur est un type de fichier.

Un fichier comme `Bountar.mov` est un fichier vidéo avec le conteneur QuickTime (.mov) et non un "codec QuickTime".
Ce dernier est juste un contenant, on peut donc utiliser quasiment n'importe quel codec avec, le conteneur se contente de contenir les données compressées avec.

Exemple de conteneur (vidéo principalement) :
- QuickTime (.mov)
- MP4 (.mp4)
- Matroska Vidéo (.mkv)

Quand on parle du type de fichier vidéo on parle du conteneur et non du codec utilisé.

---

### Encodeur
Il s'agit du nom donnée au compresseur d'un codec.

## Encodage
Il existe deux type d'encodage :

#### Matériel
L'encodage matériel est le fait d'utiliser un composant dédié à ça, donc pas de perte de performance au niveau du processeur ou de la carte graphique.

##### AMD Radeon
À faire car je n'ai pas de carte graphique AMD avec l'encodeur AMF ou autre ancien nom.

>Cela ne vous empèche pas d'essayer de l'utiliser.

##### Apple
À faire car je n'ai pas de matériel Apple avec leur encodeur.

>Cela ne vous empèche pas d'essayer de l'utiliser.

##### Intel
Si vous utilisez un processeur Intel, vous avez accès à un encodeur appelez "Quick Sync Video" mais il est vraiment pas terrible si vous avez un processeur datant d'avant la dixième génération, donc ignorez toute option lier à ce dernier si c'est le cas.

##### Nvidia
Les cartes graphiques GTX ou RTX de NVIDIA sont équipées d'une technologie appelée NVENC, elle est très bien pour tout encodage avec un très bon débit vidéo. Elle peut aussi supporter des utilisations simultanées.

#### Logiciel
Il existe un encodeur H264 logiciel et il s'agit de x264.

Ce dernier selon comment il est configuré aura un impact sur la qualité de la vidéo et l'utilisation du processeur.

## Débit

Pour pouvoir streamer au moins en 480p, vous devez avoir un débit de téléversement (upload) supérieur à 1 Mb/s.

Pour connaitre cette valeur, faites un test sur [LibreSpeed](https://librespeed.org) par exemple.


>Faites en plusieurs pour voir si c'est stable.

Vous pouvez aussi essayer un test avec 100 MB sur ce [site](https://testmy.net/upload) avec `Manual Test Size` pour tester votre connexion sur la durée.

Et prenez en considération que 2/3 de cette valeur surtout si votre ordinateur n'est pas le seul connecté à votre connexion internet. Exemple, vous avez 6 Mb/s, considérez que vous en avez que 4.

Passer cette valeur en kb/s (multiplier par 1000) et vous aurez une valeur à ne pas dépasser lorsque que vous configurerez OBS.

>Si vous utiliser un VPN, faites-en sorte que OBS n'utilise pas le VPN pour vos lives sauf si vous le souhaitez vraiment.