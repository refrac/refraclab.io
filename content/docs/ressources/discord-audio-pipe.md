---
title: Discord Audio Pipe (Perroquet Réfractaire)
menutitle: <b>Discord Audio Pipe</b>  (Perroquet Réfractaire)
type: docs
weight: 20
---

# Discord Audio Pipe (Perroquet Réfractaire)

Le **Perroquet Réfractaire** est un bot qui permet d'ajouter à un salon vocal une source audio externe (autre salon discord, lecteur de musique et etc).

## Origine du bot
Le Perroquet Réfractaire est né pendant les préparations finales de la première de l'anthropologie réfractaire suite au besoin de transférer le son du vocal studio (là où parlent les intervenants) vers l'amphi principal (là où les spectateurs pourraient écouter).

Après quelques recherches je suis tombé sur le logiciel [Discord Audio Pipe](https://github.com/QiCuiHub/discord-audio-pipe).

Ce dernier permet d'envoyer le son d'une **entrée** audio vers un bot.

Et il marche sous Windows et Linux. Cool, je suis sous Linux, je teste de suite et je découvre que le logiciel fonctionne en utilisant directement ALSA et pas PulseAudio.

J'ai donc dû trafiquer le logiciel pour le faire fonctionner avec PulseAudio. Puis la rustine est devenue un [fork](https://gitlab.com/refrac/discord-audio-pipe) en développement sur le Gitlab du canard.

## Comment reproduire un "Perroquet Réfractaire" ?

### Disclaimer pour Windows
Discord Audio Pipe n'utilise que les **entrées** audio du système, n'étant pas expert dans le domaine, je ne m'essayerai pas à expliquer comment avoir une entrée son qui reçoit le son de la sortie.

### Installation de Discord Audio Pipe

#### Windows
Téléchargez l'exécutable [ici](https://github.com/QiCuiHub/discord-audio-pipe/releases/latest), il ne s'agit pas d'un installateur.

Puis mettez-le dans un dossier consacré au logiciel.

#### Linux

##### ArchLinux / Manjaro
Installez avec AUR activé:
- libxcb
- qt5-svg
- python-pyqt5
- ffmpeg
- python-pynacl
- python-discord
- python-pulsectl


##### Ubuntu / Debian / Linux Mint
Installez avec APT :
```
sudo apt install libxcb-xinerama0 python3-pip python3-pyqt5.qtsvg ffmpeg python3-nacl python3-aiohttp
```

Et avec pip3 (en tant qu'utilisateur) :
```
pip3 --user pulsectl discord.py
```

---

Finalement télécharger l'archive du code source [ici](https://gitlab.com/refrac/discord-audio-pipe/-/releases).
Extrayez le dossier de l'archive, l'exécutable ici est `main.pyw` dans le dossier (conservez le dossier en entier).

> Essayez de l'exécuter si un éditeur de texte s'ouvre, vérifier les paramètres sur les fichiers exécutables de votre gestionnaire de fichier.

---

### Token du bot
Pour utiliser Discord Audio Pipe, il faut dans le même dossier que l'exécutable. Un fichier nommé `token.txt` qui contient seulement le token du bot.

Pour créer un bot et obtenir son token, suivez ce [tuto](https://devcommunity.gitbook.io/bot/robot-discord-pas-a-pas/creez-lapplication-de-votre-bot) en prenant en compte les remarques ci-dessous:

> Pour inviter votre bot sur votre serveur utilisez plutôt ce lien en remplaçant le `id_du_bot_ici` par l'identifiant du bot.
>
> https://discord.com/api/oauth2/authorize?client_id=id_du_bot_ici&permissions=3145728&scope=bot
>
> Ce lien confit seulement les droits de joindre un vocal et de parler dans un vocal au lieu de donner tous les droits.

> Si vous n'avez pas les droits pour ajouter le bot sur votre serveur :
>- Mettez votre bot en public (public bot) le temps de la manipulation
>- Donner le lien à une personne qui possède les droits nécessaires
>- Remettez le bot en privé une fois le bot ajouté

Copier-collez le token dans un "bloc-notes" puis enregistrez-le sous le nom `token.txt` dans le dossier où Discord Audio Pipe se trouve.

---

### Utilisation
Ouvrez l'exécutable, sélectionnez un périphérique d'entrée, un serveur, puis un canal et voila votre "perroquet" peut jacqueter.

> Note : Ce bot est utilisable de manière simultanée et indépendante sur plusieurs serveurs, mais sachez que l'utilisateur du logiciel aura accès aux canaux des serveurs reliés au bot.

tytan652, auteur cette page.