---
title: Réfractaires Records
bookCollapseSection: true
weight: 45
description: Chaîne de promotion de musique militante humaniste, altruiste, alter-mondialiste.
images:
  - /img/Logo_RefractaireRecords.png
---

# Réfractaires Records

<div>
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Trailer Refractaire Records" src="https://canard.tube/videos/embed/6c970549-c87f-41f9-8be6-9ed5f4c7c67e" frameborder="0" allowfullscreen></iframe>
</div>

Bienvenue sur la page Réfractaires Records, l'émission qui raccorde les musiciens et les réfractaires!  
Tous les mois, nous vous proposons une soirée Découverte et Débat (R²D²) avec un groupe ou un artiste indépendants.  
 
A mi-chemin entre live et la soirée-débat, venez venez nombreux animer la Déferlante Musicale,   
celle qui donne du **mouvement aux idées!** 

La 1ere édition prévue pour le 23/06 est reportée. Certains aspects techniques restent à travailler avant de passer au live. 
On vous dit à bientôt et merci de votre patience.

[Retrouvez Refractaires_Records sur canard.tube](https://canard.tube/video-channels/refractaires_records)

## Evenements

* 23 juin 2021 - les Forces de l'Orge : reporté


