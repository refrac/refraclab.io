---
title: Canardtube
bookCollapseSection: false
description: L'instance Peertube du Canard Réfractaire et de ses amis. C'est est une plateforme de vidéos en ligne dédiée à des créateur·ice·s militant·e·s. Nous défendons ici une ligne éditoriale humaniste, démocratique, écologiste et profondément attachée à l'égalité des citoyen·ne·s.
weight: 60
---

# Canard.tube

[Canard.tube][1] est une plateforme Peertube de vidéos en ligne dédiée à des créateur·ice·s militant·e·s. Nous défendons ici une ligne éditoriale humaniste, démocratique, écologiste et profondément attachée à l'égalité des citoyen·ne·s.

Canardtube est officiellement géré et financé par l'association loi 1901 "Le canard réfractaire" (Media indépendant des Côtes d'Armor) avec le soutien technique bénévole de la communauté des Réfractaires du discord.

# Gouvernance

La gouvernance du Canard.tube est assurée par l'équipe de gestion du serveur et par l'ensemble des vidéastes contributeur·ice·s qui souhaitent y participer.

Cette gouvernance concerne les modalités d'intégration à Canard.tube, la politique de modération des contenus et les choix nécessaires au vu des contraintes techniques.

L'intégration au Canard.tube se fait par cooptation. N'importe quel·le membre du Canard.tube peut proposer une candidature. Si aucun·e membre ne s'oppose à l'intégration du/de la candidat·e, celui ou celle-ci est intégré·e et devient membre de Canard.tube. 

S'il y a une opposition, la discussion doit amener à un consensus entre les membres. Si l'opposition persiste, le/la candidat·e n'est pas intégré·e.

---
## [Yohan][2]

[Le canard Réfractaire][3] - Media indépendant des Côtes d'Armor. Actualité critique, activisme et éducation populaire.
[site web][5] - [chaine youtube][4] - suivre `yohan@canard.tube` sur mastodon.

---
## [Aymeric][6]

[État Cryptique][7] - une chaîne à la fois militante et de vulgarisation juridique et politique ! On y parle d'institutions, de la Constitution, du fonctionnement de la République et de ses dysfonctionnements, de mouvements sociaux, etc. [chaine youtube][9]

[Aymeric Crypt][8] - une chaîne de collaboration avec d'autres vidéastes militants, pour des sessions en live.

suivre `aymeric@canard.tube` sur mastodon

---
## [Lisandru][10]

[KaLeeVision][11] - radio libre sur tout un tas de sujets de société, analyses politiques et commentaires. [chaine youtube][12] - suivre `lisandru@canard.tube` sur mastodon

---
## [Réfractaires][13]

[Educ Pop][14] - une chaine qui rassemble les initiatives d'éducation populaire issues de l'activité des membres du discord du canard réfractaire. [site web][15] - suivre `refractaires@canard.tube` sur mastodon

---
## [Booteille][16]

[Hygiène Numérique][17] - une chaîne pour traiter de logiciels libres, bonnes pratiques, et hygiène numérique en général. suivre `booteille@canard.tube` sur mastodon

---
## [Thomas][18]

[Thomas Babord][19] - petite chaine de politique de gauche. [blog][20] - [chaine youtube][21] - suivre `thomas@canard.tube` sur mastodon

---
## [Club de lecture][22]

[Podcast du club de lecture][23] - chaque semaine le mardi soir, des réfractaire lisent des livres a haute voix entre eux et puis ils en discutent. Audio seulement. [page web][24] - suivre `club.de.lecture@canard.tube` sur mastodon

---
## [Anthropologue réfractaire][25]

[Anthropologie Réfractaire][26] - tous les mois, Alexandre Duclos et les réfractaires abordent un sujet de société sous un angle anthropologique, interactivement en live sur discord. [la chaine youtube d'alexandre][27] - suivre `anthropologue.refractaire@canard.tube` sur mastodon

---
## [Punjo][28]

[Punjo][29] - Chaîne pour discuter de marxisme et de réflexions diverses et vive la révolution! [chaine youtube][30] - suivre `punjo@canard.tube` sur mastodon

---
## [Le Pixel Mort][31]

[Pixel][32] - Zappings narratifs et corrosifs sur l'actualité et la société. [chaine youtube][33] - suivre `pixel@canard.tube` sur mastodon

---

## [Malou][34]

[Malou l'accroqueuse de mot][35] - Slameuse, performeuse vocale, écrivaine, animatrice d'atelier, gesticulatrice ... [chaine youtube][36], [site web][37] - suivre `malou@canard.tube` sur mastodon

---

## [Rod Pi][38]

[Rod Pi (rod π)][39] - Musicien inspiré et militant, Islamo-végano-gauchiste auto-proclamé. [chaine youtube][40] - suivre `rodpi@canard.tube` sur mastodon

---

# Plus

Voir https://canard.tube/about/instance pour plus d'information ...

[1]: https://canard.tube
[2]: https://canard.tube/accounts/yohan
[3]: https://canard.tube/video-channels/lecanardrefractaire
[4]: https://www.youtube.com/c/LeCanardRéfractaire
[5]: https://lecanardrefractaire.org
[6]: https://canard.tube/accounts/aymeric
[7]: https://canard.tube/video-channels/etat_cryptique
[8]: https://canard.tube/video-channels/aymericcrypt
[9]: https://www.youtube.com/c/Étatcryptique
[10]: https://canard.tube/accounts/lisandru
[11]: https://canard.tube/video-channels/kalee_vision
[12]: https://www.youtube.com/c/KaLeeVision
[13]: https://canard.tube/accounts/refractaires
[14]: https://canard.tube/video-channels/educ_pop
[15]: https://discord.lecanardrefractaire.org/
[16]: https://canard.tube/accounts/booteille
[17]: https://canard.tube/video-channels/hygiene_numerique
[18]: https://canard.tube/accounts/thomas
[19]: https://canard.tube/video-channels/thomasbabord
[20]: http://thomas-babord.surge.sh/
[21]: https://www.youtube.com/c/ThomasBabord
[22]: https://canard.tube/accounts/club.de.lecture
[23]: https://canard.tube/video-channels/podcast_club_de_lecture
[24]: https://discord.lecanardrefractaire.org/docs/lectures/
[25]: https://canard.tube/accounts/anthropologue.refractaire
[26]: https://canard.tube/video-channels/anthropologie.refractaire
[27]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
[28]: https://canard.tube/accounts/punjo
[29]: https://canard.tube/video-channels/punjotube
[30]: https://www.youtube.com/channel/UCCmldKYrBh4azXU5FVY-obQ/about
[31]: https://canard.tube/accounts/pixel
[32]: https://canard.tube/video-channels/lepixelmort
[33]: https://www.youtube.com/channel/UCeXsZz8C5sUZe1YgDxR6j9A/about
[34]: https://canard.tube/accounts/malou
[35]: https://canard.tube/video-channels/maloulaccroqueusedemots
[36]: https://www.youtube.com/channel/UC-kcnsjHSANd5ujsvuvv_-A
[37]: https://maloulaccro.com/
[38]: https://canard.tube/accounts/pierrot
[39]: https://canard.tube/video-channels/rodpi/videos
[40]: https://www.youtube.com/channel/UCOtyPss6tgbrqhnZMch-bZA
