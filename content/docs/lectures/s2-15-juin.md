---
title: Séance du 15 juin
description: Constitution 1958, femmes Gilets jaunes, femmes dans la Commune de Paris, écarts sociaux, Le Corbusier, Porcs
weight: -20210615
---

# Séance du 15 juin

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="2021-06-15 Constitution 1958, femmes Gilets jaunes, femmes dans la Commune de Paris, écarts sociaux, Le Corbusier, Porcs" src="https://canard.tube/videos/embed/ed45aade-f35c-4100-97ce-11f08271edde" frameborder="0" allowfullscreen></iframe>

Myriam continue [la Constitution de la 5e République de 1958](https://fr.wikisource.org/wiki/Constitution_du_4_octobre_1958) en ses articles 11 à 19.
Pour l'article sur la Loi Molac : 
1ère partie : https://lecanardrefractaire.org/la-loi-molac/
2e partie : https://lecanardrefractaire.org/la-loi-molac-2e-partie/
3e partie : des témoignages du Réseau Diwan à venir la semaine prochaine

12:00 Claire lit [_Les femmes Gilets jaunes : révolte de classe, transgression de genre, histoire longue_](https://www.cairn.info/manuel-indocile-de-sciences-sociales--9782348045691-page-538.htm) par Fanny Gallot (1981-)

Apparté : [Catéchisme du révolutionnaire](https://fr.wikipedia.org/wiki/Cat%C3%A9chisme_du_r%C3%A9volutionnaire) : https://fr.wikisource.org/wiki/Le_Cat%C3%A9chisme_du_r%C3%A9volutionnaire

53:25 Galyem lit une page du journal "Le Siècle" du 20 mai 1871  : https://gallica.bnf.fr/ark:/12148/bpt6k732323f/f2.item
Aller plus loin sur le sujet : 
https://macommunedeparis.com/2021/05/05/5-mai-1871-union-des-femmes-ambulancieres-delegue-a-la-guerre-marechal/
https://macommunedeparis.com/2021/05/03/3-mai-1871-des-conciliatrices-et-cinq-communardes/

1:18:20 Galyem enchaine avec une lecture de [Jean Richepin](https://fr.wikipedia.org/wiki/Jean_Richepin) (1849-1926) : [La Chanson des gueux](https://fr.wikisource.org/wiki/La_Chanson_des_gueux)

1:29:34 Bountar extrait des bouts des [_écarts sociaux de recrutement des universités d'Île-de-France : un processus de ségrégation ?_](https://www.cairn.info/revue-espaces-et-societes-2014-4-page-111.htm) de Leila Frouillou 

1:56:01 Greg lit de Le Corbusier (1887-1965)

2:08:53 Raphael lit la page 170 des _Porcs 2_ de Marc-Edouard Nabe (1958-)