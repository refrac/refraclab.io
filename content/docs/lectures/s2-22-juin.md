---
title: Séance du 22 juin 
description: Anarchaféministes, féminisme, haïku et condition ouvrière
weight: -20210622
---

# Séance du 22 juin 2021 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="2021-06-22 Anarcha-fémisnisme, féminisme, haïku, Nietzsche, Simone Weil" src="https://canard.tube/videos/embed/de98fd37-6308-4c64-b394-3f16b67c7e86" frameborder="0" allowfullscreen></iframe>

Myriam lit [Coordination des groupes anarchistes - Pour une révolution anarchaféministe !](https://fr.theanarchistlibrary.org/library/coordination-des-groupes-anarchistes-pour-une-revolution-anarchafeministe) - 2012

34:41 Bountar enchaîne avec [Une question à Mme Madeleine Pelletier](https://fr.theanarchistlibrary.org/library/madeleine-vernet-une-question-a-mme-madeleine-pelletier) - 1908

57:37 Galyem propose un commentaire de "Nieche"

1:12:50 Greg partage avec nous des haïkus

livre : Pierres de l'être
Autrice : Sandrine Renard

Fausses promesses
les liqueurs
de l'aube

Tirer 
Le rideau de pluie
sur nos larmes asséchées

Fragiles pierres
De sable
Et d'être
(hommage à la pierre de l'Aître, sommet vosgien)

excursion 
dans le blanc 
des yeux

Délicate lumière
Du chagrin intérieur
Tamisée

Parer les coups 
De nos glaces
Brisées

Laisser crépitant
Le feu de nos abandons
S'éteindre

田舎、雪

電車が通り

蒸気雲
 

Campagne, neige

Un train passe

Nuage de vapeur

1:27:05 Raphael lit des bouts de la _Condition ouvrière_ de Simone Weil (1909-1943)
