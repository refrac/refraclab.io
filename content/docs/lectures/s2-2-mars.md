---
title: Séance du 2 mars
description: Lectures réfractaires du 2 mars 2021 - du Racine, de l'anarchisme, du Marx et un peu d'ethnologie de terrain...
weight: -20210302
---

<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/88396d08-51ab-4013-831f-e72a652d94b5" frameborder="0" allowfullscreen></iframe>
</div>

Bountar lit Xavier Bekaert L’anarchisme : une idéologie ou une méthodologie ? <https://fr.theanarchistlibrary.org/library/xavier-bekaert-l-anarchisme-une-ideologie-ou-une-methodologie>

Greg lit un peu de Phèdre de Jean Racine (1639-1699) : [Wikisource](https://fr.wikisource.org/wiki/Ph%C3%A8dre_(Racine),_Didot,_1854 (1677))

Professeur N poursuit Karl Marx, Friedrich Engels Manifeste du parti communiste 1847 [Wikisource](https://fr.wikisource.org/wiki/Manifeste_du_parti_communiste/Mermeix)

Patricien lit Revue des Deux Mondes, 3e période, tome 115, 1893 (p. 226-240) [Wikisource](https://fr.wikisource.org/wiki/Chronique_de_la_quinzaine_-_31_d%C3%A9cembre_1892)

Raul extrait des passages du chapitre 11 (final) de La Société contre l'État (1974) de Pierre Clastres
