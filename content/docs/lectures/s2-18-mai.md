---
title: Séance du 18 mai
description: Scolarité difficile, sadisme, Asimov & philo... et BONUS
weight: -20210518
---

# Séance du 18 mai 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/40a18d0d-522f-46d4-a76b-4e422e03b31b" frameborder="0" allowfullscreen></iframe>

I Bountar entame avec [On voudrait nous apprendre à marcher en nous coupant les pieds](https://fr.theanarchistlibrary.org/library/anonyme-on-voudrait-nous-apprendre-a-marcher-en-nous-coupant-les-pieds) - anonyme - 2004   
![ecole](/img/club-lecture/s2-18-mai/ecole.png "ecole")  

[II](https://canard.tube/videos/watch/40a18d0d-522f-46d4-a76b-4e422e03b31b?start=16m46s) Greg résume [Justine, ou les Malheurs de la vertu](https://fr.wikisource.org/wiki/Justine,_ou_les_Malheurs_de_la_vertu/Texte_entier) du Marquis de Sade (1740-1814)  
![sado](/img/club-lecture/s2-18-mai/sado.png "sado")  

[III](https://canard.tube/videos/watch/40a18d0d-522f-46d4-a76b-4e422e03b31b?start=43m5s) NoPesto poursuit [Sun Tzu L’Art de la guerre (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Suite de la préface et Chapitre 8  
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

[IV](https://canard.tube/videos/watch/40a18d0d-522f-46d4-a76b-4e422e03b31b?start=56m20s) Patricien lit [Isaac Asimov: les (quatre) “ lois de la robotique ” et l’échange de paroles](https://hal.archives-ouvertes.fr/hal-01397948/document) par Patrick Goutefangea - cf. l'oeuvre d'[Isaac Asimov (1920-1992)](https://fr.wikipedia.org/wiki/Isaac_Asimov)  
![asimov](/img/club-lecture/s2-18-mai/asimov.png "asimov")  


[V](https://canard.tube/videos/watch/40a18d0d-522f-46d4-a76b-4e422e03b31b?start=1h24m) Raphaël lit le chapitre LXXI de _Vues sur l'Europe_ d'André Suarès (1868-1948)  
![suares.png](/img/club-lecture/s2-11-mai/suares.png "Suarès")   

[BONUS !](https://canard.tube/videos/watch/40a18d0d-522f-46d4-a76b-4e422e03b31b?start=1h39m50s) 

Greg lit un texte issu de la propagande de la seconde guerre mondiale.

Bonus 2 : Patricien finit l'article commencé plus haut sur Asimov  :  
  
En vérité, il ne peut y avoir d'esclave idéal : l'idée que l'homme est un être libre se trouve au principe même du rapport du maître à l'esclave. Tout se passe comme si le robot d'Asimov incarnait l'idéal de l'homme lui-même, de sorte qu'il ne peut être, pour l'homme et  pour lui-même, autre chose qu'une fin. Le robot pour lequel sont énoncées les lois de la robotique, le robot dont la construction impose la formulation de ces lois, le robot dont elles déterminent le comportement, est sans doute une machine, mais une machine que ces lois mêmes élèvent à la dignité que l'être humain, son constructeur, s'attribue et inclut dans la définition de lui-même qu'il lui fournit.


