---
title: Séance du 27 avril 
description: fils de communard, fils d'ouvrier, éco-anxiété, Sun Tzu, Nietzsche rebelle aristocratique.
weight: -20210427
---

# Séance du 27 avril 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/376af5cf-f7c9-4ddc-8793-0fe1b7023432" frameborder="0" allowfullscreen></iframe>

[I](https://canard.tube/videos/watch/376af5cf-f7c9-4ddc-8793-0fe1b7023432?start=39s) Bountar entame la soirée avec [L’authentique embusqué](https://fr.theanarchistlibrary.org/library/emile-armand-l-authentique-embusque) par Émile Armand (1872-1962)  
![juin](/img/club-lecture/s2-27-avril/juin.png "juin")  

[II](https://canard.tube/videos/watch/376af5cf-f7c9-4ddc-8793-0fe1b7023432?start=5m46s) Patricien déroule avec [Les papillons de la révolte](https://short-edition.com/fr/oeuvre/nouvelles/les-papillons-de-la-revolte)  
![vosges](/img/club-lecture/s2-27-avril/88.png "Kichompré, Chapelle Saint-Étienne")  
Kichompré, Chapelle Saint-Étienne

[III](https://canard.tube/videos/watch/376af5cf-f7c9-4ddc-8793-0fe1b7023432?start=18m42s) Greg nous enchaîne avec Serge Latouche (1940-) qui évoque sa théorie de la décroissance conçue sous l'angle de ["l'abondance frugale comme art de vivre"](https://www.cairn.info/revue-projet-2011-5-page-160.htm)     
![Latouche](/img/club-lecture/s2-27-avril/latouche.png "Latouche")  

[IV](https://canard.tube/videos/watch/376af5cf-f7c9-4ddc-8793-0fe1b7023432?start=37m15s)  NoPesto poursuit [Sun Tzu L’Art de la guerre (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Suite de la préface et Chapitre 6   
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

[V](https://canard.tube/videos/watch/376af5cf-f7c9-4ddc-8793-0fe1b7023432?start=53m38s) Raphael lit l'article [Nietzsche contre la Révolution - A propos du livre de Domenico Losurdo : Nietzsche, il ribelle aristocratico](https://www.cairn.info/revue-actuel-marx-2004-1-page-147.htm) par Didier Renault  
![losurdo](/img/club-lecture/s2-27-avril/losurdo.png "losurdo")  


__

Revue de 1915 à laquelle participait Émile Armand :   
![refrac](/img/club-lecture/s2-27-avril/refrac.png "Réfractaires")  
