---
title: Séance du 9 février 
description: Illégalistes, Stratagèmes, Baudelaire et le Bataclan
weight: -20210209
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/22cac834-9a43-41e8-9419-6d77fa5464e8" frameborder="0" allowfullscreen></iframe>

Bountar Le Batard : Ricardo Flores Magon (1873-1922) Les illégalistes :
https://fr.theanarchistlibrary.org/library/ricardo-flores-magon-les-illegalistes

NoPesto entame [les Stratagèmes de l'Art d'avoir toujours raison](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_I) de Arthur Schopenhauer (1788-1860) :

Gяeg vous lira [L'Albatros & Élévation issus des Fleurs du mal](https://fr.wikisource.org/wiki/Les_Fleurs_du_mal/1868/L’Albatros) de Charles Baudelaire (1821-1867) :


Raul le Bœul fait un pas de côté mais reviendra bientôt à Rousseau et Darwin et lit 
des extraits de [La bataille d’Hajin : victoire tactique, défaite stratégique ?](https://www.asafrance.fr/images/legrier_françois-regis_la-bataille-d-hajin.pdf) par Colonel François-Régis Legrier. (1973-)


Liens pour aller plus loin sur cette lecture :
[Wikipedia - opération Chammal](https://fr.wikipedia.org/wiki/Op%C3%A9ration_Chammal)

[Irak, destruction d'une nation : Épisode 4 - Le fantôme (France 5)](https://www.france.tv/france-5/irak-destruction-d-une-nation/irak-destruction-d-une-nation-saison-1/2218789-episode-4-le-fantome.html)

[Intervention française en Syrie: "Les frappes contre Daech sont contreproductives"](https://www.lexpress.fr/actualite/monde/proche-moyen-orient/intervention-francaise-en-syrie-les-frappes-contre-daech-sont-contreproductives_1715729.html) (Express)

Analyse de La bataille d’Hajin : victoire tactique, défaite stratégique ?
https://www.asafrance.fr/item/analyse.html
