---
title: Séance du 2 février 
description: De la nécessité d'agir - lu par Bountar
weight: -20210202
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/839cbb5d-65e5-42db-adf9-d9ccb0c96116" frameborder="0" allowfullscreen></iframe>

[De la nécessité d’agir](https://lechafaud.noblogs.org/)

_Considération actuelle sur la guerre et sur la mort_ - Freud 1915

_L'art d'avoir toujours raison de Schopenhauer_ (fin de l'avant propos)

Suite et fin du _Discours sur l’origine et les fondements de l’inégalité parmi les hommes_ - JJ Rousseau [Wikisource](https://fr.wikisource.org/wiki/Discours_sur_l%E2%80%99origine_et_les_fondements_de_l%E2%80%99in%C3%A9galit%C3%A9_parmi_les_hommes)

Que sais-je de Patrick Tort - _Darwin et le darwinisme_ Chap. 4 La filiation de l'homme et la sélection sexuelle 2. l'effet réversif de l'évolution
