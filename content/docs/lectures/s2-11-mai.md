---
title: Séance du 11 mai
description: Féminisme, Psychopathinnen, bataille militaire, Dame Guillotine, Suarès et surprise finale !
weight: -20210511
---

# Séance du 11 mai 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/c8e3b249-a75b-403d-b92d-1922ba97da49" frameborder="0" allowfullscreen></iframe>

[I](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=1h19m36s) Bountar commence en lisant l'intégralité de ce beau texte écrit en 2003 : [_Les anarcha-féministes_](https://fr.theanarchistlibrary.org/library/louise-boivin-les-anarcha-feministes) par Louise Boivin

[II](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=9m6s) Greg enchaine en nous résumant un livre de 2018 : _Psychopathinnen (Femmes psychopathes) - Die Psychologie des weiblichen Bösen (la psychologie du mal au féminin)_ écrit par Lydia Benecke (1982-)   
![psychopath_greg.png](/img/club-lecture/s2-11-mai/psychopath_greg.png "Psychopathinnen")  

[III](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=30m) NoPesto poursuit [Sun Tzu _L’Art de la guerre_ (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Suite de la préface et Chapitre 7   
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

[IV](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=46m36s) Patricien extrait une des _Cinq Nouvelles extraordinaires_ de Gustave Le Rouge (1867-1938) : _Notre-Dame la Guillotine_  
Parue en 1893, elle a pour objet cette interrogation :  
_La querelle des têtes tranchées : Les médecins, la guillotine et l'anatomie de la conscience au lendemain de la Terreur_ disponible [ici](https://www.cairn.info/revue-d-histoire-des-sciences-2008-2-page-333.htm)  
![patricien.png](/img/club-lecture/s2-11-mai/patricien.png "Notre-Dame la Guillotine") 

[V](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=1h4m5s) Raphaël lit le chapitre LXX de _Vues sur l'Europe_ d'André Suarès (1868-1948)  
![suares.png](/img/club-lecture/s2-11-mai/suares.png "Suarès")   

[VI](https://canard.tube/videos/watch/c8e3b249-a75b-403d-b92d-1922ba97da49?start=1h19m36s) Galyem lit Edmond Rostand pour conclure !
