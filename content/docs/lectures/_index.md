---
title: Les lectures réfractaires
bookCollapseSection: true
weight: 40
description: Les lectures réfractaires sont la partie émergée du club de lecture de la communauté du discord du Canard réfractaire.
---

# Lectures réfractaires - chaque mardi soir à 21h30

## Présentation

Chaque mardi soir, nous partageons des lectures et échangeons autour d'elles.

Une nouvelle saison a débuté le 28 septembre 2021 et nous aurons toujours une place pour vous mardi prochain !

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/video-playlists/embed/bd789c79-8b31-4b45-a494-689aa091d731" frameborder="0" allowfullscreen></iframe>

## Les séances


- [2021-01-12 - Démocratie (2), Management, Au commencement était... (2)][29]
- [2021-12-21 - Ambassadeurs clients, acab, Hegel et Warhammer 40k][28]
- [2021-12-14 - Comprenez-vous ?, scandale et fascisme, Feurbach & Hegel "la belle âme"][27]
- [2021-10-19 - Crise du politique, un mois avec Manu, les capitalistes verts (revue _La Décroissance_), le Neveu de Rameau][26]
- [2021-09-28 - Emma Goldman, la STASI, Ordure et vengeance Traverse n° 37, Kropotkine (mémoires), Mort à Crédit][25]
- [2021-06-22 - Anarchaféministes, féminisme, haïku et condition ouvrière][24] 
- [2021-06-15 - Constitution 1958 art. 11 à 19, les femmes Gilets jaunes, femmes dans la Commune de Paris, écarts sociaux, Le Corbusier, Porcs][23] 
- [2021-06-08 - De Maistre et Rousseau, Constitution 1958, Abu-Jamal et impérialisme, patrimoine et mont St Michel, enfance][22] 
- [2021-06-01 - Rouge, barbarie française, S. Latouche : la Décroissance, l'atelier du vagin (Eve Ensler), Thomas Sankara][21] 
- [2021-05-25 - Noah, Tryo, Mickey 3D, Linkin Park, Rosemont, Miron, Pouchkine , Sun Tzu, AlexCentrique, Despentes][20]
- [2021-05-18 - Apprendre à marcher en nous coupant les pieds, Sade, Sun Tzu, Asimov, Suarès + bonus][19]
- [2021-05-11 - Les anarcha-féministes par L. Boivin, Psychopathinnen, Sun Tzu, Notre-Dame la Guillotine, Suarès, Rostand][18]
- [2021-05-04 - Voz de la mujer, je ne vote pas, Lénine est mort, Latouche, Professeur Choron, les jésuites][17]
- [2021-04-27 - L'authentique embusqué, les papillons de la révolte, Latouche, Sun Tzu, Losurdo sur Nietzsche][16]
- [2021-04-20 - Chouard 2/2, l'éduc'pop tiers espace par H. Bazin, Scandales et corruption, Onfray, Sun Tzu, Zweig, Hegel][15]
- [2021-04-13 - Chouard (1/2), encyclopédie anar : A comme altruiste, gestion de crise Lactalis, Sun Tzu, Nietzsche, Nabe][14]
- [2021-04-06 - Nationalisme et la route du bonheur pour les chinois, Sun Tzu, Pardo : la supercherie judiciaire, Mallarmé][13]
- [2021-03-30 - Nouveaux chiens de garde, rue de l'avenir est une impasse, Nietzsche, Schopenhauer, Foucault, Baudelaire][12]
- [2021-03-23 - Gilets jaunes (Soulèvement), HP et consentement, Laurent Alexandre, JCVD, Homo Domesticus 2/2][11]
- [2021-03-16 - Psychiatrie, Nietzsche, Schopenhauer, les lois scélérates, Feuillets d'usine, Homo Domesticus 1/2][10]
- [2021-03-09 - Aux aspirants suicidaires, Marx, Schopenhauer, Rivarol (1783), Clastres (3/3)][9]
- [2021-03-02 - Anarchisme : idéologie ou méthodologie, Racine, Marx, Revue des 2 mondes 1893, Clastres (2/3)][8]
- [2021-02-23 - Mort aux juges, les exploiteurs de la révolution, PowerPoint, Schopenhauer, Marx, Clastres (1/3)][7]
- [2021-02-16 - Au delà de la loi (P. Nin), Pouchkine, Schopenhauer, Darwin - la filiation de l'homme][6]
- [2021-02-09 - Illégalistes, Schopenhauer, Baudelaire, la bataille d'Hajin : vicroire tactique, défaite stratégique ?][5]
- [2021-02-02 - De la nécessité d'agir, Freud, Schopenhauer, Rousseau (2/2), Darwin & l'effet réversif de l'évolution][4]
- [2021-01-26 Lectures réfractaires - Rousseau (1/2), Michéa][3]
- [2021-01-26 Lectures réfractaires - Willful disobedience, Schopenhauer][2]
- [Saison 1 - 2020][1]

[1]: https://discord.lecanardrefractaire.org/docs/lectures/s1/
[2]: https://discord.lecanardrefractaire.org/docs/lectures/s2-19-janvier/
[3]: https://discord.lecanardrefractaire.org/docs/lectures/s2-26-janvier/
[4]: https://discord.lecanardrefractaire.org/docs/lectures/s2-2-fevrier/
[5]: https://discord.lecanardrefractaire.org/docs/lectures/s2-9-fevrier/
[6]: https://discord.lecanardrefractaire.org/docs/lectures/s2-16-fevrier/
[7]: https://discord.lecanardrefractaire.org/docs/lectures/s2-23-fevrier/
[8]: https://discord.lecanardrefractaire.org/docs/lectures/s2-2-mars/
[9]: https://discord.lecanardrefractaire.org/docs/lectures/s2-9-mars/
[10]: https://discord.lecanardrefractaire.org/docs/lectures/s2-16-mars/
[11]: https://discord.lecanardrefractaire.org/docs/lectures/s2-23-mars/
[12]: https://discord.lecanardrefractaire.org/docs/lectures/s2-30-mars/
[13]: https://discord.lecanardrefractaire.org/docs/lectures/s2-6-avril/
[14]: https://discord.lecanardrefractaire.org/docs/lectures/s2-13-avril/
[15]: https://discord.lecanardrefractaire.org/docs/lectures/s2-20-avril/
[16]: https://discord.lecanardrefractaire.org/docs/lectures/s2-27-avril/
[17]: https://discord.lecanardrefractaire.org/docs/lectures/s2-4-mai/
[18]: https://discord.lecanardrefractaire.org/docs/lectures/s2-11-mai/
[19]: https://discord.lecanardrefractaire.org/docs/lectures/s2-18-mai/
[20]: https://discord.lecanardrefractaire.org/docs/lectures/s2-25-mai/
[21]: https://discord.lecanardrefractaire.org/docs/lectures/s2-1-juin/
[22]: https://discord.lecanardrefractaire.org/docs/lectures/s2-8-juin/
[23]: https://discord.lecanardrefractaire.org/docs/lectures/s2-15-juin/
[24]: https://discord.lecanardrefractaire.org/docs/lectures/s2-22-juin/
[25]: https://discord.lecanardrefractaire.org/docs/lectures/s3-28-septembre/
[26]: https://discord.lecanardrefractaire.org/docs/lectures/s3-19-octobre/
