---
title: Séance du 23 mars 
description: Gilets Jaunes, consentement, JCVD, transhumanisme...
weight: -20210323
---

# Séance du 23 mars 2021 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/721f8217-b285-46ce-94c8-53c1c8d28026" frameborder="0" allowfullscreen></iframe>

Myriam évoque un témoignage de Gilet Jaune à retrouver dans ce [Journal - JAUNE n°1](https://jaune.noblogs.org/files/2019/01/Jaune1-web.pdf) issu du site : https://jaune.noblogs.org/ mais aussi reproduit dans ce livre :  
![soulev1](/img/club-lecture/s2-23-mars/soulev1.png "soulev1")  
![soulev2](/img/club-lecture/s2-23-mars/soulev2.png "soulev2")  


Bountar lit [Entre consentement et refus - Sur l’HP de Saint Venant](https://sansremede.fr/entre-consentement-et-refus/)  
![hospi](/img/club-lecture/s2-23-mars/hospi.jpg "hospi")

Patricien donne son avis sur le futur merveilleux de Laurent Alexandre en abordant les textes suivants :  
![pat0](/img/club-lecture/s2-23-mars/pat0.png "pat0")  
![pat1](/img/club-lecture/s2-23-mars/pat1.png "pat1")  
![pat2](/img/club-lecture/s2-23-mars/pat2.png "pat2") 

Greg lira des aphorismes de Jean Claude Van Damme et les mettra en parallèle avec d'autres pensées

"Je suis fasciné par l'air. Si on enlevait l'air du ciel, tous les oiseaux tomberaient par terre....Et les avions aussi.... En même temps l'air tu peux pas le toucher...ça existe et ça n'existe pas...Ça nourrit l'homme sans qu'il ait faim...It's magic...L'air c'est beau en même temps tu peux pas le voir, c'est doux et tu peux pas le toucher.....L'air c'est un peu comme mon cerveau..."

film Paterson :"I go through trillions of molecules that move aside to make way for me."

citation de Charles Baudelaire "Le merveilleux nous enveloppe et nous abreuve comme l'atmosphère mais nous ne le voyons pas"

Jean-Claude Van Damme : Moi Adam et Even, j'y crois plus, tu vois, parce que je suis pas un idiot : les serpents c'est gentil la pomme ça peut pas être mauvais, c'est plein de pectine et c'est anti-cholesterole.

https://www.dailymotion.com/video/x1tx9l

Michel Onfray Décadence ed j'ai lu (poche) page 129  

![jcvd](/img/club-lecture/s2-23-mars/jcvd.png "jcvd")   

Raul : termine sur Homo Domesticus - Une histoire profonde des premiers États, un ouvrage de l'anthropologue anarchiste James C. Scott publié en France en 2019 (titre original : _Against the Grain: A Deep History of the Earliest States_) - Chapitre 7 - L'âge d'or des barbares  
![Homo domesticus](/img/club-lecture/s2-16-mars/homo-domesticus.png "Homo domesticus")  

NoPesto continue [L’Art d’avoir toujours raison d’Arthur Schopenhauer (1788-1860)](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_XXXIII) - Stratagèmes 33 à 36.  
![L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)](/img/club-lecture/s2-16-mars/schopenhauer.png "L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)")
