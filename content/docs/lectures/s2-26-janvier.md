---
title: Séance du 26 janvier 
description: Rousseau, Michéa et Schopenhauer
weight: -20210126
---

- Première partie 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/fbb8a23f-b241-4b34-a55b-049cc7d57c37" frameborder="0" allowfullscreen></iframe>

Reprenons le contrôle de nos vies de Willful Disobedience N°2 ;
_L'art d'avoir toujours raison_ de Schopenhauer : le début de l'avant propos 

- Deuxième partie 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/cbfdaa7a-c741-4cc4-858e-ab1dc9bd155b" frameborder="0" allowfullscreen></iframe>

_Du contrat social_, Livre I Chap. 8, Rousseau 

"Ce passage de l'état de nature à l'état civil produit dans l'homme un changement très remarquable en substituant dans sa conduite la justice à l'instinct et en donnant à ses actions la moralité qui leur manquait auparavant. C'est alors seulement que la voie du devoir succède à l'impulsion physique, et le droit à l'appétit, l'homme, qui jusque là n'avait regardé que lui-même, se voit forcé d'agir sur d'autres principes, et de consulter sa raison avant d'écouter ses penchants. Quoi qu'il se prive dans cet état de plusieurs avantages qu'il tient de la nature, il en regagne de si grands, ses facultés s'exercent et se développent, ses idées s'entendent, ses sentiments s'ennoblissent, son âme toute entière s'élève à tel point que, si les abus de cette nouvelle condition ne le dégradaient souvent au-dessous de celle dont il est sorti, qu'il devrait bénir sans cesse l'instant heureux qu'il l'en arracha pour jamais et fit d'un animal stupide et borné un être intelligent et un homme.

puis _Discours sur l’origine et les fondements de l’inégalité parmi les hommes_ 

J-C Michéa _L'empire du moindre mal_
