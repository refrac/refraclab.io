---
title: Séance du 6 avril
description: Ils arrivent à pied par la Chine, supercherie judiciaire...
weight: -20210406
---

# Séance du 6 avril

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/4b484fe8-c03f-4047-9700-70a4a21e156a" frameborder="0" allowfullscreen></iframe>

Bountar débute sur [Ba Jin Le nationalisme et la route du bonheur pour les Chinois](https://fr.theanarchistlibrary.org/library/ba-jin-le-nationalisme-et-la-route-du-bonheur-pour-les-chinois)  

NoPesto entame [Sun Tzu L’Art de la guerre (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - chapitre 3 - Hachette  
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

Patricien lit [La supercherie judiciaire : De la criminalité en col blanc à la criminalité en robe noire](https://catalogue.bnf.fr/ark:/12148/cb45509876n)
![patsupercherie](/img/club-lecture/s2-6-avril/pat_supercherie.png "SUPERCHERIE")   

Raphaël lit [Mallarmé (1842-1898) - L’Ecclésiastique](https://fr.wikisource.org/wiki/Vers_et_Prose_(Mallarm%C3%A9)/L%E2%80%99Eccl%C3%A9siastique)
![mal](/img/club-lecture/s2-6-avril/mal.png "mal")
