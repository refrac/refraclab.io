---
title: Séance du 25 mai 
description: Crise et chansons
weight: -20210525
---

# Séance du 25 mai 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/71e30099-28c2-4f5f-acb6-72f0028a3822" frameborder="0" allowfullscreen></iframe>

I Myriam entame avec une lecture commenté de paroles de chansons autour de l'écologie : [lien vers les paroles et les commentaires](https://pad.lecanardrefractaire.org/pad/#/2/pad/view/7TmKZZbsxtVkZqh-65wa3CmkPjLyZe2CxfvdmV0G0Do/embed/) (cf. ci-dessous)


[II](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=16m) Bountar  lit un texte bourré d'idées et écrit en 2000 : [La Psychopathologie du Travail](https://fr.theanarchistlibrary.org/library/penelope-rosemont-la-psychopathologie-du-travail) par Rosemont Penelope (1942-)

[III](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=31m2s) Galyem entonne un beau poème : [La marche à l'amour](http://www.pierdelune.com/miron1.htm) par Gaston Miron (1928-1996)

[IV](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=36m47s) Greg reste dans la poésie avec : [Elégie](http://manfred.b.free.fr/pages/pouchkine02.htm) par Alexandre Pouchkine (1799-1837) puis [Le Lac](https://fr.wikisource.org/wiki/%C5%92uvres_compl%C3%A8tes_de_Lamartine_(1860)/Tome_1/Le_Lac) d'Alphonse de Lamartine (1790-1869)

[V](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=43m30s) NoPesto poursuit [Sun Tzu L’Art de la guerre (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Suite de la préface et Chapitre 9  
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

[VI](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=51m20) Patricien lit une transcription de [L'1FAUX D'1TOX#2 - L'Alchimie : Pythagore VS Fernand Nathan](https://youtu.be/0HqtaVrwMS0) (cf. ci-dessous)

[VII](https://canard.tube/videos/watch/71e30099-28c2-4f5f-acb6-72f0028a3822?start=1h14m30s) Raphaël lit de Virginie Despentes (1969-) : _King Kong Théorie_ (2006)

Texte évoqué par Bountar en résonnance : [Emma Goldman - L'Anarchisme et la question sexuelle - 1896](https://fr.theanarchistlibrary.org/library/emma-goldman-l-anarchisme-et-la-question-sexuelle)

L'intervention de Myriam en détail :

Ce soir, je voulais partager avec vous quelques paroles de chansons engagées pour l'écologie.
Quelques chansons que j'appécie particulièrement pour les paroles essentiellement mais également pour la mélodie.

Respire - Mickey 3D
L'hymne de nos campagnes - Tryo
Aux arbres citoyens - Yannick Noah
What I've done - Linkin Park

Respire - Mickey 3D

Approche-toi petit, écoute-moi gamin,
Je vais te raconter l'histoire de l'être humain
Au début y avait rien au début c'était bien
La nature avançait y avait pas de chemin
Puis l'homme a débarqué avec ses gros souliers
Des coups d'pieds dans la gueule pour se faire respecter
Des routes à sens unique qu'il s'est mis à tracer
Les flèches dans la plaine se sont multipliées
Et tous les éléments se sont vus maîtrisés
En deux temps trois mouvements l'histoire était pliée
C'est pas demain la veille qu'on fera marche arrière
On a même commencé à polluer le désert

Il faut que tu respires, et ça c'est rien de le dire
Tu vas pas mourir de rire, et c'est pas rien de le dire

D'ici quelques années on aura bouffé la feuille
Et tes petits-enfants ils n'auront plus qu'un oeil
En plein milieu du front ils te demanderont
Pourquoi toi t'en as deux tu passeras pour un con
Ils te diront comment t'as pu laisser faire ça
T'auras beau te défendre leur expliquer tout bas
C'est pas ma faute à moi, c'est la faute aux anciens
Mais y aura plus personne pour te laver les mains
Tu leur raconteras l'époque où tu pouvais
Manger des fruits dans l'herbe allongé dans les prés
Y avait des animaux partout dans la forêt,
Au début du printemps, les oiseaux revenaient

Il faut que tu respires, et ça c'est rien de le dire
Tu vas pas mourir de rire, et c'est pas rien de le dire
Il faut que tu respires, c'est demain que tout empire
Tu vas pas mourir de rire, et c'est pas rien de le dire

Le pire dans cette histoire c'est qu'on est des esclaves
Quelque part assassin, ici bien incapable
De regarder les arbres sans se sentir coupable
A moitié défroqués, cent pour cent misérables
Alors voilà petit, l'histoire de l'être humain
C'est pas joli joli, et j'connais pas la fin
T'es pas né dans un chou mais plutôt dans un trou
Qu'on remplit tous les jours comme une fosse à purin

Il faut que tu respires, et ça c'est rien de le dire
Tu vas pas mourir de rire, et c'est pas rien de le dire
Il faut que tu respires, c'est demain que tout empire
Tu vas pas mourir de rire, et c'est pas rien de le dire

Mickey 3D fait ici le pari de parler du futur complètement hallucinant qui pourrait nous arriver. Mais tout en se basant sur des choses déjà réelles. Pour atténuer la brutalité de leurs paroles, ils ont fait un clip en animation qui permet de penser que l'on regarde plutôt un desssin animé. Tout est joli et paisible. Mais la chute de la fin, nous imprime l'image de tout ce que l'on pourrait perdre et que l'on commence à perdre déjà.

L'hymne de nos campagnes - Tryo
Si tu es né dans une cité HLM
Je te dédicace ce poème
En espérant qu'au fond de tes yeux ternes
Tu puisses y voir un petit brin d'herbe
Et les man faut faire la part des choses
Il est grand temps de faire une pause
De troquer cette vie morose
Contre le parfum d'une rose
Refrain : C'est l'hymne de nos campagnes De nos rivières, de nos montagnes
De la vie man, du monde animal
Crie-le bien fort, use tes cordes vocales!
Pas de boulot, pas de diplômes
Partout la même odeur de zone
Plus rien n'agite tes neurones
Pas même le shit que tu mets dans tes cônes
Va voir ailleurs, rien ne te retient
Va vite faire quelque chose de tes mains
Ne te retourne pas ici tu n'as rien
Et sois le premier à chanter ce refrain{au Refrain}
Assieds-toi près d'une rivière
Ecoute le coulis de l'eau sur la terre
Dis-toi qu'au bout, hé ! il y a la mer
Et que ça, ça n'a rien d'éphémère
Tu comprendras alors que tu n'es rien
Comme celui avant toi, comme celui qui vient
Que le liquide qui coule dans tes mains
Te servira à vivre jusqu'à demain matin !{au Refrain}
Assieds-toi près d'un vieux chêne
Et compare le à la race humaine
L'oxygène et l'ombre qu'il t'amène
Mérite-t-il les coups de hache qui le saigne?
Lève la tête, regarde ces feuilles
Tu verras peut-être un écureuil
Qui te regarde de tout son orgueil
Sa maison est là, tu es sur le seuil...{au Refrain}
Peut-être que je parle pour ne rien dire
Que quand tu m'écoutes tu as envie de rire
Mais si le béton est ton avenir
Dis-toi que c'est la forêt qui fait que tu respires
J'aimerais pour tous les animaux
Que tu captes le message de mes mots
Car un lopin de terre, une tige de roseau
Servira la croissance de tes marmots !{au Refrain}

Cette chanson, sur un tempo plutôt tranquille dénonce également tout ce que l'on peut faire à la nature qui nous entoure. Tryo essaie ici de réveiller un peu les consciences afin que nous nous rendions compte que l'Homme doit vivre avec la nature et qu'il vit déjà grâce à cette même nature.
Leur clip fait le parallèle entre la vie d'avant et celle de maintenant. Le clip commence par l'agriculture que l'on avait il y a 50 ans, une agriculture raisonnée pour se nourrir et on passe d'un coup sur l'image de l'agro-alimentaire qui tuent plus d'animaux pour se nourrir que l'on en a besoin.
Ils soulignent l'absurdité de notre vie comparée à la vie plus simple que l'on pourrait avoir. Ils comparent 2 modes de vies à l'opposée : celle de la vie en ville et celle plus proche de la nature.


Aux arbres citoyens - Yannick Noah

Le ciment dans les plaines
Coule jusqu'aux montagnes
Poison dans les fontaines,
Dans nos campagnes
De cyclones en rafales
Notre histoire prend l'eau
Reste notre idéal
"Faire les beaux"
S'acheter de l'air en barre
Remplir la balance
Quelques pétrodollars
Contre l'existence
De l'équateur aux pôles,
Ce poids sur nos épaules
De squatters éphémères...
Maintenant c'est plus drôle
Puisqu'il faut changer les choses
Aux arbres citoyens !
Il est grand temps qu'on propose
Un monde pour demain !
Aux arbres citoyens
Quelques baffes à prendre
La veille est pour demain
Des baffes à rendre
Faire tenir debout
Une armée de roseaux
Plus personne à genoux
Fais passer le mot
C'est vrai la terre est ronde
Mais qui viendra nous dire
Qu'elle l'est pour tout le monde...
Et les autres à venir...
Puisqu'il faut changer les choses
Aux arbres citoyens !
Il est grand temps qu'on propose
Un monde pour demain !
Puisqu'il faut changer les choses
Aux arbres citoyens !
Il est grand temps qu'on s'oppose
Un monde pour demain !
Aux arbres citoyens
Plus le temps de savoir à qui la faute
De compter sur la chance ou les autres
Maintenant on se bat...
Avec toi, moi j'y crois...
Puisqu'il faut changer les choses
Aux arbres citoyens !
Il est grand temps qu'on propose
Un monde pour demain !
Avec toi,moi j'y crois...

Ici, Yannick Noah, fait un parallèle entre le monde capitaliste et les peuples qui y sont éloignés. Il montre dans son clip à l'allure d'un dessin-animé que le capitalisme détruit le mode de vie de peuple qui vivent en harmonie avec la nature.


What I've done - Linkin Park

In this farewell, there's no blood
Dans cet adieu, il n'y a pas de sang
There's no alibi, cause I've drawn regret
Il n'y a pas alibi, parce que je tire du regret
From the truth of a thousand lies
De la vérité, des milliers de mensonges
So let mercy come, and wash away
Alors laisse venir la pitié laver tout ça


[CHORUS : ]
[REFRAIN : ]
What I've done
Qu'ai-je fait !
I'll face myself
Je me ferais face
To cross out
Pour rayer
What I've become
Ce que je suis devenu
Erase myself
Je m'efface
And let go of
Et abandonne
What I've done
Ce que j'ai fait

Put to rest what you thought of me
Enterre ce que tu pensais de moi,
While I clean this slate
Pendant que je fais table rase
With the hand of uncertainty
Avec les mains de l'incertitude
So let mercy come, and wash away
Alors laisse venir la pitiélaver tout ça

[CHORUS]
[REFRAIN]

For what I've done
Pour ce que j'ai fait,
I'll start again
Je recommencerais
And whatever pain
Et quoi qu'il m'en coûte,
May come
Cela s'arrête aujourd'hui
Today this ends
Miséricorde pour ce que j'ai fait !
I'm forgiving

[REFRAIN]
[CHORUS]

Ce que j'ai fait
What I've done
(nananannana... )
(nananannana... )
Pardonnez ce que j'ai fait
Forgiving what I've done
(nananannana... )
(nananannana... )

Dans cette chanson, Linkin Park fait le mea culpa de l'espèce humaine. Ils se mettent en position de coupables face à tout ce qui a été fait contre la planète et contre la vie sur terre en général.
Leur clip commence par la nature et la vie animale. Les images suivantes sont la vie de fous que l'on mène, avec de la circulation de véhicules à gogo et des grandes villes avec des immeubles tellement hauts et entourés de pollution. Une pollution si dense que l'on ne voit pas à travers. On ne distingue que le haut des bâtiments, qui sont tellement hauts, qu'ils passent à travers cette pollution.
Dans ce clip, il y a des images successives qui font le contraste entre la nature et la destruction de cette naure par l'homme. On y voit aussi des pans d'histoires avec le Ku Klux Klan et les Nazis entre-autre. La drogue et la modification génétique y est aussi mentionné.


J'avais envie de partager avec vous ces paroles de chansons qui ne sont pas issues de livres ou de romans mais qui ont le mérite d'être aussi de la littérature à mes yeux et qui portent des messages importants parfois. Et qui peuvent être de bons points de départ pour en faire un débat ou pour en discuter.



L'intervention de Patricien en détail :
![pat_pyt](/img/club-lecture/s2-25-mai/pat_pyt1.png "Pat_pyt1")  
![pat_pyt2](/img/club-lecture/s2-25-mai/pat_pyt2.png "Pat_pyt2")  
