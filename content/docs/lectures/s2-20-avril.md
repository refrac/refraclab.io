---
title: Séance du 20 avril 
description: L'Art de la lecture, et un nouvel intervenant !
weight: -20210420
---

# Séance du 20 avril

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/6257a18d-3124-4dc7-ba2e-0ffa690e8c84" frameborder="0" allowfullscreen></iframe>


Myriam continue Etienne Chouard (1956-) - _Notre Cause Commune : instituer nous-mêmes la puissance politique qui nous manque_  
![chouard](/img/club-lecture/s2-13-avril/chouard.png "CHOUARD")   

[13:57](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=13m57s) Hugues Bazin lit son article [_L’éducation populaire, un tiers espace à redéfinir et reconquérir_](http://recherche-action.fr/hugues-bazin/2021/04/12/leducation-populaire-un-tiers-espace-a-redefinir-et-reconquerir/)

[32:00](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=32m) Bountar : [_Dénoncer scandales et corruption, est-ce faire le lit du fascisme ?_](https://fr.theanarchistlibrary.org/library/joao-bernardo-denoncer-scandales-et-corruption-est-ce-faire-le-lit-du-fascisme) - João Bernardo, mars 2009. Traduit par Y.C. pour mondialisme.org  
Liens pour plus d'informations concernant les scandales politico-financiers évoqués à la fin de la lecture :
http://archives.investir.fr/2007/jdf/20070721ARTHBD00070-le-temps-des-scandales-hanau-oustric-et-stavisky.php  
https://en.wikipedia.org/wiki/Barmat_scandal  
https://fr.wikipedia.org/wiki/Affaire_Sklarek 


[50:00](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=50m)  Greg : Michel Onfray (1959-) [_Éloge de la Commune_](https://michelonfray.com/interventions-hebdomadaires/eloge-de-la-commune?mode=text)

[1:15:11](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=1h15m11s) NoPesto poursuit [Sun Tzu _L’Art de la guerre_ (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Chapitre 4   
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

[1:30:15](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=1h30m15s) Patricien se penche sur [_Le joueur d’échecs_ par Stefan Zweig (1881-1942)](https://www.etudier.com/fiches-de-lecture/le-joueur-d-echecs/)

[1:52:02](https://canard.tube/videos/watch/6257a18d-3124-4dc7-ba2e-0ffa690e8c84?start=1h52m2s) Raphaël : par Georg W. F. Hegel (1770-1831), _La raison dans l'Histoire_
