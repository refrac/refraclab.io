---
title: Séance du 19 janvier 
description: Neuroscience, frites et condamnation à mort
weight: -20210119
---

Émile Henry - _Déclaration d’Émile Henry à son procès_ par Bountar ;
_Du Ketchup dans les veines_ - Hélène Weber par Greg ;
Sébastien Bohler _Le Bug humain_ par Raul
