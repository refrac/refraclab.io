---
title: Séance du 1er juin
description: Rouge, barbarie française, décroissance, l'atelier du vagin, Thomas Sankara 
weight: -20210601
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/ed219e73-72fa-4ac8-83be-aae2c67835fe" frameborder="0" allowfullscreen></iframe>

# Séance du 1er juin

Patricien lit [_Rouge_](http://clg-antoine-meillet-chateaumeillant.tice.ac-orleans-tours.fr/eva/sites/clg-antoine-meillet-chateaumeillant/IMG/pdf/rouge.pdf) 

12:00 Bountar lit d'Emile Pouget (1860-1931) : [Barbarie française](https://fr.theanarchistlibrary.org/library/emile-pouget-barbarie-francaise)

20:00 Greg lit de Serge Latouche (1940-),la théorie de la décroissance conçue sous l'angle de ["l'abondance frugale comme art de vivre"](https://www.cairn.info/revue-projet-2011-5-page-160.htm)     
![Latouche](/img/club-lecture/s2-27-avril/latouche.png "Latouche")  

30:40 Myriam lit L'atelier du vagin tiré des _Monologues du Vagin_ d'Eve Ensler (1953-)

55:56 Galyem lit un discours de Thomas Sankara (1949-1987) donné à Dori le 23 septembre 1983 : [_le Diagnostic de la vérité_](https://www.thomassankara.net/le-diagnostic-de-la-verite-discours-de-thomas-sankara-a-dori-le-23-septembre-1983/)

  



