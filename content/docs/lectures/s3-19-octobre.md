---
title: Séance du 19 octobre
description: Crise politique, un mois avec Manu, les capitalistes verts, le Neveu de Rameau
weight: -20211019
---

# Séance du 19 octobre 2021

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="2021-10-19 - Crise politique, un mois avec Manu, les capitalistes verts, le Neveu de Rameau" src="https://canard.tube/videos/embed/fd55c76b-9506-4836-b2fb-9c02e9d6937b" frameborder="0" allowfullscreen></iframe>

Bountar entame avec un article intitulé ["Crise du politique" ou inadaptation des partis ?](https://gallica.bnf.fr/ark:/12148/bpt6k442072g.image.r=jesus.f27.pagination.langFR) rédigé par Colette Ysmal et publié en septembre 1990  

15:16 Mathieu présente son propre article [Comment communique le Président ? Une ébauche de réponse : un mois avec Manu](https://ptvirgule.hypotheses.org/2346) 

53:05 Greg nous lit le journal : un article issu de [_La Décroissance_](http://www.ladecroissance.net) : La primaire des « idiots utiles » (verts) du capitalisme libéral, par Vincent Cheynet  

1:16:45 Raphaël lit des extraits des écrits politiques de Jean-Jacques Rousseau (1712-1778) et du _Neveu de Rameau_ de Denis Diderot (1713-1784) en abordant à nouveau la tension entre nature et culture

Liens posés durant la séance :  
https://ptvirgule.hypotheses.org/ (tous les articles de Mathieu sur le site Hypotheses.org)  
https://fr.wikipedia.org/wiki/Trois_lois_de_la_robotique (lois de la robotique selon Isaac Asimov)  
https://www.alain-bensoussan.com/droit-des-robots/ (droit des robots)  
https://www.assemblee-nationale.fr/dyn/15/textes/l15b2585_proposition-loi# (proposition de loi constitutionnelle relative à la Charte de l’intelligence artificielle et des algorithmes)
