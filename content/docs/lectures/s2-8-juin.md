---
title: Séance du 8 juin
description: De Maistre et Rousseau, Constitution 1958, Abu-Jamal et impérialisme, patrimoine et mont St Michel, enfance
weight: -20210608
---

# Séance du 8 juin

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="2021-06-08 De Maistre et Rousseau, Constitution 1958, Abu-Jamal et impérialisme, patrimoine et mont St Michel, enfance" src="https://canard.tube/videos/embed/8519a235-636c-4166-a6c2-8c1be81e673e" frameborder="0" allowfullscreen></iframe>

Patricien commence avec [_Deus sive populus_ Joseph de Maistre, Jean-Jacques Rousseau et la question de la souveraineté](http://juspoliticum.com/article/Deus-sive-populus-Joseph-de-Maistre-Jean-Jacques-Rousseau-et-la-question-de-la-souverainete-791.html) par Michaël Rabier

20:00 Myriam entame [la Constitution de 1958](https://fr.wikisource.org/wiki/Constitution_du_4_octobre_1958)

44:50 Bountar lit : [Juste une autre forme d'impérialisme](https://fr.theanarchistlibrary.org/library/mumia-abu-jamal-juste-une-autre-forme-d-imperialisme) par Mumia Abu-Jamal (1954-)

57:35 Greg lit un article de Science et Vie Hors série histoire et civilisation Patrimoine "Les monuments dans la tourmente de l'histoire de France" 

1:15:05 Raphael évoque des souvenirs d'enfance
