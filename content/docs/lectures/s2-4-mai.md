---
title: Séance du 4 mai
description: La voix de la Femme en Argentine, Abstention, Décroissance, Abstention, Joie, Jésuites...
weight: -20210504
---

# Séance du 4 mai 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/85197f6e-e4e6-43ac-b003-e8674143dddc" frameborder="0" allowfullscreen></iframe>

[I](https://canard.tube/videos/watch/85197f6e-e4e6-43ac-b003-e8674143dddc?start=5s) Myriam entame avec [« La Voz de la mujer » (« La Voix de la femme »)](https://fr.wikipedia.org/wiki/La_Voix_de_la_femme) - publication féministe communiste-anarchiste de la fin du XIXe en Argentine.

Le 8 janvier 1896, un groupe de femmes principalement animé par Virginia Bolten, faisait paraître, à Buenos Aires, en Argentine, le premier numéro d’une « La Voz de la mujer »

[II](https://canard.tube/videos/watch/85197f6e-e4e6-43ac-b003-e8674143dddc?start=14m45s) Bountar envoie deux beaux et courts textes :   
[Blasphegme - Je ne vote pas
JE N'ABDIQUERAI JAMAIS MA LIBERTÉ !
LA RÉVOLTE PLUTÔT QUE LA PASSIVITÉ DU VOTE ! 
](https://fr.theanarchistlibrary.org/library/blasphegme-je-ne-vote-pas)  
 
et   

[Errico Malatesta - Deuil ou fête](https://fr.theanarchistlibrary.org/library/errico-malatesta-deuil-ou-fete)  

[III](https://canard.tube/videos/watch/85197f6e-e4e6-43ac-b003-e8674143dddc?start=21m45s) Greg continue avec Serge Latouche (1940-), entamé la semaine passée et la théorie de la décroissance conçue sous l'angle de ["l'abondance frugale comme art de vivre"](https://www.cairn.info/revue-projet-2011-5-page-160.htm)     
![Latouche](/img/club-lecture/s2-27-avril/latouche.png "Latouche")  

[IV](https://canard.tube/videos/watch/85197f6e-e4e6-43ac-b003-e8674143dddc?start=41m42s) Raphael lit la fin de _Vous me croirez si vous voulez_ par [le Professeur Choron](https://fr.wikipedia.org/wiki/Professeur_Choron)

[V](https://canard.tube/videos/watch/85197f6e-e4e6-43ac-b003-e8674143dddc?start=49m30s) Patricien : [Le serment secret des Jésuites](http://fede-eglises.com/Jesuites.htm)


