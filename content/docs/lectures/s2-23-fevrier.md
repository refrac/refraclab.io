---
title: Séance du 23 février 
description: Marx & Engels, origin
weight: -20210223
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/cc217f4b-c981-471e-a546-ce317ad60a06" frameborder="0" allowfullscreen></iframe>

Patricien réstitue le contenu [d'une affiche Mort aux juges ! Mort aux jurés !](https://placard.ficedl.info/article8854.html)  (1893) 

Bountar lit : [C.G. Jacqueline Les exploiteurs de la révolution](https://fr.theanarchistlibrary.org/library/c-g-jacqueline-les-exploiteurs-de-la-revolution) (1871) 

Greg lit [Devenez beau, riche et intelligent, avec PowerPoint, Excel et Word](http://pauillac.inria.fr/~weis/info/haladjian.pdf)

[NoPesto continue L’Art d’avoir toujours raison](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_X) d’Arthur Schopenhauer (1788-1860) 

Professeur N entame [Karl Marx, Friedrich Engels - Manifeste du parti communiste](​https://fr.wikisource.org/wiki/Manifeste_du_parti_communiste/Mermeix) (1847​​)

Raul présente le chapitre 6 de La Société contre l'État (1974) de Pierre Clastres
