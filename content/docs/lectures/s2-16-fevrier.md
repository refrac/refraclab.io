---
title: Séance du 16 février 
description: Anar', Pouchkine, Stratagèmes et Darwin
weight: -20210216
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/46778159-5296-41d8-bb59-918424b7e1dc" frameborder="0" allowfullscreen></iframe>

Bountar commence avec [Penelope Nin - Au delà de la loi (2001)](https://fr.theanarchistlibrary.org/library/penelope-nin-au-dela-de-la-loi)


Gяeg lit un beau texte traduit du russe [Je me rappelle — instant de grâce](https://arbrealettres.wordpress.com/2020/05/06/je-me-rappelle-instant-de-grace-alexandre-pouchkine/)  d'Alexandre Pouchkine (1799-1837)


NoPesto entame le [4e stratagème de l'Art d'avoir toujours raison d'Arthur Schopenhauer](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_IV) (1788-1860)


Raul explore [le chapitre 4 de Darwin (1809-1882) - La Filiation de l'homme et la sélection liée au sexe](https://fr.wikisource.org/wiki/La_Descendance_de_l’homme_et_la_sélection_sexuelle/04)

