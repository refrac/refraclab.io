---
title: Séance du 30 mars 
description: Chiens de garde, tracts, folie, gai savoir et la fin des Stratagèmes !
weight: -20210330
---

# Séance du 30 mars 2021 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/2a2e3b1d-dc70-4499-8ad9-fa38b5d18223" frameborder="0" allowfullscreen></iframe>

Myriam lit le début du livre de Serge Halimi [Les Nouveaux Chiens de garde (1997)](https://fr.wikipedia.org/wiki/Les_Nouveaux_Chiens_de_garde)  
![Chiens](/img/club-lecture/s2-30-mars2/chiens.png "Les Nouveaux Chiens de garde")

Bountar enchaine deux textes vigoureux : 
[Lucioles - La rue de l’avenir est une impasse](https://fr.theanarchistlibrary.org/library/lucioles-la-rue-de-l-avenir-est-une-impasse)  
![impasse](/img/club-lecture/s2-30-mars2/impasse.jpg)  
[Dans l’Etat le plus libre du monde... ](https://fr.theanarchistlibrary.org/library/dans-l-etat-le-plus-libre-du-monde)  
Tract trouvé dans la rue en 2010.
"Que le tumulte de la révolte s’abatte contre ce foutu monde de merde !"

Greg lit [l'aphorisme 125 du Gai Savoir de Nietzsche (1844-1900)](https://mecaniqueuniverselle.net/textes-philosophiques/nietzsche-gai.php)
![gaisavoir](/img/club-lecture/s2-30-mars2/gaisavoir.jpg "Gai Savoir")

NoPesto finit [L’Art d’avoir toujours raison d’Arthur Schopenhauer (1788-1860)](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Ultime_stratagème) - Ultime stratagème !  
![L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)](/img/club-lecture/s2-16-mars/schopenhauer.png "L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)")

Patricien lit [Le XVIIe siècle - La Folie comme écart](http://www.geopsy.com/memoires_theses/la_folie.pdf) qui évoque la thèse de Michel Foucault (1926-1984) [Histoire de la folie à l'âge classique](https://fr.wikipedia.org/wiki/Histoire_de_la_folie_%C3%A0_l%27%C3%A2ge_classique)

Raphaël lit issu des Fleurs du Mal de Charles Baudelaire (1821-1867) [La Cloche fêlée](https://fr.wikisource.org/wiki/Les_Fleurs_du_mal/1861/La_Cloche_f%C3%AAl%C3%A9e)
