---
title: Séance du 16 mars
description: Anarchisme et psychiatrie, politique et terrorisme, marxisme et stratagèmes !
weight: -20210316
---

# Séance du 16 mars 2021

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/5c43e73d-0581-451f-9f94-68c1bfcc5bfe" frameborder="0" allowfullscreen></iframe>

Bountar nous amène la lumière, qui s'échappe de la tête des fêlés, au travers d'un texte traitant de psychiatrie [Limite, reine de l'asile](https://fr.theanarchistlibrary.org/library/c-limite-reine-de-l-asile) - écrit par une personne ayant vécu l'internement psychiatrique en 2010 et en en restituant les effets produits sur elle.  
![Limite, la reine de l asile](/img/club-lecture/s2-16-mars/limite-reine-asile.png "Limite, la reine de l asile")

Greg entame la belle lecture du prologue de [Ainsi parlait Zarathoustra - Un livre pour tous et pour personne](https://texto.best/z/fr/01) de Friedrich Nietzsche  
![Zarathoustra](/img/club-lecture/s2-16-mars/zarathoustra.png "Zarathoustra")

NoPesto continue [L’Art d’avoir toujours raison d’Arthur Schopenhauer (1788-1860)](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_XXX) - Stratagèmes 30 à 32 - Arguments d'autorité et assimilés.  
![L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)](/img/club-lecture/s2-16-mars/schopenhauer.png "L Art d avoir toujours raison d Arthur Schopenhauer (1788-1860)")

Patricien présente puis lit avec l'intonation qui va bien comment s'instaurait, dans la jeune IIIe République, *Notre loi des Suspects* avec [Francis de Pressense, Léon Blum, Émile Pouget - Les Lois Scélérates de 1893-1894](https://fr.wikisource.org/wiki/Les_Lois_Sc%C3%A9l%C3%A9rates_de_1893-1894/Texte_complet) - débats pas si inactuels.  
![Les Lois Scélérates de 1893-1894](/img/club-lecture/s2-16-mars/lois-scelerates.png "Les Lois Scelerates de 1893-1894")

Princeps nous fait découvrir un extrait de [À la ligne : Feuillets d'usine](https://www.babelio.com/livres/Ponthus--la-ligne--Feuillets-dusine/1099039) de Joseph Ponthus, et sorti en 2020. Immersion dans le monde impitoyable de la crevette.  
![À la ligne - Feuillets d'usine](/img/club-lecture/s2-16-mars/ponthus-feuillets.png "À la ligne - Feuillets d'usine")

Raul continue dans la veine après [Clastres et sa Société contre l'Etat](https://fr.wikipedia.org/wiki/La_Société_contre_l'État) avec [Homo domesticus, Une histoire profonde des premiers États](https://fr.wikipedia.org/wiki/Homo_domesticus._Une_histoire_profonde_des_premiers_%C3%89tats), un ouvrage de l'anthropologue anarchiste James C. Scott publié en France en 2019, dont le titre original est *Against the Grain: A Deep History of the Earliest States* - Chapitre 5 - Contrôle des populations, servitude et guerre.  
![Homo domesticus](/img/club-lecture/s2-16-mars/homo-domesticus.png "Homo domesticus")  
![Societe contre l'Etat](/img/club-lecture/s2-16-marssociete-contre-etat.png "La Société contre l'État")  
  
![striatum](/img/club-lecture/s2-16-mars/striatum.png "striatum")  


[Le documentaire d'ARTE sur la mode](https://youtu.be/oYg8ujH_HgE)  

Livre sur les rapports entre striatum et les hommes : https://fr.wikipedia.org/wiki/Le_Bug_humain
