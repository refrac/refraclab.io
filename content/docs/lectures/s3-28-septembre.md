---
title: Séance du 28 septembre
description: L'anarchisme et la question sexuelle, STASI, l'Ordure, Kropotkine, Mort à crédit
weight: -20210928
---

# Séance du 28 septembre 2021

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="2021-09-28 - L&#39;anarchisme et la question sexuelle, STASI, l&#39;Ordure, Kropotkine, Mort à crédit" src="https://canard.tube/videos/embed/44d366f4-c699-4e69-b540-8c580ccfd44b" frameborder="0" allowfullscreen></iframe>

1:28 Bountar lit d'Emma Goldman [L'Anarchisme et la question sexuelle](https://fr.theanarchistlibrary.org/library/emma-goldman-l-anarchisme-et-la-question-sexuelle) paru en 1896

16:19 Greg nous parle de la STASI et de [La décomposition](https://fr.wikipedia.org/wiki/D%C3%A9composition_(minist%C3%A8re_de_la_S%C3%A9curit%C3%A9_d%27%C3%89tat))

42:00 Claire lit de Michel Guillou _L'ordure la vengeance_ article issu d'une brochure parue en 1986 par les éditions Centre Pompidou 

1:12:30 Conclusion des _Mémoires_ de Kropotkine

1:26:20 Raphaeël lit un bout de _Mort à crédit_ de Céline
