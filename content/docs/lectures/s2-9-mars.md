---
title: Séance du 9 mars
description: Lectures réfractaires du 9 mars 2021 - de la joie (de mourir) et de la bonne humeur (d'extrême droite)
weight: -20210309
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/2d2a2742-690f-454c-b5f0-da758e8b4f3c" frameborder="0" allowfullscreen></iframe>

Bountar nous apporte de la bonne humeur en vous lisant ce joyeux petit texte : [Aux aspirants suicidaires…](https://fr.theanarchistlibrary.org/library/aux-aspirants-suicidaires) 

Professeur N poursuit [Karl Marx, Friedrich Engels Manifeste du parti communiste (1847)](https://fr.wikisource.org/wiki/Manifeste_du_parti_communiste/Mermeix) (page 5)

NoPesto continue [L’Art d’avoir toujours raison d’Arthur Schopenhauer (1788-1860)](https://fr.wikisource.org/wiki/L’Art_d’avoir_toujours_raison/Stratagème_XXI) - Stratagèmes 21 à 29

Patricien lit  [Rivarol - Discours sur l'universalité de la langue française](https://www.axl.cefan.ulaval.ca/francophonie/Rivarol-Discours-universalite_fr.htm)

Raul termine le chapitre 11 (final) de La Société contre l'État (1974) de Pierre Clastres
