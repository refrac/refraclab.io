---
title: Séance du 13 avril
description: L'État, le plus froid des monstres froids
weight: -20210413
---

# Séance du 13 avril

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/d2b6a7e4-6a7b-44b9-99cd-8f56cc6bf21c" frameborder="0" allowfullscreen></iframe>

Myriam : Etienne Chouard (1956-) - Notre Cause Commune : instituer nous-mêmes la puissance politique qui nous manque  
![chouard](/img/club-lecture/s2-13-avril/chouard.png "CHOUARD")   

Bountar aborde 3 définitions issues de L’Encyclopédie anarchiste  - à retrouver [ici](https://fr.theanarchistlibrary.org/library/sebastien-faure-l-encyclopedie-anarchiste-a#toc3) : Absolutisme, Altruisme et Amélioration.  
![anar](/img/club-lecture/s2-13-avril/anar.png "ANAR")  

Greg dévoile une gestion de crise catastrophique (voir [ici](https://www.fun-mooc.fr/courses/course-v1:Paris2+09004+session07/about) pour une présentation d'un cours de 6 semaines intitulé "Gestion de crise" et prodigué par Panthéon-Assas Paris II) : [5 étapes pour planter sa communication de crise comme Lactalis](https://www.chefdentreprise.com/Thematique/marketing-vente-1027/Breves/etapes-planter-communication-crise-comme-Lactalis-327132.htm)  
![lactalis](/img/club-lecture/s2-13-avril/lactalis.png "lactalis")  

NoPesto poursuit [Sun Tzu L’Art de la guerre (IVe siècle avant notre ère)](https://fr.wikisource.org/wiki/L%E2%80%99Art_de_la_guerre) - Suite de la préface et Chapitre 4   
![suntzu](/img/club-lecture/s2-6-avril/suntzu.png "suntzu")  

Patricien vous plonge dans [Zarathustra 12 - Vom neuen Götzen - De la nouvelle idole](https://texto.best/z/fr/12) de Friedrich Wilhelm Nietzsche (1844-1900)  
Ainsi parlait Zarathoustra : « l’Etat, c’est le plus froid de tous les monstres froids »

Raphael lit l'entrée du 8 mai 1984 de M-É Nabe (1958-) [_Nabe's Dream_](https://wikinabia.com/Nabe%27s_Dream)  
![nabe](/img/club-lecture/s2-13-avril/nabe.png "Nabe's Dream")  
