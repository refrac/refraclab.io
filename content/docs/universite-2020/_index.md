---
title: Université confinée 2020
bookCollapseSection: true
weight: 20
description: Le Canard Réfractaire a organisé sa première édition de l’Université confinée sur Discord, avec Framasoft, Franck Lepage, Etienne Chouard, et plein d'autres intervenants. Conférences, tables rondes, débats.
images:
  - /img/univ-confine-2020.png
---

[![Affiche université confinée](/img/univ-confine-2020.png)](https://discord.gg/HaEpyY3)
# Les 5 et 6 Décembre 2020
## Université confinée du Canard Refractaire

**Le Canard Réfractaire** a organisé sa première édition de l’Université confinée sur Discord (événement pour célébrer la communauté réfractaire grandissante et les 50 000 abonnés Youtube).

**Au programme de l’Université:** conférences, ateliers, débats, discussions citoyennes ! La bonne ambiance était de mise!

Vous avez pu retrouver des thématiques telles que la parole citoyenne, les médias militants, l’environnement, l’autonomie alimentaire, la démocratie, la désobéissance civile ou l’émancipation de la société matérialiste pour retrouver une vie centrée sur le vivant.

Les invités présent étaient tous plus intéressants les uns que les autres avec des noms comme :

- Framasoft
- Frank Lepage
- Le Pixel Mort
- Le Canard Réfractaire
- Vincent Cespedes
- Olivier Azam
- Extinction rébellion
- Thomas Babord
- Diable Positif 
- Les Mutins de Pangée
- Le Sanglier Jaune
- Etienne Chouard
- ... et pleins d'autres encore!

**L’université a pris place sur le Discord du Canard Réfractaire** les samedi 5 et dimanche 6 décembre 2020 sur le [discord des refractaires](https://discord.gg/HaEpyY3).

## Programme

- [Retrouver tous les détails du programme](programme) et les replays des conférences [sur Youtube][5] ou [sur Peertube][6] de l'université

## FAQ

### Comment s'est déroulée l'université: présence sur le Discord et sur les platforms live de vidéo

Il y avait plusieurs moyens de suivre et de participer:

- pour la participation active, c'était sur le [discord][1]
- pour simplement suivre le live et apprécier les échanges et conférence, il y avait 4 endroits:
  - sur twitch sur https://www.twitch.tv/canardrefractaire où il était possible de poser des commentaires et des questions
  - sur peertube sur https://canard.tube en lecture seule, où il n'y avait pas d'interaction possible (fonctionnalité expérimentale - première mondiale d'un stream live Peertube)
  - sur youtube sur https://www.youtube.com/channel/UCQTkRwkHtHUL6OJB5SI201Q (les commentaires étaient désactivés)
  - sur youtube chez Kalee Vision https://www.youtube.com/channel/UCrHMeoPLDWw-_xfFMN-585g

Certaines sessions ont débordées un peu du programme et le live n'a couvert que la continuité du programme. Comme on s'attendait à ce que ca déborde un peu dans des canaux différents sur discord, pour ne rien louper et poursuivre un échange qui durrait plus longtement, il va fallait etre sur le discord.

### Est-ce que les sessions étaient enregistrées ?

Oui, toutes les sessions étaient enregistrées afin de pouvoir les partager par la suite, et de les publiés sur le [peertube du canard][2].

### Qui c'est qui à organisé tout ça ?

Cet événement a été organisé de manière collective par les membres actifs du Discord du Canard Réfractaire. Ils se sont donnés pour but de créer durant 2 jours des espaces d'échanges pour promouvoir l'éducation populaire.

Au départ Yohan  [cherchait des idées][4] pour fêter les 50k abonnés a sa chaîne youtube, fin octobre. Du coup on en discuté lors de notre discussion hebdomadaires sur le Discord le [dimanche 1 novembre][3] et on a organisé cet évenement. 


[1]: https://discord.gg/HaEpyY3
[2]: https://canard.tube
[3]: https://discord.lecanardrefractaire.org/docs/reunions/2020-11-01/
[4]: https://discord.com/channels/633753846791012352/699864966047596604/771776594787827722
[5]: https://www.youtube.com/channel/UCQTkRwkHtHUL6OJB5SI201Q/videos
[6]: https://canard.tube/

