---
title: Vulgarisation dessinée
weight: 5150
---

# Université du Canard Confiné 2020

## Esprit critique et vulgarisation dessinée

    Samedi 5 Décembre, 15h

Tommy Tahir créateur de la chaine le diable positif vous présente des dessins animés axés sur l'actu afin de comprendre comment les humains foutent tout en l'air !

---

- Intervenant: [Le Diable positif][1]
- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://www.youtube.com/channel/UCI_FnSJPNGd3Y3IL0PwD-NQ
[7]: https://canard.tube/videos/watch/bb458287-5879-4bb7-94c5-7c8e05950cf6
[8]: https://www.youtube.com/watch?v=dSxVsS14GhM
[9]: https://www.youtube.com/watch?v=8vlI2fzGBBc

---

<< [retour au programme](../programme)
