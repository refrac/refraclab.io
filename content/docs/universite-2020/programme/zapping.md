---
title: Zapping critique
weight: 6190
---

# Université du Canard Confiné 2020

## Mini-documentaire sur le zapping / Éducation média, Espace critique dans les médias

    Dimanche 6 Décembre, 19h

- Intervenant: [Le Pixel Mort][1]

Monteur vidéo depuis 2013, passionné par le paysage politique et audiovisuel français, j'ai officié sur la chaine "Zap Télé" jusqu'en août 2018.

Les neurones grillés par l'ingurgitation de milliers d'heures de télévision, devenu une machine à fabriquer du zapping, je décide de voler de mes propres ailes et de monter ma chaîne, "Le Pixel Mort", le 1er septembre 2018.

Disposant désormais d'une totale liberté dans la création de mes vidéos, j'ai l'intention de vous livrer une analyse au vitriol de notre société.

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]


[1]: https://www.youtube.com/channel/UCeXsZz8C5sUZe1YgDxR6j9A
[7]: https://canard.tube/videos/watch/6c3f0ae9-6f49-4e96-b76c-c468673a5f03
[8]: https://www.youtube.com/watch?v=Ki8BUJA8tJE
[9]: https://youtu.be/ah5idzYJCZk?t=3699

---

<< [retour au programme](../programme)
