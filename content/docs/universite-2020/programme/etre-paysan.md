---
title: Etre Paysan
weight: 6120
---

# Université du Canard Confiné 2020

## La cultivation de la vie: être paysan et être heureux & volontariat solidaire!

    Dimanche 6 Décembre, 12h


L’engagement de Sanglier Jaune s’organise en 2 parties :

**Résister autrement**

Votre Sanglier change de vie et rejoint une équipe dans la France profonde pour aider au développement de projets locaux qui nous ressemblent dans une démarche de lutte globale cohérente.
Des sortes de bases arrières, des lieux où nous voulons pouvoir accueillir et faire venir du monde à travers la France pour se former, s’autonomiser, se préparer, s’organiser.

**Les projets Vidéos**

Des reportages et des œuvres avec une marque de fabrique, imaginées comme de vraies passerelles pour s’impliquer et s’éveiller.
Intervenant: [Le Sanglier Jaune][1]

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://www.youtube.com/channel/UCIJffkItaprUTFmDNOMXYxA
[7]: https://canard.tube/videos/watch/412dcd3a-77a5-4765-9195-3b9371dd87a1
[8]: https://www.youtube.com/watch?v=KVHEZqlN4QM
[9]: https://youtu.be/-TqQ7zqO4Dk?t=3686
[17]: https://canard.tube/videos/watch/406ec864-bff8-4f98-997e-dd84d0caaac7
[18]: https://www.youtube.com/watch?v=QcDdHp9ZoYs

---

<< [retour au programme](../programme)
