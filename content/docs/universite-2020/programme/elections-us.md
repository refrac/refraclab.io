---
title: Elections US
weight: 6180
---

# Université du Canard Confiné 2020

## L’élection Présidentielle États-Unienne 2020 vue par un socialiste Américain

    Dimanche 6 Décembre, 18h

- Intervenant: Un militant socialiste américain

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[7]: https://canard.tube/videos/watch/def88dae-89f2-4aed-b35d-811d6da2e341
[8]: https://www.youtube.com/watch?v=xg3PKxuWk_s
[9]: https://www.youtube.com/watch?v=ah5idzYJCZk

---

<< [retour au programme](../programme)
