---
title: Bonus
weight: 6220
---

## Bicentenaire du texte de Charles Fourier

### Détérioration matérielle de la planète

Lecture de Gaëtan Fustec d'un texte qui remet en question la possibilité de la société de se changer de l'intérieur.

[Source: Texte de 92 pages](/pdf/deterioration-materielle-de-la-planete.pdf)

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[7]: https://canard.tube/videos/watch/812bfa8c-4a8b-40bf-a116-b4e3de4712e4
[8]: https://www.youtube.com/watch?v=dQRmTCZQ-t0
[9]: https://youtu.be/ah5idzYJCZk?t=13265

---

<< [retour au programme](../programme)
