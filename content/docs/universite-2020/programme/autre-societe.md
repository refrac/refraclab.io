---
title: Autre Société
weight: 5180
---

# Université du Canard Confiné 2020

## Penser la société autrement

    Samedi 5 Décembre, 18h

Partons du postulat que l'état prend forme dès lors que deux facteurs sont présents :

- ceux qui y sont favorables
- ceux qui s'y opposent.

Tentons de nous placer hors de cette vision bipolaire de la question.
Il vous est ici proposé de creuser l'émancipation d'un système/état en passant par différentes questions et en débattant ouvertement de chacune d'entre-elles (à voir selon le temps consacré à chaque question) :

- Peut-on exister par le discrédit ?
- Faut-il penser une alternative au système actuel, et si oui, faut-il la penser à l'échelle d'un état ?
- Dans l'hypothèse d'un "nouveau" modèle de société viable, peut-on résister au milieu de pays néo-libéralistes dont l'unique moteur est un productivisme aveugle ?
- Modèles alternatifs théoriques/pratiques connus
- Remise en question des assertions développées au cours des échanges.

L'objectif serait de définir un modèle de société qui soit adaptable à un maximum de contextes, ou à minima d'en dessiner certains traits, permettre de dégager des pistes pour aller plus loin sans nous perdre dans l'opposition brute et la réaction plutôt que l'action.

### Sources

- https://www.cairn.info/revue-du-mauss-2016-2-page-5.htm#
- https://fr.theanarchistlibrary.org/library/revolution-liquide
- https://www.revue-ballast.fr/le-municipalisme-libertaire-quest-ce-donc/
- https://fr.theanarchistlibrary.org/library/murray-bookchin-le-municipalisme-libertaire
- https://fr.theanarchistlibrary.org/library/pierre-kropotkine-la-federation-comme-moyen-d-union

---

- Intervenant: Bountar
- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[7]: https://canard.tube/videos/watch/d53c6b23-b603-4a62-8a1e-3335917fe28a?start=55m35s
[8]: https://youtu.be/oZjTpXfOFPA?t=3160
[9]: https://youtu.be/wNLot5uv1B0?t=25260
[17]: https://canard.tube/videos/watch/88395f86-2271-4479-bfcc-659bec3bacff
[18]: https://www.youtube.com/watch?v=IcopKesstMI

---

<< [retour au programme](../programme)
