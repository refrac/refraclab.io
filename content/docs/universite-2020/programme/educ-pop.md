---
title: Education Populaire
weight: 5130
---

# Université du Canard Confiné 2020


## L’éducation populaire: quoi, pourquoi, comment et les conférences gesticulées

    Samedi 5 Décembre, 13h

Franck Lepage sur [Youtube][1]

---

- Intervenant: Franck Lepage
- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://www.youtube.com/user/francklepage
[7]: https://canard.tube/videos/watch/a9fbb52a-d919-44bb-bc78-1100ba03577c
[8]: https://www.youtube.com/watch?v=tYSzy763pT8
[9]: https://www.youtube.com/watch?v=6otCWSPa1X8

---

<< [retour au programme](../programme)
