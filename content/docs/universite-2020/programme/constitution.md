---
title: Constitution et RIC
weight: 6140
---

# Université du Canard Confiné 2020

## 	Débat : Qui est légitime pour fixer les règles de la représentation (la Constitution) ? Les représentés ou les représentants ? Le RIC : oui ou non ?

    Dimanche 6 Décembre, 14h

- Intervenant: [Etienne Chouard][1]

Étienne Chouard est un enseignant, blogueur, essayiste et militant politique français. Défenseur du Référendum d'initiative citoyenne, il a contribué aux discussions autour du sujet. Il propose des ateliers de réécriture de la constitution et a incarné une figure connue du mouvement des gillets jaunes.

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://chouard.org
[7]: https://canard.tube/videos/watch/621aca00-0bec-4c88-bb58-b13ae65f2e25
[8]: https://www.youtube.com/watch?v=OxD_J6EXRw4
[9]: https://youtu.be/-TqQ7zqO4Dk?t=12565

---

<< [retour au programme](../programme)
