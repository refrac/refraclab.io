---
title: Action Militante
weight: 5160
---

# Université du Canard Confiné 2020

## De l’Action militante !

    Samedi 5 Décembre, 16h

- Intervenants: Nina et Antoine de [Extinction Rebellion][1]

Extinction Rebellion est un mouvement mondial de désobéissance civile en lutte contre l’effondrement écologique et le réchauffement climatique lancé en octobre 2018 au Royaume-Uni. "Nous sommes… vous ! Vous êtes invité.e.s à nous rejoindre et à participer, si vous êtes d’accord avec nos principes et nos valeurs."

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]


[1]: https://rebellion.global/
[7]: https://canard.tube/videos/watch/ebfbbb74-3e58-4aa5-94a0-d14742d57bf3
[8]: https://www.youtube.com/watch?v=Ywmk_jfTRpc
[9]: https://youtu.be/wNLot5uv1B0?t=15096
[17]: https://canard.tube/videos/watch/a8b81b32-74b6-4f21-bdd8-9ac4ca2fdea5
[18]: https://www.youtube.com/watch?v=_qonF6ieh0Q

---

<< [retour au programme](../programme)
