---
title: Alternatives Capitalisme
weight: 5170
---

# Université du Canard Confiné 2020

## *Table ronde*<br>Les modèles alternatifs au capitalisme: l’éducation populaire, et après … ?

    Samedi 5 Décembre, 17h

Table ronde ouverte à tous les intervenants

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[7]: https://canard.tube/videos/watch/d53c6b23-b603-4a62-8a1e-3335917fe28a
[8]: https://www.youtube.com/watch?v=oZjTpXfOFPA
[9]: https://youtu.be/wNLot5uv1B0?t=22569
[17]: https://canard.tube/videos/watch/88395f86-2271-4479-bfcc-659bec3bacff
[18]: https://www.youtube.com/watch?v=IcopKesstMI

---

<< [retour au programme](../programme)
