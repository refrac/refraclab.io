---
title: Droits et Luttes aux US
weight: 6170
---

# Université du Canard Confiné 2020

## 	Les droits civiques aux USA - Moyen de luttes actuels + Discussion

    Dimanche 6 Décembre, 17h

- Intervenant: Olivier Azam des [Mutins de Pangée][1]

Les mutins de Pangée est une coopérative audiovisuelle et cinématographique de production, d’édition et de distribution (en salles, DVD, VOD).

Olivier Azam est un réalisateur français de films documentaires. Il est également directeur de la photographie, acteur, monteur, compositeur et producteur de cinéma

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + (suite hors live sur [PeerTube][17])
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://www.lesmutins.org/
[7]: https://canard.tube/videos/watch/09ec72c8-3a94-4fae-ab41-b787d7c39197
[8]: https://www.youtube.com/watch?v=trOMTgR0TSU
[9]: https://youtu.be/-TqQ7zqO4Dk?t=21950
[17]: https://canard.tube/videos/watch/1298b92d-ac73-4db9-b5c1-789127119541
[18]: https://www.youtube.com/watch?v=zSzMEy8OU9k

---

<< [retour au programme](../programme)
