---
title: Conclusion
weight: 6210
---

# Université du Canard Confiné 2020

## Conclusion: on refait le monde

    Dimanche 6 Décembre, 21h

- Intervenant: Yohan du [Canard Réfractaire][1] et Gaëtan

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]


[1]: https://lecanardrefractaire.org
[7]: https://canard.tube/videos/watch/8e629fed-70f5-4481-b77b-4f998779001f
[8]: https://www.youtube.com/watch?v=LyIZTZl6Qak
[9]: https://youtu.be/ah5idzYJCZk?t=12462

---

<< [retour au programme](../programme)
