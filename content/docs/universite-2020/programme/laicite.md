---
title: Laïcité
weight: 5200
---

# Université du Canard Confiné 2020

## Religion, laicité, athéisme: on fait le point

    Samedi 5 Décembre, 20h

- Intervenant: [Thomas Babord][1]

Militant, youtuber et vulgarisateur politique. Ami du Canard.

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[1]: http://thomas-babord.surge.sh/
[7]: https://canard.tube/videos/watch/132b1770-c5fc-45c5-a6be-a9d77c75f1eb
[8]: https://www.youtube.com/watch?v=_LD7JtK338c
[9]: https://youtu.be/WWcglaOCJHo?t=3567
[17]: https://canard.tube/videos/watch/da0b7ed5-cdcc-4c7f-9431-865bbabb033a
[18]: https://www.youtube.com/watch?v=CDZuREdJ9Q4

---

<< [retour au programme](../programme)
