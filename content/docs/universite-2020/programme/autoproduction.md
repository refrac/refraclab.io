---
title: Autoproduction
weight: 5190
---

# Université du Canard Confiné 2020

## 	Autoproduction populaire

    Samedi 5 Décembre, 19h

- Intervenant: Yohan du [Canard Réfractaire][1]

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > -  sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://lecanardrefractaire.org
[7]: https://canard.tube/videos/watch/736b7be9-9dca-44e2-8e58-39571b5ee162
[8]: https://www.youtube.com/watch?v=W2C8btDE5xs
[9]: https://youtu.be/WWcglaOCJHo?t=33
[17]: https://canard.tube/videos/watch/d0757cae-9cb7-49df-99f9-8a193f2b4ea6
[18]: https://www.youtube.com/watch?v=Jne4SZUzU6g

---

<< [retour au programme](../programme)
