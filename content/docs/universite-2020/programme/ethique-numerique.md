---
title: Ethique Numérique
weight: 5140
---

# Université du Canard Confiné 2020

## Comment retrouver mes valeurs éthiques dans mes outils numériques ?

    Samedi 5 Décembre, 14h

Alors que notre environnement se numérise de plus en plus et qu'il semble devenir indispensable d'utiliser des outils numériques au quotidien, une question se pose : Qu'en est-il de l'éthique de tout cela ?

Nos données, personnelles ou non, avivent les marchés du monde entier, que l'économie de l'attention semble se situer dans tous les recoins d'internet, comment pouvons-nous aborder le numérique en y associant nos principes moraux ?

Cette question sera abordée sous la forme d'un question-réponse, en compagnie des copines et copains de Framasoft.

### A propos de Framasoft

Framasoft est un réseau d'éducation populaire créé en novembre 2001 par Alexis Kauffmann, Paul Lunetta, et Georges Silva, et soutenu depuis 2004 par l'association homonyme. Consacré principalement au logiciel libre, il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne. [ref wikipedia][2]

---

- Intervenant: [Framasoft][1]
- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]


[1]: https://framasoft.org
[2]: https://fr.wikipedia.org/wiki/Framasoft
[7]: https://canard.tube/videos/watch/657647d9-7db1-4849-89f8-4eedfd794cf8
[8]: https://www.youtube.com/watch?v=mpCokMTKNao
[9]: https://youtu.be/wNLot5uv1B0?t=11564
[17]: https://canard.tube/videos/watch/e6fff8f1-1e2f-4a68-beeb-f931ed9b4a22
[18]: https://www.youtube.com/watch?v=MLk2VH1B-Rs

---

<< [retour au programme](../programme)
