---
title: Le cas Assange
weight: 5210
---

# Université du Canard Confiné 2020

## La crucifixion de Julian Assange

    Samedi 5 Décembre, 21h

- Intervenant: Viktor Dedaj

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[7]: https://canard.tube/videos/watch/36e180fd-41dc-4c83-ac93-b373b6ff58b3
[8]: https://www.youtube.com/watch?v=a-UedmqKPpo
[9]: https://youtu.be/WWcglaOCJHo?t=6824

---

<< [retour au programme](../programme)
