---
title: Démocratie et Utopie
weight: 6160
---

# Université du Canard Confiné 2020

## 	Table ronde: Débat réflexion avec les conférenciers: On fait quoi ensuite

Conferenciers: DataGueule, Etienne Chouard, Alexandre Duclos

Democratie, information, militantisme, utopie

    Dimanche 6 Décembre, 16h

- Intervenants:
  - [DataGueule][1] (n'a pas pu venir)
  - [Alexandre Duclos][2]
  - [Etienne Chouard][3]

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://lecanardrefractaire.org
[2]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
[3]: https://chouard.org/
[7]: https://canard.tube/videos/watch/621aca00-0bec-4c88-bb58-b13ae65f2e25?start=1h31m
[8]: https://www.youtube.com/watch?v=Nh9doT89Uek
[9]: https://youtu.be/-TqQ7zqO4Dk?t=17871

---

<< [retour au programme](../programme)
