---
title: Securité Alimentaire
weight: 6100
---

# Université du Canard Confiné 2020

## La sécurité sociale alimentaire, le hacking territorial & présentation du week-end SSA

    Dimanche 6 Décembre, 11h

- Intervenant: Invivo et Denved ar vro des [Réfractaires][1]

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://discord.lacanardrefractaire.org/
[7]: https://canard.tube/videos/watch/7b0ee27f-006f-465d-82b3-08a0c14b553b
[8]: https://www.youtube.com/watch?v=NldAwRp-aGM
[9]: https://youtu.be/-TqQ7zqO4Dk?t=43

---

<< [retour au programme](../programme)
