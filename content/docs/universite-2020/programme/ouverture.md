---
title: Ouverture
weight: 5100
---

# Université du Canard Confiné 2020

## Ouverture et accueil

    Samedi 5 Décembre, 10h

### Accueil et mise en place

A 10h nous commencerons à ouvrir les canaux vocaux, et l'équipe des réfractaires, aidé des petits débrouillards, sera à votre disposition pour régler les aspects techniques. Peut-être que discord est compliqué ? N'hésitez pas à demander de l'aide.

### Présentation de l'organisation du discord et de l'université

Vers 10h40 - Présentation de l'équipe orga formée sur le [discord réfractaire][2] et de comment nous avons organisé collectivement l'université populaire, par Yohan.

### Les petits Débrouillards

Le réseau des [Petits Débrouillards][3] participe du renouveau permanent de l'éducation populaire. Par une éducation aux démarches scientifiques, expérimentales et raisonnées, il contribue depuis 1986 à développer l’esprit critique, et à élargir les capacités d’initiatives de chacune et chacun. Son objectif est de permettre aux jeunes et moins jeunes de s’épanouir individuellement et collectivement, par des parcours de citoyenneté active et démocratique.

La formation Parcours Tremplin Numérique proposée par Les Petits Débrouillards est une formation pré-qualifiante pour accéder à l’emploi en passant par la construction et le suivi d'un projet professionnel, des rencontres avec des acteurs du secteur et des stages et le tout sur une durée de 6 mois.

Ce qui est proposé dans cette formation :
* Découverte des métiers numériques
* Formation technique (culture numérique, bureautique, création 2D/3D, etc.)

La formation est gratuite et proposée aux 16/29 ans sur Toulon et Marseille.

Le recrutement est encore ouvert pour l'année 2020.
Prochaine session en mai 2021.

---

- Intervenant: Yohan du [Canard Réfractaire][1]
- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://lecanardrefractaire.org
[2]: https://discord.lecanardrefractaire.org
[3]: https://www.lespetitsdebrouillards.org/
[7]: https://canard.tube/videos/watch/792fa8fa-fe70-4cfb-bf4a-f3329ac7984d
[8]: https://www.youtube.com/watch?v=2gNKnpES7cI
[9]: https://www.youtube.com/watch?v=wNLot5uv1B0

---

<< [retour au programme](../programme)
