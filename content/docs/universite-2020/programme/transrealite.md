---
title: Transréalité
weight: 6130
---

# Université du Canard Confiné 2020

## Complotisme & cybermodernité



    Dimanche 6 Décembre, 13h

- Intervenant: [Vincent Cespedes][1], philosophe

Vincent Cespedes est un philosophe et un essayiste français. Entre l'intime et le politique, sa pensée s'organise autour de trois axes : la création de sens, la quête de l'efficience interpersonnelle et la critique sociale. 

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires
  > - sur la chaine live des refractaires sur [Youtube][8]
  > - chez Kalee Vision sur [Youtube][9]

[1]: https://www.youtube.com/user/Zapatoustra
[7]: https://canard.tube/videos/watch/9d557422-c728-487c-84db-5481f95d4abf
[8]: https://www.youtube.com/watch?v=iY6pzJ8GnvA
[9]: https://youtu.be/-TqQ7zqO4Dk?t=7251

---

<< [retour au programme](../programme)
