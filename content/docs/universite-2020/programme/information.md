---
title: Emancipation Information
weight: 6200
---

# Université du Canard Confiné 2020

## S’émanciper de l’information

    Dimanche 6 Décembre, 20h

[Préambule (brouillon) : S'émanciper de l'information (PDF - 30 pages)](/pdf/Universite_des_Canards_Refractaires_Semanciper_de_linformation_ADuclos.pdf)

https://www.youtube.com/playlist?list=PLID1ompbJIzTdUFw1ofkXkA3WrQc6d8wa


- Intervenant: [Alexandre Duclos][1]

---

- Enregistrements (chaque enregistrement est différent, si l'un est imparfait, l'autre peut ne pas l'être):

  > - sur le [PeerTube][7] live des refractaires + suite hors live sur [PeerTube][17]
  > - sur la chaine live des refractaires sur [Youtube][8] + suite hors live sur [Youtube][18]
  > - chez Kalee Vision sur [Youtube][9]


[1]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
[7]: https://canard.tube/videos/watch/b9cb7e9b-5e30-44a5-82f6-e1a84d5b6e91
[8]: https://www.youtube.com/watch?v=IYGQ-wegMDo
[9]: https://youtu.be/ah5idzYJCZk?t=7440
[17]: https://canard.tube/videos/watch/07f6288b-fa02-46d7-a357-e3297fb8ecf3
[18]: https://www.youtube.com/watch?v=aF0mpyNT4b0

---

<< [retour au programme](../programme)
