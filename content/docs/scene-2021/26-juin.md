---
title: 26 Juin - Solitude et Servitude
menutitle: "<b>26 Juin</b>  Anthroplogie Réfractaire #4  Solitude et Servitude"
description: "La Fondation de France publie son 10e rapport annuel sur les solitudes. Cette étude révèle une forte augmentation de l’isolement relationnel au sein de la population française au cours des 10 dernières années : 7 millions de personnes se trouvent en situation d'isolement, soit 14 % des Français, contre 9 % en 2010.  Sommes-nous confrontés à une épidémie de solitudes en France ?"
weight: -20210626
---
# Vendredi 26 Juin à 21h

<div style="text-align:center">

**Annonce (1min)**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="[Teaser] Anthropologie Réfractaire #4 - Solitudes et servitudes" src="https://canard.tube/videos/embed/01c9c691-6a65-41a8-9276-d6a1ed51455d" frameborder="0" allowfullscreen></iframe>

</div>

## Anthropologie réfractaire #4 - Solitude et Servitude : petit kit de survie morale à l'ère lacrymo-liberale

D'un côté, il y a des faits, objectivables :"La Fondation de France publie son 10e rapport annuel sur les solitudes. Cette étude révèle une forte augmentation de l’isolement relationnel au sein de la population française au cours des 10 dernières années : 7 millions de personnes se trouvent en situation d'isolement, soit 14 % des Français, contre 9 % en 2010.  Sommes-nous confrontés à une épidémie de solitudes en France ?"

L'évolution des structures familiales en France décrit elle aussi une extension du domaine de la solitude. Et d'un autre côté, la nécessité de comprendre cette solitude paradoxale, dans un monde omni-connecté, dans une société dans laquelle nous sommes de plus en plus accolés les uns aux autres, superposés, de plus en plus interdépendants, de plus en plus bombardés d'informations et de sollicitations en tout genre. 

Innombrables, agglutinés, sans cesse médiatisés, mais seuls. Qu'est-ce que cela veut dire? Et à rebours, qu'est-ce que cela nous dit de ce que c'est que la solitude?

Ressources: 

https://www.fondationdefrance.org/fr/7-millions-de-francais-confrontes-la-solitude-decouvrez-notre-enquete-annuelle

https://www.insee.fr/fr/statistiques/5016913

https://www.insee.fr/fr/statistiques/2381514#figure1_radio2 

Autres références :
Friedrich Nietzsche, [Ecce homo, traduction par Alexandre Vialatte][3]

Fabrice Humbert ["Les mots pour le dire" éditions Gallimard, 2021][1] 

William Wordworth "I wandered lonely as a cloud"

André Gorz, [Eloge du suffisant, PUF, 2019][2]

Il sera aussi largement question d'Aragorn, de Frodon, de Bilbon, de Jon Snow, de Wolverine, d'Achille et d'Ulysse

[1]:http://www.gallimard.fr/Catalogue/GALLIMARD/Tracts/Les-Mots-pour-le-dire
[2]:https://www.puf.com/content/%C3%89loge_du_suffisant
[3]:https://www.oeuvresouvertes.net/spip.php?article818
