---
title: 26 février - Résignation, renoncement, des postures politiques ?
menutitle: "<b>26 février</b>  Anthroplogie Réfractaire #1  Résignation, renoncement,  des postures politiques ?"
description: Alexandre Duclos présente une analyse anthropologique de l'écart abyssal qui sépare la résignation du renoncement. Il nous amènera à cette occasion dans une traversée inattendue de notre quotidien social et politique. On verra notamment la puissance de la logique de renoncement comme échappatoire à la fabrique du consentement et peut-être ira-t-on au passage explorer le couple jamais affiché acceptabilité-inacceptabilité.
weight: -20210226
images:
  - /img/resignation_1200x630.jpg
---

# Vendredi 26 Février à 20h

<div style="text-align:center">

**Annonce**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/e54b93f7-53f2-41fd-9f04-fda08e8cf6a9" frameborder="0" allowfullscreen></iframe>

**Replay**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/f246dcc4-4736-49dc-8d97-400984336140" frameborder="0" allowfullscreen></iframe>
</div>

## Anthropologie réfractaire #1 - Résignation, renoncement, des postures politiques ?

Alexandre Duclos présente une analyse anthropologique de l'écart abyssal qui sépare la résignation du renoncement. Il nous amènera à cette occasion dans une traversée inattendue de notre quotidien social et politique. On verra notamment la puissance de la logique de renoncement comme échappatoire à la fabrique du consentement et peut-être ira-t-on au passage explorer le couple jamais affiché acceptabilité-inacceptabilité.

## Qu'est-ce que l'Anthropologie réfractaire?

Nous avons reçu Alexandre Duclos lors de l'université confinée 2020 et son regard de sociologue anthropologue sur la situation politique et sociale du moment propose une profondeur de réflexion peu courante dans les média militants.

Nous avons donc décidé de renouveler l'expérience sous un format que nous espérons mensuel, où pendant une paire d'heure Alexandre proposera une réflexion de son cru, et se tiendra à la disposition des auditeurs pour des échanges libre sur le thème du moment.

## Qui est Alexandre Duclos?

Enseignant à Paris 1 Panthéon Sorbonne, il est sociologue et anthropologue. Il diffuse depuis plusieurs années ses cours en ligne ainsi que des diffusions d'opinions, de pensées et d'analyses [sur sa chaine youtube][ad].
## Comment participer?

Le plus simple est de nous rejoindre sur le [discord des réfractaires](https://discord.gg/HaEpyY3) dans les canaux de la Scène Réfractaire. Un canal vocal permettra d'écouter le live, et ceux qui en exprimeront le désir seront invité en studio pour interagir, poser des questions ou demander des précisions. Il sera possible de poser des questions à l'écrit à cet endroit également.

En plus du live dans discord, la session sera diffusée sur notre peertube en direct (ou en fait avec un léger différé de 30 secondes). Passez voir https://canard.tube au moment du live. Mais Peertube ne permet pas (encore) de réagir en live dans un chat, donc si vous voulez intéragir, venez donc plutôt sur le discord.

## Est-ce qu'il y aura un replay?

La session sera disponible à la fin de l'événement sur https://canard.tube/ et sur la [chaine youtube d'Alexandre Duclos][ad].

- https://canard.tube/videos/watch/f246dcc4-4736-49dc-8d97-400984336140


[ad]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
