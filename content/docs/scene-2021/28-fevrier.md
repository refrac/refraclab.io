---
title: 28 février - Améliorer notre hygiène numérique sur Android
menutitle: "<b>28 février</b>  Améliorer notre hygiène numérique sur Android"
description: Ce Dimanche 28 Février 2021, à 16h, nous explorerons comment paramétrer nos téléphones Android afin d'en améliorer notre hygiène numérique.
weight: -20210228
images:
  - /img/hygiene-numerique.jpg
---

# Dimanche 28 Février à 16h

<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/61ff152a-5b33-464a-b133-bad35d227a10" frameborder="0" allowfullscreen></iframe>
</div>

## Hygiène Numérique<br>Améliorer notre hygiène numérique sur Android

Nous explorerons comment paramétrer nos téléphones Android afin d'en améliorer notre hygiène numérique.

En deuxième partie du live, nous explorerons comment supprimer/désinstaller les applications système (qu'on ne peut pas désinstaller normalement).

Comme d'hab', ça se fera à la bonne franquette, sans pression.

Venez suivre le live sur https://canard.tube/videos/watch/61ff152a-5b33-464a-b133-bad35d227a10
