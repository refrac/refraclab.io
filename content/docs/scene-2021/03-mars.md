---
title: 3 mars - Atelier communication non violente
menutitle: "<b>3 mars</b>  Communication non violente"
description: Greg et Thomas nous présentent les principes de la communication non violente
weight: -20210303
images:
  - /img/cnv-1200x630.png
---

# Mercredi 3 Mars à 21h

**Replay**
<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/3d5fcb37-c405-4801-b4ab-c6a0e1908cfb" frameborder="0" allowfullscreen></iframe>
</div>

## Atelier communication non violente

La vie que ce soit sur nos serveurs ou en physique est faite de rencontre, d'échange et de communication. Chacun a son style souvent hérité de son éducation. Et ca marche parfois plus ou moins bien. Ca dépend des gens et des situations.

Y a t-il des méthodes éprouvées qui permettent de mieux communiquer de manière sereine sans écraser les autres ni s'écraser soi-même ?

On donnera aussi de nombreux exemples de ce qui faut faire et pas faire ainsi que des recettes et canevas et leurs limites. On abordera aussi la négociation et la persuasion.

### Source des slides

- les [slides de la présentation au format pdf][1]

### Autres liens

- https://fr.wikipedia.org/wiki/Assertivit%C3%A9
- https://fr.wikipedia.org/wiki/Communication_non_violente
- une bonne présentation sur YouTube de Rosenberg l'inventeur du concept https://youtu.be/bIjRxdN-kL8
- [le blog de Thomas Babord][5]

### Retour sur la formation

    Après la formation, Thomas Babord à ré-écrit la formation pour mieux l'adapter au milieu millitant :

    https://thomas-babord.surge.sh/post/cnv-militantisme/

### Bouquins

- [Etre soi dans ses relations, Développer son assertivité en entreprise][2] (aussi en [ebook][3]), par Sylvie Grivel
- Pour comprendre la dynamique des relations: vous pouvez lire des livres d'analyse transactionnelle par exemple ["l'analyse transactionnelle" aux presses universitaires de France][4].

[1]: /pdf/Presentation_CNV_3Mars.pdf
[2]: https://www.leslibraires.fr/livre/7042373-etre-soi-dans-ses-relations-developper-son-ass--sylvie-grivel-eyrolles
[3]: https://www.leslibraires.fr/livre/7326221-etre-soi-dans-ses-relations-developper-son-ass--sylvie-grivel-editions-eyrolles
[4]: https://www.leslibraires.fr/livre/16229711-l-analyse-transactionnelle-gerard-chandezon-antoine-lancestre-presses-universitaires-de-france
[5]: http://thomas-babord.surge.sh/
