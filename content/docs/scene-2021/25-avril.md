---
title: 25 avril - Table ronde Réfractaire 
menutitle: "<b>25 avril</b>  Révolution agraire :  Initiatives citoyennes  et paysannes "
description: 
weight: -20210424
images:
  - /img/ssa-25-avril-1200x630.png
---

# Dimanche 25 avril à partir de 15h45

<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/1e6069e0-b783-400a-ae62-452e32cf5c9d?warningTitle=0" frameborder="0" allowfullscreen></iframe>
</div>

## Révolution agraire : Initiatives citoyennes et paysannes

Au programme de l’évènement

> - 16h - **Table ronde** autour de la Sécurité Sociale de l'Alimentation avec  
>   - [Bernard Friot][3] (Sociologue et économiste)  
>   - [Mathieu Dalmais][2] (Conférencier gesticulant)  
>   - [Julien Le Dem][4] ([Terre de liens][5])
> - 19h - **Micro ouvert** sur Discord  
>   Initiatives citoyennes et paysannes, parlez-nous de vos projets et idées

Depuis maintenant plus d'un an une équipe de Réfractaires se retrouvent sur Discord. Confinés à cette époque tout le monde imaginait un monde d'après. Les questions de santé, alimentation, environnement, agriculture, étaient  très souvent centrales dans nos échanges. Il est aisé de soulever de nombreux paradoxes et cercles vicieux que nous propose l'agriculture moderne.  
Comment peut-on être à la fois  excédentaire mais bien loin de l'autonomie alimentaire ? Quel est le réel impact de notre agriculture sur notre environnement ? Comment peut-on perdre 10% de producteurs par an ?  

Tant de questions qui en ramènent une première : pourquoi la quasi totalité des citoyens se retrouvent déracinés de l'agriculture ?  
Une réponse envisageable : se réapproprier les facteurs de productions de nos besoins essentiels.  

> Nous allons en discuter dimanche sur Discord.

Le constat est lourd, deux révolutions vertes nous ont conduit dans une situation désolante, l'exploitation des ressources par l'industrie agricole entraîne l'érosion du vivant en accentuant la dégradation du tissu social agricole. En plus d'être toujours aussi écocide l'élevage industriel est montré du doigt sur la question des pandémies... Les freins à une transition agricole sont nombreux mais quelques leviers apparaissent, et le confinement a permis de comprendre leurs portées. On a alors recommencé à entendre parler sérieusement de la Sécurité Social de l'Alimentation.  

Cette idée a été soufflée à Mathieu Dalmais, animateur syndical, par un paysan de la confédération paysanne : "Face au mur des décideurs nous ne pouvions plus compter sur les producteurs pour renverser la table. Il faut y impliquer les citoyens." Ce changement de paradigme en écho aux travaux de Bernard Friot, fait depuis le début du Covid son petit bonhomme de chemin.  

Parallèlement, forte de son soutien populaire, la résistance paysanne propose depuis longtemps de nouvelles façons de s'installer dans l'agriculture, de produire et de s'organiser collectivement.  
Les citoyens ne sont pas en reste en s'impliquant avec le peu d'outils en leurs mains pour peser sur les orientations agricoles, proposer des scénarii alternatifs. Le hacking territorial a de beaux jours devant lui...  

## Les invités

> Bernard Friot - Sociologue et économiste  
> Mathieu Dalmais - Conférencier gesticulant  
> Julien Le Dem - Éleveur breton et co-président de Terre de liens Bretagne (animateur)  

## Participer

- La table ronde sera diffusée en live sur https://canard.tube/video-channels/educ_pop
- pour participer en vocal sur discord https://discord.gg/HaEpyY3
- pour participer en chat textuel, sur discord ou avec Elements (matrix) https://matrix.to/#/#refractaire-amphi:matrix.org
## Documents

Pour vous documenter sur le sujet de la SSA
> Vidéo du Canard Réfractaire :  
> https://youtu.be/LqC4SZUfJ9o  
> La conférence sur la SSA l'université confiné des réfractaires (du 5 au 6 décembre 2020) :  
> https://www.youtube.com/watch?v=NldAwRp-aGM  
> De la fourche à la fourchette... Non ! L'inverse ! Conférence gesticulée - Mathieu Dalmais  
> https://www.youtube.com/watch?v=9Tqy5DSL1N4  
> Pour une sécurité sociale de l'alimentation  
> https://www.youtube.com/watch?v=7Stjd-tnA4s  
> Un live de canal concorde  
> https://www.youtube.com/watch?v=wwswqUyvMto  
> Conférence gesticulée de Bernard FRIOT  
> https://youtu.be/ZuZz9NSOh10  
> L'histoire de l'agriculture de 1940 à nos jours  
> https://lecanardrefractaire.org/lagriculture-des-annees-1940-a-nos-jours/  

[1]: https://fr.wikipedia.org/wiki/Gilles_Luneau
[2]: https://conferences-gesticulees.net/conferences/de-fourche-a-fourchette-non-linverse/
[3]: https://fr.wikipedia.org/wiki/Bernard_Friot_(sociologue)
[4]: https://mardi.lespaniersdubocage.com/Logodall-Ti-Feurm-Julien-Le-Dem
[5]: https://terredeliens.org/
