---
title: 17 Janvier - Dérives autoritaires
menutitle: <b>17 Janvier</b>  Dérives autoritaires
description: Programme de la Scène réfractaire du 17 janvier 2021 sur les dérives autoritaires
weight: -20210117
images:
  - /img/janv21.png
---

# Dimanche 17 janvier 2021

## Dérives autoritaires

[![Affiche université confinée](/img/janv21.png)](https://discord.lecanardrefractaire.org/docs/scene-2021/janvier/)

Sur cette après-midi thématique, nous parlerons **des dérives autoritaires.**

Venez nombreux, pour participer, observer, échanger !

## Programme

Heure  | Programme             | Sujet
-------|-----------------------|------------------------------------
&nbsp; |      <h2 style="margin:0">Dimanche 17 Janvier 2021</h2>                 | 
14h    | Les&nbsp;Réfractaires et le canard réfractaire | Ouverture : présentation du programme
14h20  | Discussion ouverte: [Kalee][3] & [Booteille][4]| Dystopie et société de surveillance
16h15  | Pause gouter, Le réfractaire **Farzad Felezzi** à la Sitar | Musique 
16h30  | Table ronde: [Alexandre Duclos][5], [Malou l'Accroqueuse des mots][9], [Punjo][7], [Cemil][8] | Les dérives autoritaires
19h45  | Présentation des musiciens | 
20h    | Concert avec [Commis d'office][15] & [Rod Pi][10] | Musique
21h    | After sur Discord | 

## Lieu et plate-formes

L'évenement aura lieu sur [le Discord][1] du Canard Réfractaire et sera diffusé en live: 

1. Sur [Peertube][12]
2. Sur [le Youtube des réfractaires][13]
3. Sur [Youtube chez Kalee][14]
4. Sur [Twitch][16]


[1]: https://discord.com/invite/HaEpyY3
[2]: https://www.youtube.com/c/KaLeeVision/videos
[3]: https://www.youtube.com/c/KaLeeVision/videos
[4]: https://framapiaf.org/@booteille
[5]: https://oc.todon.fr/@Alexandre_Duclos
[6]: https://maloulaccro.com/
[7]: https://youtube.com/channel/UCCmldKYrBh4azXU5FVY-obQ
[8]: https://youtube.com/channel/UC6-BWVphnrCj5xNL73qOd0w
[9]: https://maloulaccro.com/
[10]: https://youtube.com/channel/UCugFhFPcBHZsXR1DdXpT_0g
[12]: https://live.canard.tube/videos/watch/77718500-0820-488b-9bd7-1df4aaf12cb2
[13]: https://www.youtube.com/channel/UCQTkRwkHtHUL6OJB5SI201Q/featured
[14]: https://www.youtube.com/c/KaLeeVision/videos
[15]: https://youtube.com/channel/UCdeVSVIZqOu2P4H-NcKJBiQ
[16]: https://www.twitch.tv/canardrefractaire@everyone
