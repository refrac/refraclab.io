---
title: 2 avril - Le mensonge en politique
menutitle: "<b>2 avril</b>  Anthroplogie Réfractaire #2  Le mensonge en politique"
description: La parole du personnel politique est de plus en plus dévaluée aux yeux d'une opinion politique échaudée par des scandales politico-judiciaires, fatiguée par une novlangue communicante creuse et décérébrante. Là se trouve un écueil, un réflexe, une butée pour le sens commun et pour ainsi dire un piège.
weight: -20210402
images:
  - /img/anthropo-mensonge-1200.png
---

# Vendredi 2 Avril à 20h

<div style="text-align:center">

**Replay du live (remasterisé) (2h10)**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/118d6ebb-e37b-419f-8a4f-5b7371a50857" frameborder="0" allowfullscreen></iframe>
</div>

<div style="text-align:center">

**Annonce (2min)**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/7d884043-45fe-4935-97e4-0dfe713340d8?title=0&warningTitle=0" frameborder="0" allowfullscreen></iframe>
</div>

## Anthropologie réfractaire #2 - Le mensonge en politique

La parole du personnel politique est de plus en plus dévaluée aux yeux d'une opinion politique échaudée par des scandales politico-judiciaires, fatiguée par une novlangue communicante creuse et décérébrante. Là se trouve un écueil, un réflexe, une butée pour le sens commun et pour ainsi dire un piège. Si le dévoilement du mensonge et la révélation de la vérité sont cruciale dans certains domaines (la justice, l'enquête policière, la relation amoureuse, etc.), elles n'intéressent que peu la science qui s'intéresse plutôt à l'exactitude et la réfutabilité des énoncés, et détourne le débat politique de ses objets naturels (conflit sur la répartition des richesses, des statuts et sur le mode de régulation des conflits).

Une opinion, une prise de position politique ne peut être vrai ou fausse, on est d'accord avec elle, on s'y oppose ou elle sert de base à une négociation.

On verra ensemble qu'il existe une tendance à encombrer le débat politique d'une matière certes vérifiable, factuelle, objectivable, pouvant être vrai  ou fausse, des chiffres, des représentations-massues, des réalités-obus qui étouffent le débat, préemptent les termes du débat, limitent le nombre d'experts capables de dire ce qui est vrai ou faux et destituent les citoyens lambda de leur compétence élémentaire d'interpréter le réel commun, d'avoir un réel commun. A l'occasion d'une réflexion proposée sur le mensonge en politique, c'est une fantasmagorie de la vérité et une gouvernementalité de la vérité qu'on essayer d'analyser.

On verra aussi quel rôle la communication politique joue dans ce nouveau "gouvernement par les faits".

On éclaircira enfin une logique souterraine de l'action sociale très simple : le mensonge engage, la communication dégage, le mensonge lie, la communication délie.

A lire:

https://www.sciencespo.fr/cevipof/sites/sciencespo.fr.cevipof/files/CEVIPOF_confiance_10ans_CHEURFA_CHANVRIL_2019.pdf
https://www.sciencespo.fr/cevipof/sites/sciencespo.fr.cevipof/files/Round%2012%20-%20Barome%cc%80tre%20de%20la%20confiance%20en%20politique%20-%20vague12-1.pdf

On pourra discuter de tout ça, mais sachez qu'Alexandre sera plus armé pour répondre spécifiquement sur les sujets suivants:

- Le mensonge comme pratique sociale de guérison, d’évitement ou de protection
- Le mensonge comme pratique d’intégration
- Le mensonge comme pratique sociale d’enchantement du monde 
- Le mensonge comme engagement VS la communication comme dégagement
- Le mensonge et la parodie dans la mythologie
- Les awards du mensonge politique de l’année

|
---|---
Une intervention de | Alexandre Duclos (voir sa [chaine youtoube](https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag))
Organisé par | SoHappy, NoPesto
Animé par | NoPesto
Régie technique | Mose, Tytan652
Graphisme | Mose
Interlude | Le Pixel Mort (retrouvez-le [sur Youtoube](https://www.youtube.com/channel/UCeXsZz8C5sUZe1YgDxR6j9A), ou mieux, [sur peertube](https://canard.tube/accounts/pixel/))

## Qu'est-ce que l'Anthropologie réfractaire?

C'est un rendez-vous mensuel avec Alexandre Duclos qui expose son regard de sociologue anthropologue sur la situation politique et sociale du moment. C'est aussi une occasion pour se tenir à la disposition des auditeurs pour des échanges libre sur le thème du moment.

## Qui est Alexandre Duclos?

Enseignant à Paris 1 Panthéon Sorbonne, il est sociologue et anthropologue. Il diffuse depuis plusieurs années ses cours en ligne ainsi que des diffusions d'opinions, de pensées et d'analyses [sur sa chaine youtube][ad].

## Comment participer?

Le plus simple est de nous rejoindre sur le [discord des réfractaires](https://discord.gg/HaEpyY3) dans les canaux de la Scène Réfractaire. Un canal vocal permettra d'écouter le live, et ceux qui en exprimeront le désir seront invité en studio pour interagir, poser des questions ou demander des précisions. Il sera possible de poser des questions à l'écrit à cet endroit également.

En plus du live dans discord, la session sera diffusée sur notre peertube en direct (ou en fait avec un léger différé de 30 secondes). Passez voir https://canard.tube au moment du live. Mais Peertube ne permet pas (encore) de réagir en live dans un chat, donc si vous voulez intéragir, venez donc plutôt sur le discord.

## Est-ce qu'il y aura un replay?

La session sera disponible à la fin de l'événement sur https://canard.tube/ et sur la [chaine youtube d'Alexandre Duclos][ad].


[ad]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
