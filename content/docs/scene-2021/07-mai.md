---
title: 7 mai - Sans contact
menutitle: "<b>7 mai</b>  Anthroplogie Réfractaire #3  Sans contact! Analyse des mutations du quotidien en pandémie"
description: Quand l’échange est sans contact, le média qui permet l’échange devient indispensable et omniprésent. On peut donc chercher à décrire l’hypertrophie et la toute-puissance des substituts de médiation (ou média de substitution). En fait, au lieu de dire sans contact, on pourrait dire avec médiation, indirect.
weight: -20210507
---
# Vendredi 7 mai à 20h

<div style="text-align:center">

**Replay du live (2h10)**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/0c46f97a-5056-474b-9628-4d13d30c3b48" frameborder="0" allowfullscreen></iframe>
</div>

<div style="text-align:center">

**Annonce (1min)**  
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://canard.tube/videos/embed/cad2a785-0cff-4bfa-a36c-0ba945cd441a" frameborder="0" allowfullscreen></iframe>
</div>

## Anthropologie réfractaire #3 - Sans contact! Analyse des mutations du quotidien en pandémie

Le "sans contact" est une figure omniprésente de nos quotidiens, de manière plus intense encore dans nos quotidiens confinés. Cette formule est porteuse d'un mensonge, d'une falsification, d'une tromperie élémentaire.  

Quand l’échange est sans contact, le média qui permet l’échange devient indispensable et omniprésent. On peut donc chercher à décrire l’hypertrophie et la toute-puissance des substituts de médiation (ou média de substitution). En fait, au lieu de dire sans contact, on pourrait dire avec médiation, contact indirect, contact avec via un tiers inclus, interaction avec action d’un intermédiaire. Ou encore, un plan à trois.

Dans un monde sans contact, il est impossible d'avoir une relation directe avec quoi que ce soit ou qui que ce soit sans médiation technique, et la plupart du temps, sans transaction économique. Le sans contact n'économise rien, il engage, il introduit des macro-systèmes techniques entre les corps et ce que les corps peuvent expérimenter. Par le prisme de cette manière de dire, on peut apercevoir un déterminant anthropologique profond de notre modernité liquide.
