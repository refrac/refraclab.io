---
title: Bientôt - Atelier-discussion féminisme
menutitle: "<b>Bientôt</b>  Atelier discussion  Féminisme"
description: Discutons ensemble pour comprendre ce qu'est le féminisme, témoignages et subjectivités.
weight: -20211212
---

# 13 mars 2022

## Atelier-discussions sur le féminisme

Le féminisme dans sa globalité, c'est pour certaines personnes au choix :

- un mouvement annexe qui dilue les efforts militants
- un mouvement d'utilité publique participant à l'émancipation des femmes dans un monde dominé par le patriarcat
- un mouvement pluriel de personnes qui pour certaines se bouffent entre elles et parasitent le combat pour les droits des femmes
- un mouvement qui tend à vouloir inverser le rapport de domination
- un truc un peu vague qui provoque une certaine démesure ou dont on n'entend presque pas parler.

Nous souhaitons ouvrir à toutes et tous les questions sur le féminisme, des plus simples aux plus poussées.

Le but étant de permettre à des courants de pensées se réclamant d'un féminisme d'échanger, à des gens se posant des questions d'obtenir des réponses et de se construire un avis.

Une vigilance particulière veillera à ce que les échanges se déroulent avec bienveillance et nous souhaiterions pouvoir réitérer l'expérience pour creuser les sujets qui pourraient émerger, répondre aux frustrations des personnes pensant que les échanges ne sont pas allés assez loin.

Dans le format, nous souhaitons un micro tournant, des intervenants et intervenantes respectueux, et un échange constructif. Nous ne formulons pas d'autres attentes que de simples avancées sur les questionnements des uns et des autres, ouvrant la voie, nous l'espérons, à une poursuite sous de multiples formes de ce type de manifestations.

Invitez vos amis et connaissances, partagez, venez nombreux, et croisons un instant nos avis et nos questions

Cet atelier se déroulera sur discord et sera enregistré et (peut-être) disponible a l'écoute ultérieure.

Le déroulé :

- "Le patriarcat n'existe plus" : petit tour des courants féministes
- Petit point sur les (in)égalités de nos jours
- Féminisme et/ou universalisme ?
- Pour aller plus loin (bibliographie, références en pagaille, effervescence) /30
- On recommence ?
