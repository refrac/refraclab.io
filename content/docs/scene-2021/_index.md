---
title: EducPop 2021
bookCollapseSection: true
weight: 30
description: Le Canard Réfractaire a organisé sa première édition de l’Université Confinée sur Discord en décembre 2020. Du coup sur cette lancée on s'est lancé dans une saison 2021 d'éducation populaire.
---


# Education Populaire Réfractaire 2021

Le discord du Canard Réfractaire a organisé sa première édition de l’Université confinée sur Discord en décembre 2020. Ce fut une expérience riche et appréciée. Nous continuons à organiser des événements, débats, présentations, regroupés sous le nom d'EducPop 2021 pour l'année qui vient.

A noter que l'organisation de ces événements est indépendante du media libre Canard Réfractaire. Ce travail et un effort collectif des Réfractaires présents sur le discord

## A venir

Date  | Événement
------|----------------------
[Bientôt][14] | [Atelier - Discussion Féminisme][14]

## Scènes précédentes

Date  | Événement
------|----------------------
[Samedi 18 Décembre][16] | [Les étrangers ont-ils des droits ? - Anthropologie réfractaire #7][16]
[Mardi 16 Novembre][15] | [Atelier techos - découverte OBS][15]
[Samedi 23 Octobre][13] | [Au commencement était une femme - Anthropologie réfractaire #6][13]
[Samedi 11 Septembre][12] | [Reconnaissance vs identification - Anthropologie réfractaire #5][12]
[Samedi 26 Juin][11] | [Solitude et Servitude - Anthropologie réfractaire #4][11]
[Vendredi 7 mai][10] | [Sans contact! - Anthropologie réfractaire #3][10]
[Dimanche 25 avril][9] | [Révolution agraire : Initiatives citoyennes et paysannes][9]
[Vendredi 2 avril][7] | [Le mensonge en politique - Anthropologie réfractaire #2][7]
[Mardi 23 mars][8] | [Club de lecture][8]
[Mardi 16 mars][6] | [Club de lecture][6]
[Mardi 9 mars][5] | [Club de lecture][5]
[Mercredi 3 mars][4] | [Atelier communication non violente][4]
[Dimanche 28 février][3] | [Hygiène Numérique - Améliorer notre hygiène numérique sur Android][3]
[Vendredi 26 février][2] | [Résignation ou renoncement - Anthropologie réfractaire #1][2]
[Dimanche&nbsp;17&nbsp;janvier][1] | [Les dérives autoritaires][1]

[1]: https://discord.lecanardrefractaire.org/docs/scene-2021/17-janvier/
[2]: https://discord.lecanardrefractaire.org/docs/scene-2021/26-fevrier/
[3]: https://discord.lecanardrefractaire.org/docs/scene-2021/28-fevrier/
[4]: https://discord.lecanardrefractaire.org/docs/scene-2021/03-mars/
[5]: https://discord.lecanardrefractaire.org/docs/lectures/s2-9-mars/
[6]: https://discord.lecanardrefractaire.org/docs/lectures/s2-16-mars/
[7]: https://discord.lecanardrefractaire.org/docs/scene-2021/02-avril/
[8]: https://discord.lecanardrefractaire.org/docs/lectures/s2-23-mars/
[9]: https://discord.lecanardrefractaire.org/docs/scene-2021/25-avril/
[10]: https://discord.lecanardrefractaire.org/docs/scene-2021/07-mai/
[11]: https://discord.lecanardrefractaire.org/docs/scene-2021/26-juin/
[12]: https://discord.lecanardrefractaire.org/docs/scene-2021/11-septembre/
[13]: https://discord.lecanardrefractaire.org/docs/scene-2021/2021-10-23-anthropo-6/
[14]: https://discord.lecanardrefractaire.org/docs/scene-2021/2021-10-10-feminisme/
[15]: https://discord.lecanardrefractaire.org/docs/scene-2021/2021-11-16-bases-obs/
[16]: https://discord.lecanardrefractaire.org/docs/scene-2021/2021-12-18-anthropo-7/
