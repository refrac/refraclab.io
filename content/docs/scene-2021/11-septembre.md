---
title: 11 Septembre - Reconnaissance VS identification
menutitle: "<b>11 Septembre</b>  Anthroplogie Réfractaire #5  Reconnaissance VS identification"
description: Pourquoi opposer ces deux termes qui peuvent être si proches et si facilement confondus. Parce qu'il se joue dans cet écart de sens notre humanité d'un côté, et l'exercice d'un pouvoir anti-démocratique de l'autre. En glissant de l'un à l'autre, on perd à la fois la capacité d'être semblable, de s'appartenir, mais d'être aussi radicalement différent, la fécondité liante de la négociation et du conflit politique et la possibilité même de la liberté.
weight: -20210911
images:
  - /img/anthropo_5.png
---

# Samedi 11 Septembre à 21h

**Bande Annonce**
<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="[Bande annonce] Anthropologie réfractaire #5 - Reconnaissance VS identification" src="https://canard.tube/videos/embed/37ed8212-ab17-41dc-b33a-f1c805e819a0" frameborder="0" allowfullscreen></iframe>
</div>

**Enregistrement audio**
<div style="text-align:center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="[Audio] Anthropologie réfractaire #5 - Reconnaissance VS identification" src="https://canard.tube/videos/embed/98b57fc3-f0bd-491f-b06f-30f02a726930" frameborder="0" allowfullscreen></iframe>
</div>

## Anthropologie réfractaire #5 - Reconnaissance VS identification

Pourquoi opposer ces deux termes qui peuvent être si proches et si facilement confondus. Parce qu'il se joue dans cet écart de sens notre humanité d'un côté, et l'exercice d'un pouvoir anti-démocratique de l'autre. En glissant de l'un à l'autre, on perd à la fois la capacité d'être semblable, de s'appartenir, mais d'être aussi radicalement différent, la fécondité liante de la négociation et du conflit politique et la possibilité même de la liberté.

En effet, c'est dans le processus de reconnaissance réciproque que prend forme le sujet et l'Autre, ce que nous verrons ensemble, quand l'identification réduit l'individu identifié à un jeu de paramètres standards jusque dans leur variabilité. On peut identifier n'importe quoi. On ne se reconnait qu'entre semblable et cette faculté de reconnaissance ou de non-reconnaissance est le dernier et premier bastion de ce que l'on pourrait appeler le sujet souverain. On part là au coeur de ce qui oppose système technique et ensemble symbolique, puissance du technique et puissance du symbolique.

Laissez vos QR codes à l'entrée, nul n'entre ici s'il n'est capable d'être humain. Si le coeur vous en dit, on ira aussi approfondir du côté d'Axel Honneth (au risque d'un léger hors-sujet) ou vers Claude Levi Strauss et Jacques Lacan.

## Références bibliographiques

- Emile Durkheim, Les formes élémentaires de la vie religieuse, PUF, Paris, 1912.
- Axel Honneth, La Lutte pour la reconnaissance, Cerf, 2000 (première édition allemande 1992)
- Jacques Lacan, Écrits, Seuil, Paris, 1966.
- Claude Lévi-Strauss, 
  - Les structures élémentaires de la parenté, Mouton, Paris/La Haye, 1949
  - « Introduction à l’œuvre de Marcel Mauss », in Mauss M., Sociologie et anthropologie, PUF, Paris, 1950.
  - Anthropologie structurale, Plon, Paris, 1958.
  - La pensée sauvage, Plon, Paris, 1962.
  - Séminaire « L’identité » (1974-1975), coord par Claude Levi-Strauss, PUF, Quadrige, 2007
- Georges Lucas : Le retour du Jedi, scène 28 : http://maxam.chez.com/sw_scripts_6_028.html
- Lucien Scubla, « Le symbolique chez Lévi-Strauss et chez Lacan », Revue du MAUSS permanente, 6 octobre 2014 [en ligne]. http://www.journaldumauss.net/./?Le-symbolique-chez-Levi-Strauss-et
- Stéphane Vibert, « L’imaginaire et le symbolique : Castoriadis et le structuralisme de Lévi-Strauss », Cahiers Société, (1) (2019), 35–61. https://doi.org/10.7202/1068420ar

---

> ## Qu'est-ce que l'Anthropologie réfractaire?
> 
> C'est un rendez-vous mensuel avec Alexandre Duclos qui expose son regard de sociologue anthropologue sur la situation politique et sociale du moment. C'est aussi une occasion pour se tenir à la disposition des auditeurs pour des échanges libre sur le thème du moment.
> 
> ## Qui est Alexandre Duclos?
> 
> Enseignant à Paris 1 Panthéon Sorbonne, il est sociologue et anthropologue. Il diffuse depuis plusieurs années ses cours en ligne ainsi que des diffusions d'opinions, de pensées et d'analyses [sur sa chaine youtube][ad].
> 
> ## Comment participer?
> 
> Le plus simple est de nous rejoindre sur le [discord des réfractaires](https://discord.gg/HaEpyY3) dans les canaux de la Scène Réfractaire. Un canal vocal permettra d'écouter le live, et ceux qui en exprimeront le désir seront invité en studio pour interagir, poser des questions ou demander des précisions. Il sera possible de poser des questions à l'écrit à cet endroit également.
> 
> En plus du live dans discord, la session sera diffusée sur notre peertube en direct (ou en fait avec un léger différé de 30 secondes). Passez voir https://canard.tube au moment du live. Mais Peertube ne permet pas (encore) de réagir en live dans un chat, donc si vous voulez intéragir, venez donc plutôt sur le discord.
> 
> ## Est-ce qu'il y aura un replay?
> 
> La session sera disponible à la fin de l'événement sur https://canard.tube/video-channels/anthropologie.refractaire et sur la [chaine youtube d'Alexandre Duclos][ad].


[ad]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag
