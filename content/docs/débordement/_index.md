---
title: Débordement Médiatique
date: 2021-01-25
weight: 50
---
# La gazette numérique du Débordement Médiatique

Le `débordement médiatique` est un couriel militant fait par le [Canard Refractaire](http://twitter.com/canard_media) et [Thomas Babord](http://twitter.com/thomas_babord) où l'on envoie toutes les vidéos militantes et autres liens intéressants qu'on a trouvé dans la semaine.

Le courriel est envoyé le dimanche, souvent en début d'après midi.

La plupart des liens qu'on envoie sont généralement sur les lignes " gillet jaune de gauche ", mais il est possible d'y trouver d'autres point de vue d'un monde alternatif.

Chacun peut proposer des vidéos en nous contactant par email, par discord ou par twitter.

## Inscriptions pour le Débordement Médiatique :
[Inscription pour le débordement médiatique](https://lecanardrefractaire.org/debordons/)

## Les archives du débordement Médiatique

- [#16 - 23/03/21](https://preview.mailerlite.com/o6u1r0/1647988893921842749/d6m9/)
- [#15 - 16/03/21](https://preview.mailerlite.com/e8m5d8/1642914505027490923/r9q1/)
- [#14 - 07/03/21](https://preview.mailerlite.com/m9e0r6/1636514790895523430/e7s3/)
- [#13 - 28/02/21](https://preview.mailerlite.com/r7k5g4/1631104575102522734/i3w9/)
- [#12 - 21/02/21](https://preview.mailerlite.com/l4y5x7/1626217712038450914/v1f1/)
- [#11 - 08/02/2021](https://preview.mailerlite.com/l5r5x8/1616813474891961665/t9b6/)
- [#10 - 31/01/2021](https://preview.mailerlite.com/g0o9i8/1610800590034572562/c2g3/)
- [#9 - 24/01/2021](https://preview.mailerlite.com/l0p2s0/1605745672122799558/y2a0/)
- [#8 - 17/01/2021](https://preview.mailerlite.com/d4i6n7/1600680598945404025/a2l3/)
- [#7 - 10/01/2021](https://preview.mailerlite.com/s5n8t0/1595834120745784967/l0q0/)
- [#6 - 13/12/2020](https://preview.mailerlite.com/q9h1f7/1575512242483369254/m4v9/)
- [#5 (oups') - 04/12/2020](https://preview.mailerlite.com/d5b9o7/1568728538402526232/n4k1/)
- [#5 - 22/11/2020](https://preview.mailerlite.com/s7t0b3/1560264688330085993/h7m4/)
- [#4 - 15/11/2020](https://preview.mailerlite.com/q0z4v5/1555039757401068969/v5i4/)
- [#3 - 08/11/2020](https://mailchi.mp/26f5040b79d4/dbordons-le-courriel-des-mdias-militants-3567834?e=d4d93d4668)
- #2 - XX/XX/2020 - Lien manquant
- #1 - XX/XX/2020 - Lien manquant
