---
title: Dimanche 21 Mars 2021
date: 2021-03-21
---

# Concertation du Dimanche 21 Mars 2021 - 21h

## Educpop / evenements

### le triptyque infernal (Alimentation Agriculture et écologie)

Des réunions régulières sont amorcées tous les mercredi soir et le dimanche avant notre rituel hebdomadaire pour l'orga. 

> - La date de l'événement a changé pour les 24 et 25 avril
> - Le 10 et 11 Avril session de jeu de role pour la SSA à guingamp
> - les lundis soirs : rédaction collaborative pour l'article du canard
> - 3 parties(sur les 3 thèmes sur 3 périodes
> - travail historique pour comprendre comment on en est arrivé à la situation actuelle
> - Le mercredi c'est pour l'organisation

### Duclos Anthropologie réfractaire

- Affiche par Mose
- Premier Teaser: un peu long - Alexandre va en refaire un plus court
- Dès que affiche et Teaser: réunion communication - Mardi 20h30
- Pause de 5 min toujours pas définie: idée musique ou zapping mensonge ou pas de pause. Des gens avec des idées - des possibilités? (vidéo de 5 min musique par exemple)

> - appel à transmettre des vidéos sur les mensonges
> - suggestions : Et tout le monde s'en fout (la séance presque parfaite) https://www.youtube.com/watch?v=QZ2oj5N3xNg
> - Pixel mort

## Discord

### Confirmation du renommage du canal #vidéos-du-canard en #medias-du-canard

Le nouveau bot peut aussi poster les articles du site, on les met dans le même canal que les vidéos ? et donc on renomme le canal.

> Après un long brainstorming on adopte le kiosque du canard

### On attend encore PeerTube ou on fait débarquer Géo Postetou sur le serveur ?

- Le bug PeerTube sur les flux RSS est mineure, il faut juste quelqu'un qui supprime s'il y a un lien erroné.
- Pour paramétrer ou ajouter les flux RSS du bot, il faut que l'utilisateur est le droit "Gérer les salons" sur tout le serveur ce qui est pas bénin.

> (c'est le bot qui fait les annonces)


## Mention rapide

- Le tuto OBS a maintenant une annexe sur le plugin VST pour les utilisateurs GNU/Linux, les autres c'est déjà inclus par défaut.

