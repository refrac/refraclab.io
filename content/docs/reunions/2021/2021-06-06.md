---
title: Dimanche 06 Juin 2021
date: 2021-06-06
---

# Réunion du Dimanche 06 Juin 2021 - 21h

## CanardTube
RodPi as rejoint l'instance.

## Mobilizon
Un groupe est à disposition et personne l'utilise pour créer des événements.

## Refractaire records
- Présentation du trailer affiche - Consultation globale sur les éléments visuels : logo, charte graphique...
- du premier trailer video - présentation de la logique RR
- Confirmation de la date proposée : 23 Juin - à soumettre à l'approbation collective
- Point sur l'avancement Projet "Wanted Festival"
> - Affiche : rajouter le QR Code et changer la police du lien du Discord (pour la différence entre les majuscules et minuscules)
> - pour le trailer vidéo : proposition d'un logiciel libre pour le montage. L'idée est de présenter la chaîne Réfractaires Records
> - Date du 23 juin : pas de soucis avec la date. 
> - format de 2h : 1/2H de vidéo du groupe et 1h30 avec interview/débat/questions du public si nécessaire
> - Intégration au static vu 
> - Wanted Festival : Affaire Music Too (agressions sexuelles dans le milieu de musique dites"extrêmes"). Comment réagir ? A en rediscuter avec Yohan et Myriam (c'est en cours...) 

## Pad
On est passé à 7 équipes possible sur le pad.

## Modération
> Nous avons banni un ancien réfracaire modérateur, destitué depuis plusieurs mois, suite à son comportement devenu toxique sur le serveur.
