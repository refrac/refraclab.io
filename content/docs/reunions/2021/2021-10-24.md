---
title: Dimanche 24 Octobre 2021
date: 2021-10-24
---

# Clash du Dimanche 24 Octobre 2021 - 21h

## Modération

> - Ban du l'élément perturbateur qui nous trolle en vocal depuis qq jours?
> - Sondage des présents : 6 pour, 0 contre, 1 drapeau blanc
> - Décision actée et ban effectué

## Stream de l'antropologie refractaire

> - Mose semble avoir des soucis récurrent de streaming.
> - Faudrait d'autres streameur·euse·s en backup de mose. Organiser des formations pour la maîtrise des outils OBS.
> - Le mode conférence à été utilisé. En mode vocal uniquement (pas de webcam, ni partage écran). Ca s'est bien passé. 
> - Mais il y avait la webcam sur le stream PeerTube

## Vidéos

> - La vidéo FAQ sort demain.
> - Une vidéo sur l'écologie sort dans 2/3 jours
> - ET on voit le petit chien de Sohàpy

## Atelier féminisme 

> - Evénement en stand-by. On a toujours trouvé personne qui veut parler.

## Club de lecture

> - Le club de lecture marche bien. 
> - Viendez toutes et tous mardi à 21h30. Mathieu, blogeur (https://ptvirgule.hypotheses.org/author/ptvirgule), analyse les techniques de communication de E. Macron
> - Autre idée pour le club : "La France de traverse" par Alexandre Duclos (https://www.mollat.com/livres/2540225/alexandre-duclos-la-france-de-traverse-anthropologie-itinerante-d-un-pays-eprouve)


## Agriculture

> - Denved aurait bien fait un petit format sur l'agriculture avec les election présidentielle. Une analyse des proposition des différents candidats. Petites vidéo à sortir fin d'hiver début du printemps.
> - Trouver des paysans sur les réseaux sociaux c'est pas évident.


