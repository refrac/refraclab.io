---
title: Dimanche 21 Juin 2020
date: 2020-06-21
---

# Communauté

> - ça serait pas pas mal de communiquer plus sur les comptes rendus de réunion
> - on peut utiliser le canal compte-rendu-de-discussions pour ça

## Modération

- proposer une date de travail sur la charte de le modération
- comment on fait avec *pseudo d'emprunt* (transgression du règlement : à partir de quel seuil on considère que des propos peuvent entraîner la sanction, dans quelles circonstances fait-on usage du règlement) que faisons nous avec lui ?

> - y'a eu un rendez-vous avorté cette semaine pour la charte modération
> - djé ré-organise ça mardi à 21h et pose une annonce, si protestation décalage possible
> - Quoi faire avec *pseudo d'emprunt*, chiant, idée raciste homophobe latent il dérape beaucoup, que faire quand des gens dérapent, il se sent à l'aise pour le faire, il empêche les prises de parole. Première proposition parler avec lui : Espion se propose de le faire.

## Vie collective réfractaires

- on zone sur quelques autres discord, pas mal de réfractaires font aussi partie d'autres discord/réseaux
- peut-être qu'on pourrait ajouter dans le site statique une page réseaux amis? comment on décide qui s'y trouve?
  - canal concorde
  - culture ric
  - crapulax
  - ipgj
  - bangbang TV
  - répertoire des serveurs engagés
  - autres? ...
- kl : vois une convergence possible avec MCCA : (OdJ de Mardi 23 prochain 20h30 début 21h) Annuaire, recensement - ateliers et outils de travail - liens entre différents projets existants et organisations -….

> - proposition d'Espion en référence au bot décrit pus bas
> - il semblerait que c'est bien, feedback positif
> - concordance possible avec l'atelier qui a développer le sujet 2, https://discord.lecanardrefractaire.org/docs/ateliers/saison2/2-repertoire-groupe-actions/

### nouveaux réfractaires de la semaine

- invivo : ou sont les propositions de nouveau réfractaire ? Perso cette semaine j'ai vu deux personnes qui pourrait correspondre, mais n'ayant pas noté leur nom je me retrouve comme un con. 
- Voici des membres qui sont sur le discord depuis un bout de temps et qui mériteraient au moins une discussion sur leur entrée en tant que Réfractaires, pour la plupart ils n'ont jamais étaient proposé il me semble: 
- J'avais demandé à la personne suivante si elle souhaitait rejoindre les réfractaires - le cas échéant je me serais positionné en tant que Parrain : Mangatse. Sa réponse a été non.
- le point précédent souligne l'importance du consentement.

> - Il n'y a pas vraiment eu de proposition de nouveaux réfractaires, salon de validation continu des réfractaire pour kys qui a besoin d'un retour de légitimité. 
> - Il y a eu des gens qui n'ont jamais été proposé, malgré le temps qu'ils sont présents sur le serveur. Ca ne semble pas être un problème pour tout le monde. Si Kys veut intégrer ces personne il peut les proposer pour voir les réactions et en discuter
> - Le modérateur qui valide les nouveaux réfractaires supprime les messages de délibération. Donc seul les modérateurs et admins peuvent intégrer des nouveaux membres et supprimer les messages.
> - Envisager de nettoyer régulièrement le salon d'intégration

## Structure juridique

### Création d'une association

- Pour légaliser ma situation, j'ai besoin d'une association a qui faire des factures. Ça pourra aussi servir a d'autre truc ( adhésion a des fédérations, assurance, orga d'événement, rémunération d'autres gens... )
- L'idée est de faire une asso vide pour le moment, avec cooptation. Si un jour, c'est pertinent vu notre orga d'avoir une vraie asso collégiale et tout le patatra, on changera les statuts.
- Proposition de statut : https://nuage.lecanardrefractaire.org/apps/onlyoffice/s/WGqCeqTGZpWAHBg

### question

- kl : est-ce que le projet de Yohan concerne les réfractaires ?

> - ça ne nous concerne pas vraiment, c'est pour savoir si il y a des compétences parmi les réfractaires pour examiner les statuts de l'asso. l'Asso est une construction technique pour rémunéré l'équipe du journal. se sont des statut type pour une entreprise à statut associatif. Penses à prévoir d'évoluer : propositions de travail de réflexion sur les systèmes d'adhésion (AMAP médiatique, ou autre proposition novatrice pour soutenir la création indépendante). 
> - invivo regardera pour faire ses retours sur les statuts de Yohan.

## Mise a jour du financement

### Mise a jour du tipeee

- Plus mise a jour depuis le début d'année, il n'y avait pas mention des vidéos face-cam. Chose faite ! On va pouvoir maintenant réellement https://fr.tipeee.com/lecanardrefractaire/

### Création d'un Paypal

- contact@lecanardrefractaire.org

### Ce qui reste a faire

- Création d'un compte en banque pour l'asso. Je pense au crédit patate a côté de chez moi, au moins, il y aura un humain. Pas trop envie de me galérer avec d'autre. La discussion est ouverte !
- Créer une vidéo sur le financement avec les différents moyens : paypal, virement, cheque, tipeee.

# Production de contenu

## Education Populaire

- Le travail du groupe 4 "refrac'terre" nous mène à une réflexion sur l'achat de foncier et la création de village réfractaire, si la thématique vous intéresse rejoignez nous (invivo + bountar, denved, farzad, XIII, ...)
- Le travail du groupe 6 "hell'eau" nous a conduit sur une piste inintéressante à développer (à part peut-être un nouveau sujet?) : Analyser la convention citoyenne (SoHapy, Ghani + invivo, paprika, ...)
- Les moments d'éducation populaire sont des moments parfaits pour intégrer des nouveaux ou des gens qui n'osent pas passer en vocal, ça leur donne une occasion de "franchir le pas" trouvons des occasions de multiplier ce format. Chaque ateliers devrai poser des rendez vous accessible aux @membres.
- Proposition de farzad : Atelier en intelligence collective tous les jeudi 12h à 13h - Réinventer nos prises de parole pour les diffuser dans nos actions, dans les médias et les institutions. https://pad.crapaud-fou.org/p/_R%C3%A9inventons_nos_prises_de_parole 

> - rangement sujet 5 invivo et kys
> - comment rendre ça publique : parti projet sur le site statique, rendre visible les groupe d'action, les techos pourrait plancher pour proposer de gérer les flux de données issu des ateliers. 
> - on va essayer de faire un méta-atelier éducation populaire dans la semaine, mose propose Mercredi soir 21h. il fera une annonce dans le canal annonce refractaire, pour promouvoir les ateliers utiliser le canal compte rendu.

## Sous-titrage

- C'est Oki 00101010 à épinglé l'organisation des sous titres si des gens souhaitent rejoindre une équipe n'hésitez pas à nous contacter.
- Bountar le batard / correction du temps
- Denved ar vro / correction orthographe
- Sohàpy / correction des sous titres

> - Gros boulot bravo !!

## Vidéo

- Le gros pavé sur Macron a plutôt bien marché. Niveau stat, ca remonte bien les dernières vidéos qui n'ont pas dépasser le cercle "habituel". Même si des grosses vidéos, ca prends plus de temps et d'énergie, ça rythme bien la vie de la chaîne - a refaire !
- Nouveaux titres aussi, plus "putaclic" et qui évite la redite avec les informations de la miniature. 
- Première démarche pour la mise en place de la structure juridique, histoire d'être reglo sur ma rémunération.
- Une question se pose : qu'elle vidéo réaliser pour fêter les 30k abonnés ?
- yohan bosse ce soir sur une proposition. Je la mettrai sur le discord avant la réunion.

> - Est-ce vraiment un passage important (le 30k) : ça a pas trop l'air 

# Techos

## Discord

- Est-ce qu'on donne la visibilité sur les logs serveur à tous les réfractaires ??

> - ceux qui veulent avoir accès au bac à sable réfractaire demander à Espion. 
> - les réfractaires vont avoir accès au log (vote a 7 contre 1)

## Nuage

- peut-on migrer ce présent document sur le nuage ?
- transition complète vers le nuage???

> - Le MD sur le nuage pose problème pour l'exportation donc 
> - Est qu'on ne pourrai pas poser un codiMD à coté pour remplacer le crapad
> - Prendre en main Farzad pour lui permettre de produire du contenu écrit (il consomme beaucoup de crapad)

## Robotisation
  
- bot "telephone" pour organiser des rencontres avec d'autres guildes discord? https://bots.ondiscord.xyz/bots/377609965554237453 (en gros c'est un pont audio entre deux salons vocaux)

> - excellent : faire des états généraux des discords engagé un grenelle 
