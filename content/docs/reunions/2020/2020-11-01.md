---
title: Dimanche 1 Novembre 2020
date: 2020-11-01
---
Présence : 15 personnes

# Education populaire : 
## La sécurité sociale alimentaire
> discussion reportée au lundi 2/11

## L'université populaire confinée
* 14-15 novembre : atelier constituant sur la sécurité sociale alimentaire. 
Au début conférence, puis atelier, jeu de rôle, mise en situation et finition avec un atelier d'écriture de script pour une vidéo canard sur ces travaux, histoire que l'éducation populaire ça reboucle sur de l'éduc pop.  
> Un groupe de travail sera constitué et se réunira début de semaine pro' pour organiser tout ça et faire des test.  

* 5 et 6 décembre : Université d'hiver des réfractaires, avec un jeu de mot a la mose : L'université des canard confi-né. 
Ce sera une succession d'ateliers et de conférence sur plusieurs jours. 
> Une annonce sera publiée et les premières réunions arriveront début de semaine prochaine  

*  Lancement d'une première escouade de rédaction vidéo pour augmenter le rythme des vidéos sans baisse de qualités. 
On commence avec un petit groupe de réfractaires puis on monte en puissance si le confinement le permet.  L'idée c'est de passer à 5 vidéos / semaine.  

*  Un des facteurs limitant va être le nombre d'animateur. 
L'idée peut donc de former quelques uns, créer une dynamique de groupe, faire des formations plus large. L'idée serait aussi, durant l'université, de faire des formations le matin et de la mise en pratique le soir.  

*  Greg 88 se lance dans la création de playlist youtube rassemblant des vidéos d'éduc pop. Un salon a été crée pour l'occaz et bientôt une adresse mail avec un appel en vidéo pour que tout le monde puisse participer. Ça fait du lien avec le projet débordement.

## Play-list

profiter du confinement pour faire de l'éducation populaire, en commençant par une playlist

comment matérialiser la playlist ?

> suggestion de faire une vidéo d'appel à source faite par Yohan, pour expliquer la démarche et réaliser une "vidéo fleuve", pour poser les liens de la playlist en suggestion ou en commentaire et permettre à l'audimat de communiquer des liens manquants ou critiquer les propositions faites.
> moyen d'avoir un accès. par chapitre. introduire en 5 lignes pourquoi c'est pertinent.
> proposition de discussion sur le contenu de la playlist des réfractaires sur discord
> Lancer la mise en place de la playlist sur YT rapidement

## Curation
Faut-il le retour à un bot curation pour signaler qu'un contenu a déjà été posté. 

> demande de réactivation, coopération. le nom du bot est collinks voir #archive-curation

# moyens d'actions
## Opération décoration de Noël militante, (la ronce de noël)

Présentation de l'idée d'action confinée

L'idée est de demander au gens de mettre des messages (Punchline) aux fenêtres ou déco de Noel et de demander aux gens de les photographier. ca pourrait faire un sujet de vidéo. 
- diverses idées : QRcode à mettre sur les voitures, caricatures... Lours en parlera aussi à JSPC.
- utiliser les supermarchés avec tracts planqués dans les rayons renvoyant vers une playlist média libre

> possibilité evt. Sticker 
> nom : sublimina, propaganda.
> Prochaine réunion >> Mardi 20:00 << annonce @everyone nécessaire
