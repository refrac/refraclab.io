---
title: Dimanche 20 Décembre 2020
date: 2020-12-20
---

# Discussions du Dimanche 20 Decembre 2020

## Retour sur l'emission d'hier

Vos avis:
  > cool, beaucoup d'audience (Verifier les analytics)

A améliorer: coordination de la cloture/fin du stream: bien signifier la cloture pour éviter que l'invité croit qu'il n'est plus en live et continue à parler alors que le live tourne
  > - dire un mot clef genre "des bisoux" pour couper

Signifier sur le canal écrit que les conversations sont visibles en live

### Yohan (alarash)
1. Préviens nous avant la veille quand tu a besoin des trucs (1 semaine à l'avance mini)
  > - Yohan Alarash
  > - Vérifie que les gens sont dispo pour le stream/modo/organisation des canaux et que les taches sont attribuées

### Sur peertube
  > https://canard.tube/videos/watch/64a85100-6c36-440f-9cf9-1220e4dbdaf6

## Upgrade disque sur peertube

  > on en discute cette semaine prochaine avec yohan pour ajouter du disque

## Demain soir 20h, on discute de l'évenement du 17 janvier! 

On fait le point. Nous devons finaliser le planning (Quadrature du net à recontacter/pas de retours), valider le concert et les pauses musicales (statut à actualiser): le doc est sur le crypt pad dans le dossier "évenements réfractaires"

- Besoins pour l'évenement: 1) support technique pour le stream ; 2) l'accueil ; 3) la modération. 
- Point important: la communication. Stratégie à discuter et à mettre en place
- Idées à discuter, (à améliorer), à valider et à organiser (Yohan): Teaser pour l'évenement avec la voix du Diable positif. Script ici! Lien édition! A SUPPRIMER APRES: https://pad.lecanardrefractaire.org/pad/#/2/pad/edit/-IUhmFu3Sm-HGeiiM7Op-Qux/ / On doit savoir ce soir ce que Yo doit nous préparer pour le 24 si possible.
- Prendre les RDV discord avec les intervenants
- Demander aux musiciens de débuter les enregistrements si possible
- Première Affiche à faire pour le 24



## Ebauche d'idée d'Alexandre Duclos
1. Evenenemnt physique de type atelier-débat d'éducation populaire posté dans annonces-réfractaire.

L’idée est de lier cheminement de la pensée et cheminement dans le pays, dans ses terroirs, ses cultures, ne pas être dans le déni de la distance, de la matière et des gens qui habitent la pluralité de pays qui composent le pays. Dans chaque lieu, on prend le temps de se promener, de cuisiner ensemble, de partager graines, plantes et pratiques agricoles locales, mobilisation locale. Le cheminement dans la pensée se substitue ici à la notion de validations successives d’acquis scolaires, ou à la simple succession. On prend le temps, on se mêle au lieu. 

## Calendrier

  Nopesto veut un calendrier, plusieurs options: le nuage, un tableur sur le cryptpad, un bot, autre chose...

## Modération

### Messages à caractère promotionnel 

1. Aujourd'hui, un membre a partager un Lien facebook pour une formation webinaire sur l'habitat de type yourte (légalité, contrainte, etc...): la conférence était payante.
  
  **Question:** autorisons nous en général les messages à caractere promotionnel payant si ceux-ci sont utiles pour les membres du canard? Ex: formations
  
  > De base: pas de message promotionnel à caractere payant 

### Liens d'invitation en général

2. Quid des liens d'invitation vers d'autres serveurs Discord, sachant que pour de nombreux utilisateurs permettre cette publicité revient à cautionner le serveur qui fait la pub.

  > Possible mais on en discute avant

### Autre

3. Pas d'insulte envers les membres, même ceux qui sont au marais-cage. Soyons plus matures, restons bienveillants.
4. que fait on de (pseudo d'emprunt) qui est toujours au marecage. je ne trouve pas ca sain de laisser la situation ainsi
> on le reintegre a la suite d'une discussion avec lui

## Université confi-née
### MCP
1. Atelier pour parler de l'organisation technique de l'université à des membres du MCP qui ont besoin de gérer un évènement, est prévue Mardi 22 décembre à 20h. Ce rendez-vous reste à confirmer après en avoir parlé demain soir à la réunion. Ils souhaitent organiser un evenement physique diffusé numériquement, ils se posent donc des questions comment s'organiser et quels outils utiliser.

### Code source de l'univ

> mose va conter la génèse du projet dans une video nommée "les coulisses de l'univ"

## Admin Discord

### Canaux
1. Les salons #💖💖-univ-confinée et #💖-compte-rendu-de-discussion.  Sont-il encore actuels ?
> supprimes
2. Pourquoi pas les combiner (simplifier un peu) en un salon # annonces-et-info-Agora ?

### Permissions lecture/écriture du nouveau salon #annonces-réfractaires
> Accès en lecture à tous les membres
> Accès en écriture aux Réfractaires

### Arts
1. Creer une rubrique arts
> modification du nom du salon Musique en Arts

## Traduction
1.  Traducteur anglais -> francais pour faire la traduction d'un document audio sur Assange. Vu le taff ( + 1h de traduction ) , on s'est dit avec mymy qu'on pouvait mettre un billet du canard la dedans. Si vous êtes chaud, on est chaud !
Le doc : https://www.youtube.com/watch?v=lfZQcV-frnY&feature=youtu.be

## Évènements à venir
### Booteille
1. Booteille souhaite faire un Q&R sur l'hygiène numérique en live sur PeerTube dans la semaine. Il me faudrait la clé de stream pour pouvoir le faire. Serait-il possible pour lui de faire ça ? 
> oui.

### Canal Concorde
1. Souhaite être coorganisateur pour un évenement pour ramener leurs invités. 
2. Partenariat: pas sous ce nom, en tant qu'invité, pour organiser une table ronde pourquoi pas. 

# Deplacement de l'heure de la reunion a 20h
