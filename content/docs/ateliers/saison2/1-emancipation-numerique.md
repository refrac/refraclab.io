---
title: 1 - émancipation numérique et sécurisation des communications dans le collectif
---

Plusieurs usages :

* communication travail collaboratif interne au collectif
* publication de contenu

Problèmes: 

* censure
* exploitation des données personnelles
* parler de manière anonyme
* infox / trolls
* surveillance des communications
* émancipation des grands groupes
* s'échapper des bulles algorithmiques des réseaux sociaux
* etc...

selon 12b, neo et synapse c'est peine perdu d'essayer d'échapper à la surveillance au delà d'un tout petit groupe de personnes qui se connaissent très bien entre elles.

propositions de solution:

* communication instantanées
  * Riot / Matrix : <https://matrix.org/>
  * wickr
  * torchat
  * <https://fr.wikipedia.org/wiki/TorChat>
  * <https://github.com/prof7bit/TorChat>
  * SSB (Secure Scuttlebutt) : <https://github.com/ssbc>
* Partages de documents et travail collaboratif
  * Nextcloud Hub : <https://nextcloud.com/hub/>
  * CryptPad : <https://cryptpad.fr/>
* Réseaux sociaux et publication de contenu
  * fediverse : <https://fr.wikipedia.org/wiki/Fediverse> (problème de l'hébergement)
  * peertube
  * Riot / Matrix : <https://matrix.org/>
* perspectives plus long terme:
  * IPFS
  * DAT / Hyperdrive / Beaker
  * Holochain

émancipation de discord?

Tout ça c'est des solutions techniques. Mais une autre partie de la solution est humaine.
Il faut faire adopter ces solutions par la population. Comment?

* "Prêcher" pour l'utilisation d'outils et plateformes libres dans les groupes militants et lors des manifestations
* Besoin d'éducation aux outils numériques et bonnes pratiques sur internet.
* Faire une proposition d'hébergement sur Peertube (à Yohan)
