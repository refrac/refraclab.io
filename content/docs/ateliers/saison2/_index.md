---
title: Ateliers Samedi 30 mai
bookCollapseSection: true
---

# EducPop Session 2, Samedi 30 mai : Les moyens d'action

## Présentation de la dé-marche

Nous jugeons utile de :
- ne pas mettre en place un atelier limité au samedi, l'idée est : le Samedi 30/05 marquerai le début d'un processus en perpétuelle redéfinition d'intelligence collective prenant place sur le Discord. L'atelier porterai donc sur la canalisation des énergies individuelles au travers de modes d'action. Assez de se plaindre, maintenant on agis !
- ne pas imposer une méthodo à tous les groupes
- avoir un cadre le plus léger possible
- ne pas imposer un horaire c'est le début d'un hackathon qui débutera Samedi matin

Pour celà nous avons pensé au dispositif suivant : 
- un salon vocal d'accueil à l’événement qui sera ouvert en permanence pour soumettre des idées de moyen d'action.
- chaque participant sur ce salon d'accueil peut proposer une piste à exploré pendant cette journée d'ouverture il lui sera alors attribué un salon vocal.
- tout les atelier travailleront sur un même crapad pour permettre la libre circulation d'un atelier à l'autre pour garantir une méga-fluidité supra-holoptique.
- pas de restitution prévu, chaque groupe décidera ce qu'il veut faire de ce moment qui leur appartiens entièrement et qui sera le point de départ, l’amorce de plein de moment récurent et auto-déterminé.
- le dispositif de facilitation sera réduit au stricte minimum (fonction accueil et contextualisation, publier la description des pistes explorée et ses différentes fourches, expliquer le concept de ce moment, et mettre dans l'ambiance). Éventuellement un facilitateur mobile circulera d'un atelier à l'autre si besoin. 

## Annonce sur discord

Jusqu'à présent nous avons concentré notre temps et notre énergie dans une démarche d'observation, de recherche ainsi que d'analyse des faits et des événements au sein de nos sociétés actuelles.  
Il nous semble désormais nécessaire d'aller plus loin, faire plus que comprendre.  
Ces derniers mois ont vu nos sociétés adopter des comportements douteux, révoltants voire dangereux. Nous voulons agir pour construire le monde dans lequel nous souhaitons vivre. Cela sans couverture de quelque parti politique que ce soit. D'où l'importance de nous réunir en nombre afin de penser différents modes d'actions.  
À partir de ce samedi 30 mai, sur le Discord, débutera un processus d'intelligence collective, permettant de confronter idées et pistes de réflexions/actions.  
Le but ? Créer un climat, un terreau fertile, propice au développement et à la concrétisation de nouvelles initiatives.
A la sortie du confinement tout est revenu à l'anormal. Pourtant nous sommes nombreux à regarder avec dégout le monde qui nous entoure mais que faire?  
Vous êtes bien sûr invité-e-s à vous joindre à cette démarche, plus nous serons, plus créatifs nous pourrons être et trouver ensemble des solutions concrètes pour notre avenir, car c'est de cela dont il est question. 

## Annonce sur les réseaux sociaux

Nous organisons￼ aujourd'hui sur le discord un marathon des idées sur les moyens d'action militants, tout le monde est invité à participer, si vous voulez vous joindre à nous c'est par ici: https://discord.gg/HaE: https://www.youtube.com/watch?v=0OOoNmfZQdI

## twitter (et facebook?)

Nous organisons￼ aujourd'hui sur le discord un marathon des idées sur les moyens d'action militants, tout le monde est invité à participer, si vous voulez vous joindre à nous c'est par ici: https://discord.gg/HaEpyY3
    
## youtube

* Nous organisons￼ aujourd'hui sur le discord un marathon des idées sur les moyens d'action militants, tout le monde est invité à participer, si vous voulez vous joindre à nous c'est par ici: https://discord.gg/HaEpyY3
* A la sortie du confinement tout est revenu à l'anormal. Pourtant nous sommes nombreux à regarder avec dégoût le monde qui nous entoure mais que faire? manifester? faire des pétitions? écrire à nos députés? tant de choses ont déjà été faites mais pour quel résultat? Par les précédentes réflexions collectives nous avons démontré notre capacité à trouver des solutions ensemble. Cette fois vous proposons de réinventer les procédés de lutte. Hier nous lutions, demain nous vaincrons.
* Vous êtes bien sûr invité-e-s à vous joindre à cette démarche, plus nous serons, plus créatifs nous pourrons être et trouver ensemble des solutions concrètes pour notre avenir, car c'est de cela dont il est question.

# Propositions idées d'actions

## propositions de Bountar déjà lancé

- Reprendre les fiches fournies par le gouvernement à destination du personnel des établissements  scolaires pour voir s'il est possible d'en détourner les objectifs/arguments de manière à les retourner contre leurs auteurs
Base de travail : <https://eduscol.education.fr/cid151499/reouverture-des-ecoles.html> (section "*Préparer l'accueil des élèves*")
- Le municipalisme libertaire (Murray Bookchin), sa mise en place, ses limites
- Développer un mode de fonctionnement similaire à celui des banlieues qui se sont réappropriées leur environnement et obéissent désormais à leurs propres codes
- Utilisation du système pour l'amener à sa mort (vivre en "parasite", dépendre d'aides sociales et se reposer dessus) Tous au RSA ! ! !

## Proposition par Kys:
-Elargir la désobéissance, en faire une vrai culture : refuser de consommer dans les grandes surfaces, refuser de souffrir au travail, refuser de vendre des produits/services néfastes. Refuser ce que l'on ne cautionne pas, refuser que l'on nous l'impose. Refuser encore et toujours... enfin remplacer par des alternatives
-Théorie monétaire moderne ? Post-monétarisme ? Autre modèle économique ?
   
## Proposition de PaprikaRock
Appel à la résistance du 18 juin 2020 (80 ans après où en sont nos libertés)
<https://fr.wikipedia.org/wiki/Appel\_du\_18\_Juin> )

## Proposition de tshatsha dénoncé par invivo le cafteur :
Le Gouvernement c'est nous : GIY Gov It Yourself (à venir dans l'aprème) <https://legouv.fr/le-gouv-co/> 

## Proposition de néo
Investigation sur LREM (page wiki, compte tweeter, ...)

## Proposition de invivo 
Jeux de rôle

## Proposition d'opaline
Sensibilisation au outil numérique pour aider les camarade qui se retrouve de l'autre coté de la fracture numérique 
Gilet Jaunes à la rencontrer des gens dans les quartier populaire 

## Proposition de abricot
Les députés on des chaines YouTube c'est un terrain potentiel ? vous en pensez quoi ?
leur poser la question sur leur positions sur le RIC par exemple  , mais il ne commente jamais , donc c'est juste pour visibiliser nos idées pas plus. il faut trouver leur moyen de communication d’abord utilise t il plus le téléphone un réseau une appli il faut faire un sondage , une liste député/moyen de communication utilisé pour un contact direct, faire du lobbying citoyen. <https://www.youtube.com/watch?v=naGUckMhERo>

