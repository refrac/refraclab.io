---
title: 2 - Répertoire de groupes et d'actions
---

Point de départs : proposition de KL: Faire converger, fédérer, les différents appels, les différentes initiatives, groupes

Contre proposition de Mose : faire des passerelles entre initiatives, plutôt que les fédérer en une structure plus Rigide.

But: se rassembler plutôt autour des idees Plutôt qu’autour de personne même si on sait que les communautés sont plutot constituées de gens qui ont des idées. Les gens peuvent appartenir à plusieurs groupes et font le lien entre ceux-ci.

Par groupe nous entendons : des associations, des collectifs, des communautés de personnes qui se rassemblent autour d’idées, de thèmes, d’actions.

On ne peut pas fédérer durablement les gens en regroupant des thèmes trop variés en un seul paquet (principe des partis politiques). Il faut laisser la liberté aux gens de ne pas être d’accord sur tout et donc de se retrouver en groupe autour d’idées précises. Ils pourraient alors participer à plusieurs groupes plutôt que de devoir faire des compromis sur un thème pour en soutenir un autre.

Notre but ici est donc de trouver un moyen d’établir des passerelles et des moyens d’information/d’interactions entre les groupes.

Mode de mise en action: utilisation de la structure technique d'internet : chaque groupe reste autonome,  il faut alors des protocoles pour qu'ils se synchronisent. Il y a des passeurs (membres de plusieurs groupes a la fois) qui peuvent faire circuler les infos.

Nous avons besoin de protocole, comme un format, qui puisse etre connu de tous les groupes. Comme sur les mails.
L’objectif est de mettre au point un format qui permette de fabriquer une unité d'information/synchronisation entre groupes

Pour diffuser le plus d’informations, on peut inventorier des liens, des sources, des videos
Sous la forme d’un site d’information qui centralise plusieurs source, une revue de presse (ou autre). Mais pour sourcer efficacement, il faut savoir qui sont les auteurs. Si les groupes sources ont des sites, ca peut aider pour se faire connaitre. Si on a un format commun par exemple pour decrire chacun des groupes (genre comme humans.txt mais pour les groupes <http://humanstxt.org/FR>) ça permet de les identifier rapidement et clairement.

Chaque groupe a son interface (sa vitrine) et/ou son référent propre et en est responsable. (si on a une plateforme commune: si quelqu'un y publie une merde ça eclabousse tout le monde)
Les sites des groupes ne devraient être qu'une vitrine et un moyen de contact ou on ne publie que ce qui est validé par le groupe. Pour éviter la censure,le débat se fait en direct sur discord ou en présence physique si possible.

Pour plus de durabilité et d’efficacité, il faut voir les choses sous l'angle des actions plutot que sur les groupes: federation par action a la volée plutot que de federer des groupes.

idée : faire une sorte d’annuaire des groupes, un genre de point central pour connaitre les autres groupes et leurs actions

Nécessité d’une norme pour identifier les groupes, comme une fiche signaletique auto-declarée
et puis une norme pour qualifier les actions qui peuvent donner lieu à des convergences

on devrait avoir un site qui explicite la norme, comme pour humanstxt

norme de présentation des groupes : (tout n’est pas obligatoire)

- nom
- logo
- localisation (IRL : region, ville... / En ligne : site, plateform...)
- rayon d'action (local, regional, national, international ...)
- point et modalités de contact (adresse mail, formulaire, groupe FB...)
- site web
- flux de publication (les endroits ou le groupe publie des choses, fb, yt, autres...)
- types de structure (media, syndicat, association, fondation, association de fait, collectif)
- Types d’Actions (reflexion, conferences, podcasts, evenements, diffusion, action locale, collecte d'information, education populaire, ..)
- themes généraux (economie, politique, environnement, santé, education, societe, technologie, culture, solidarité, ...)
- description

norme de présentation des actions

- groupe(s)  organisateur(s) (ca peut plusieurs groupes)
- nom de l'action (optionnel)
- referant (point et modalités de contact) 
- lieu (physique ou virtuel)
- date (et heure, de debut)
- durée (peut-etre infini)
- pre-requis (À prévoir pour une participation  optimale: ex:  matos pour une manif, logiciel a installer pour participer, connaissances utiles si c'est une conference pointue)
- public visé (qui est concerné par l'action)
- site (eventuel, page facebook, page web, etc)
- affiche (ou visuel)
- nombre minumum et maximum (de participants)
- payant (participation aux frais?)
- type (manifestation, conference, live, atelier, educ pop, ou autres a definir chacun son truc)
- description
