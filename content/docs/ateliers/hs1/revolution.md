---
title: HS1-10 à quand/comment la révolution
weight: 10
---

# A quand/comment la révolution ?

## Sources :

- https://www.monde-diplomatique.fr/2019/08/DERRAC/60152 (Comment la non-violence protège l’État. Essai sur l’inefficacité des mouvements sociaux)
- https://blogs.mediapart.fr/cuenod/blog/040519/quand-la-revolution
- https://www.revolutionpermanente.fr/Gilets-jaunes-Le-retour-du-spectre-de-la-revolution
- https://blogs.mediapart.fr/sylla/blog/220518/comment-faire-la-revolution-aujourd-hui-et-autrement
- https://en.wikipedia.org/wiki/Blueprint_for_Revolution
- https://www.cairn.info/revue-la-cause-freudienne-2010-2-page-212.htm
- https://www.youtube.com/watch?v=Ij0phqSPuHU (NI DIEU NI MAÎTRE VERSION LONGUE COMPLÈTE 3H34)
- https://www.youtube.com/watch?v=4FJMWnQPcyI (Étienne Chouard : La vision constituante - L'interview complète)
- https://www.youtube.com/watch?v=Ycg8Pa6Wku0 (FRANCK LEPAGE #9 - La place de la violence)
- https://www.youtube.com/watch?v=xPab_IVEnpQ (Ne Vivons Plus Comme Des Esclaves)
- https://www.youtube.com/watch?v=kaTdgO8BZLM (Usul. Le «monde d’après» sera altermondialiste ou ne sera pas)
- http://www.delaservitudemoderne.org/francais1.html
- https://www.lemonde.fr/international/article/2019/11/03/a-hongkong-la-police-adapte-sa-strategie-face-a-la-perseverance-des-manifestants_6017851_3210.html
- https://www.franceinter.fr/emissions/geopolitique/geopolitique-28-avril-2020
- https://www.revolutionpermanente.fr/Des-revoltes-de-2005-a-aujourd-hui-l-urgence-d-un-programme-d-action-pour-les-quartiers-populaires

Le 42 avril, petite question : si la crise économique amène l'embrasement des quartiers populaires ? quelles possibilités avons nous d'en être solidaire ?

## Propositions sur le comment :
    
- Insurrection ou pas ?
- Ne pas avoir de propositions, apparitions spontanées de la solution : l'an 01 
- Préparer un projet de société : réseau salariat (Bernard FRIOT)
- Opérer une sécession progressive : réquisition partielle de l’État

## Faut-il répondre à la propagande d'état par une "contre-propagande" ?

Quid de la corruptibilité et des tentatives de déstabilisation des révolutionnaires ?  
Peut-on s'inspirer des méthodologies étatiques pour manipuler l'opinion dans des pays étrangers ? (E.U. vs Venezuela - sur l'axe de la propagande uniquement)  
Cas d'école d'émancipation du pouvoir : les banlieues françaises ayant créé une économie parallèle et possédant une puissance de dissuasion assez forte face à l'état (armes lourdes).  
Sommes-nous prêts à entamer une révolution et à potentiellement renoncer à un certain confort de vie que nous offre la société actuelle, quelles sont les limites collectives qui peuvent faire basculer la situation ?  
Peut-on s'enrôler dans l'armée/la police pour faire sauter les outils de protection des riches et les retourner contre eux ?

WikiLeaks cherche à rendre publiques les questions d'intérêt général pour empêcher une minorité de "puissants" de s'arroger le droit d'engager les populations dans des guerres (économiques/militaires/autres) en leur cachant ce qui se trame en fond. Il pourrait s’agir d'une première étape, mettre à mal les secrets d'états et dévoiler les cartes des uns et des autres pour que le collectif puisse prendre conscience et se soulever.  
Au risque d'échouer pour différents motifs comme par exemple la sidération.

Besoin de définition des principes de liberté et d'égalité pour aller plus avant sur le sujet.
