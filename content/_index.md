---
title: Le discord du Canard Réfractaire
type: docs
---

## Qu'est ce que le Canard Réfractaire ?

Le [Canard Réfractaire](https://lecanardrefractaire.org/) est un auto-média populaire et indépendant, basé dans les Côtes d'Armor en Bretagne.

La vitrine du média, c'est sa [chaîne youtube](https://www.youtube.com/channel/UCWGsN59FON3vOtXCozQPsZQ) (et son [serveur peertube](https://canard.tube/)) où sont publiées, depuis plus d'un an, plusieurs vidéos par semaine, d'analyse politique "en pas de côté" - c'est à dire que l'on prend soin de prendre du recul et de tenter une approche différente de la mêlée médiatique.  S'y rajoute une critique du libéralisme ; la critique de la puissance oligarchique et médiatique des grandes fortunes ; la critique de la bourgeoisie ; et finalement, la joie et la force dans nos actes militants.

## Qui sommes-nous ?

Le Canard Réfractaire est apartisan, asyndical, et ne fait partie d’aucune institution. Nous sommes totalement indépendants et militants.
Nous militons pour une éducation populaire politique afin d’apporter une réflexion et de pousser les gens à se renseigner pour devenir autonomes sur leurs décisions ou choix politiques.
Nous voulons être le moteur d’un déferlement d’actions collectives qui tendent vers une réappropriation politique par le peuple.

## Qu'est ce que le Discord du Canard Réfractaire ?

Le [Discord du Canard](https://discord.gg/HaEpyY3) a été créé pour rassembler la communauté du média, autour de la notion de l'éducation populaire politique. Nous voulions créer un débordement. Que des initiatives et des actions soient engagées sur la ligne de conduite du Canard. Le discord du Canard Réfractaire est un outil du média.

Notre ligne éditoriale est l’héritière des gilets jaunes : nous sommes de gauche pour ceux qui nous pensent la gauche, nous sommes du peuple pour ceux qui nous pensent le peuple. L’important, c’est notre volonté de construire l’émancipation du grand nombre, notre mépris de la bourgeoisie, notre critique du spectacle médiatique, notre exposition de l’impérialisme. Le reste appartient au mouvement social.

## Le règlement en vigueur sur le serveur du Canard

- Le respect est quelque chose de très important. La courtoisie et le savoir-vivre sont de rigueur.
Nous ne pouvons pas tous être d'accord. Chacun a sa propre "vérité". Ce serveur est un lieu d'échanges, de communication et de débats construits et constructifs. Si une discussion n'avance pas, ne vous fâchez pas : ça ne sert à rien ! Quittez la conversation.
- La diffamation et la propagande sont également proscrites sur le serveur.
Tous liens ou posts publiés dans les différents salons devront être accompagnés d'un argumentaire ou d'une phrase qui précise pourquoi vous l'avez publié. Cela peut susciter un dialogue, un échange ou un débat construit : de l'éducation populaire, quoi ! Tous liens ou publications, juste pour faire sa pub ou pour poster pour poster, seront supprimés.
- Les propos non sourcés n'ont aucun intérêt pour notre travail collectif et seront considérés au même titre que les messages parasites.
- Ce serveur est un serveur de gauche. Cependant, tout le monde y est le bienvenue pour échanger et faire de l'éducation populaire. Dans le cas où, un membre vient pour insulter ou mépriser notre orientation politique, il sera banni.
- Les attitudes militantes, consistant à noyer le flux des liens similaires, à imposer encore et encore le même débat clivant et à trouver un anti-camarade pour se renvoyer la balle est aussi proscrite. On se garde le droit de couper ces dynamiques pour préserver les débats utiles et sincères.
- Vous seuls êtes responsables de vos publications sur ce serveur. Le Canard Réfractaire n'engage en aucun cas, sa responsabilité sur ce qui peut être publié ou dit qui ne soit pas de son fait.
- En cas de conflits qui dépasse le cadre sympatoche du serveur, nous nous réservons le droit à la modération arbitraire. L'exposition du discord et le temps limité des modérateurs et administrateurs du serveur ne permet pas d'étudier au cas par cas.
- Il est strictement interdit de divulguer sans autorisation des informations personnelles ou privées d'une autre personne, tout comme diffuser dans les salons publics des discussions ou éléments de discussions privées.
- Evitons les promotions parasites, à l'exception des liens Discord ou vers d’autres médias indépendants et engagés.
- L'abus de l'écriture en majuscules, de retour à la ligne et d'emojis est déconseillé.
- Pensez à supprimer ou modifier votre message si vous vous trompez.

# Modération

Notre [Charte de modération](moderation) explicite les règles suivies par le groupe de modération au sein de l'espace discord. En résumé:

> Le rôle de modérateur n'est pas un enjeu de pouvoir. Son objet est de se mettre au service de la communauté. C'est une personne capable d'évaluer une situation avec bienveillance, d'intervenir efficacement pour préserver l'espace collectif, et qui joue le rôle d'un relais au nom du collectif des réfractaires.

# Canaux disponibles

- `accueil` : vous y êtes invité à lire et cocher donc accepter le règlement qui s'y trouve.

## La vitrine

- `vidéos-du-canard` : un bot ou Yohan nous averti lorsqu'une vidéo est publiée sur la chaîne YouTube ou CanardTube.
- `annonce` : les événements du discord
- `médias-des-copains` : les liens vers les partenaires et les média indépendants amis.
- `répertoire-médiatique` : est un annuaire collaboratif.

## Agora

C'est un espace de discussion avec un maximum d'auto-gestion/de libertés :

- `blabla` : c'est pour tout et n'importe quoi, du blabla.
- `discussions-générales` : c'est pour discuter de sujets variés. Merci d'argumenter sérieusement si vous débattez.
- `idees` : pour les idées militantes et les idées intéressantes.
- `news-et-actus` : pour les news et actus.
- `vos-medias` : on y partage des articles intéressants (peut servir à alimenter la revue de presse).
- `vécu` : on y partage des expériences personnelles, des anecdotes…
- `fact-checking` : on fact-check des documents.
- `humour` : …faut il préciser ?
- `arts` : on y partage nos musiques, éventuellement on en discute.
- `retours-sur-l'emission` : on discute ici des dernières vidéos du Canard Réfractaire.

et quelques canaux vocaux :

- `vocal-accueil` lié au canal `accueil-ecrit`, des réfractaires pourront vous accueillir ici.
- `vocal-1-[détendu]` lié au canal `écrit-vocal-1`. Sur ce vocal on discute où on vient juste écouter et sur l'écrit on participe par écrit, on poste les liens.
- `vocal-2-[libre]` lié au canal `écrit-vocal-2`. Idem, sur ce vocal on discute où on vient juste écouter et sur l'écrit on participe par écrit, on poste les liens.
- `vocal-3-[jeu]` lié au canal `écrit-vocal-3`. Un espace pour jouer à des jeux ou simplement discuter avec d'autres membres.
- `vocal-4-[atelier]` lié au canal `écrit-vocal-4`. Un espace pour s'exprimer à condition d'accepter le cadre choisi par le groupe (travail, brain-storming, tour de parole, réunion...).

## Revue-de-presse

Cette section se structure en canaux de débats thématiques uniquement accessibles aux membres ayant le role `LES REFRACTAIRES`, nous vous demandons de bien vouloir respecter ces thématiques autant que possible afin de faciliter la modération et de ne pas rendre les canaux illisibles.

- On poste les média au bon endroit.
- On vérifie que le lien n'a pas déjà été posté.
- On peut discuter mais on évite de « flooder » le canal ou de poster un même contenu dans différents canaux. S'il y a trop d'échanges on se recentre sur un canal de l'agora en invitant les gens à venir échanger.

## Educ-pop

Cette section sert à faire de l'éducation populaire, pratique d'éducation horizontale remise au goût du jour par Franck Lepage ([video](https://youtu.be/q2VyR875gwU)), ici on s'éduque entre nous!

## Rédaction collaborative

Cette section sert à aider yohan à rédiger ces videos à l'aide de salon temporaire et thématiques qui serviront à collecter des informations sur un sujet donné.

# Rôles

Entrer dans la communauté vous laisse la possibilité de jouer un de ses rôles à nos côtés ou juste venir vous informer. Aucune obligation n'est imposée.

- **Membre** : en vous inscrivant si vous signez la charte vous devenez membre, ce choix vous permet d’accéder aux canaux précédents et d'y participer.
- **Correcteur** : sont des membres qui se sont proposés pour aider à la correction d'article du Canard.
- **Les Réfractaires** : sont des membres qui ont été identifiés par la communauté pour leur engagement et leur participation active. Cette reconnaissance permet de contribuer à d'autres canaux et d'autres rôles :
  - **Accueil** : ce rôle est d'accueillir les nouveaux arrivants. Ils ont la possibilité d'attribuer le rôle Réfractaire.
  - **Curation** : tient à jour `revue-des-réfractaires` entretenu et agréable à la lecture.
  - **Facilitation** : fourni un cadre coopératif, accompagne les groupes et leur apporte les outils et la démarche adaptées aux missions qui leur sont confiées.
  - **Fact-checking** : vérifie les faits et sensibilise les membres publiant des articles non sourcés, et redirige au besoin vers la catégorie `fact-checking`.
  - **Techos** : conçoivent et améliorent les outils numériques à disposition du groupe pour pouvoir coopérer et fonctionner ensemble efficacement.
- **Modération** : ont pour rôle de garantir le bien être de tous. Ils veillent au respect de la charte du discord. Suppriment les messages inutiles dans les canaux revue de presse (ces derniers sont alors sauvegardés et un lien vers la sauvegarde est déposé à la place). Le cas échéant ils peuvent sanctionner les comportements toxiques / sources de tensions mais uniquement en dernier ressort. Ils peuvent vous envoyer au   où vous ne verrez plus qu'un seul canal. Dans le marais-cage, vous pourrez discuter avec les réfractaires. Ceux-ci assureront la médiation avant de décider de vous réintégrer ou de vous bannir.
- **Plombier** : ont les mêmes droits que les modérateurs mais sont là pour s'occuper de nos petits bots d'amour &#x2764;
- **Concierge** : assurent le bon fonctionnement du serveur.

# Bots

Vous pourrez apercevoir sur ce serveur discord quelques membres un peu bizarre avec une petite étiquette `BOT`, ce sont des robots c'est à dire des programmes informatiques qui répondent à des commandes pour aider à gérer le discord (ou pas). Certains de ces bots sont créés par nos soins (donc open-sources évidemment) et auto-hébergés, d'autres sont des bots externes de grandes ampleurs présents sur des millions de serveurs.

- **collinks** : ce bot (créé par @12b &#x2764; et @mose &#x2764;) est la pour collecter tout les liens postés dans le discord et les ranger dans une base de données pour pouvoir en faire la curation plus tard. Ce bot est auto-hébergé et open-source. [sources](https://gitlab.com/crapaud-fou/collinks)
- **Géo Postetou** : ce bot gère des flux [RSS](https://fr.wikipedia.org/wiki/RSS) afin de poster leur contenu dans les canaux discord dédiés. Des flux RSS tel que les chaines CanardTube, la chaine YouTube du Canard et le site du Canard. Il s'agit d'une instance auto-hébergée de [MonitoRSS](https://monitorss.xyz) qui est sous licence libre.
- **mechanical duck** : ce bot (créé par @00101010) a un but de modération, il permet d'attribuer temporairement un role a un membre du serveur. Il est auto-heberge et open-source. [sources](https://gitlab.com/refrac/mechanical-duck)
- **MEE6** : ce bot est un bot externe de modération. nous n'avons pas accès aux sources et il est hébergé par l'entreprise MEE6. [site](https://mee6.xyz/)
- **Perroquet Réfractaire** : ce bot (imaginé par @tytan652) permet d'ajouter à un salon vocal une source audio externe (autre salon discord, lecteur de musique et etc). Plus d'info [ici]({{< ref "docs/ressources/discord-audio-pipe" >}}).
- **Rythm** : ce bot est un bot externe qui sert a jouer des vidéos(juste le son) à partir de youtube dans un vocal, il est uniquement utilisable dans les canaux réfractaires. [site](https://rythmbot.co/)
- **Sandrine Caneton** : ce bot (codé par [moonstar-x](https://github.com/moonstar-x) et forké par @00101010 pour l'adapter a nos besoins) est un bot de text-to-speech c'est à dire qu'il sert à lire du texte avec une voix synthétique dans un vocal, il est utilisable par tout les réfractaires ainsi que les membres autorisés a l'aide de la commande $say. ce bot est auto-hébergé et open-source [sources](https://github.com/00I0I0I0/sandrine-caneton)\
&#x26A0; ce bot utilise l'API de google translate pour générer la voix.
- **YAGPDB.xyz** : ce bot est un bot externe de modération. nous n'avons pas accès aux sources et il est hébergé par l'entreprise botlabs. [site](https://yagpdb.xyz/)
