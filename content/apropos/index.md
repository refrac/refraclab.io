---
title: A propos
---

Ce site est maintenu par un groupe de Réfractaires, hébergé chez [Gitlab](https://gitlab.com/refrac/refrac.gitlab.io). Il est généré par [Hugo](https://gohugo.io) et utilise le thème [hugo-book](https://themes.gohugo.io/hugo-book/).

Les réfractaires, c'est le nom que se sont donné les gens qui ont rejoint le discord mis en place par Yohan (avec l'aide d'invivo et d'espiOn). Du moins le groupe de départ, les premiers venus, s'entendant bien et dans un élan de consolidation communauté, se sont mis d'accord pour accorder à ceux qui les rejoigne le status de réfractaire du moment ou ils démontrent leur volonté d'avoir une contribution constructive a cet espace social.
